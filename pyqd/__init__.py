# -*- coding: utf-8 -*-

"""

S.D.G

pyQD is a Split-Operator quantum dynamics packaged developed and maintained by
Fabian G. Dröge (@FabianGD, fabian.droege@uni-jena.de) at the
Friedrich Schiller University in Jena, Germany.

"""

from ._version import get_versions
from .exceptions import *
from .integrators import *
from .interactors import *
from .interactors.defaults import *
from .observables import *
from .observables.defaults import *
from .parameters import *

# pylint: disable=invalid-name
from .parameters.defaults import *
from .plotting import *
from .utils.io import *
from .utils.physics import *

__version__ = get_versions()["version"]
del get_versions
