# -*- coding: utf-8 -*-

""" Parameter base class for pyQD """

from dataclasses import asdict
from functools import partial
from pathlib import Path

import attr

from ..parameters.defaults import Base
from ..utils.io import input_filter, input_serialiser, open_yaml_or_json


@attr.s(slots=True)
class PyQDParameter:
    """The base parameter class. The ancestor of all PyQDParameter objects.
    Implements the most basic slots, the integer values n_s, n_g, n_m and the base
    populate() method that is used to interprete and assign values to slots.

    Args:
        n_states: How many potential energy surfaces does the system have?
        n_gridpoints: On how many gridpoints is the wavefunction discretized?
        n_monomers: How many dimensions does the wavefunction have. The name has
            legacy meaning in the sense that in a n monomer system with one vibrational
            DOF, the number of monomers is equal to the number of DOF and therefore the
            number of wavefunction dimensions

    """

    DEFAULT_CLS = Base
    UNITS = {}
    INPUT_STR_ID = ""

    n_g: int = attr.ib(converter=int)
    n_m: int = attr.ib(converter=int)
    n_s: int = attr.ib(converter=int)

    @classmethod
    def from_defaults(cls, **kwargs):
        """
        Method to populate the class from the corresponding dataclass.
        """
        inputs = asdict(cls.DEFAULT_CLS(**kwargs))
        return cls(**inputs)

    @classmethod
    def from_file(cls, file: str, idx=0, **kwargs):
        """
        Base 'template' method to populate the class from a file.
        """

        key = cls.INPUT_STR_ID
        file = Path(file)

        data = open_yaml_or_json(file)

        if key != "dummy":
            if isinstance(data[key], list):
                relevant = data[key][idx]
            elif idx == 0:
                relevant = data[key].copy()
            else:
                relevant = {}
                raise IndexError("You should not index a non-list input.")
        else:
            relevant = {}

        # If only the dict is needed, return here!
        if key == "main":
            relevant.pop("n_gridpoints")
            relevant.pop("n_dimensions")
            relevant.pop("n_states")

        inpdict = {
            "n_g": data["main"]["n_gridpoints"],
            "n_m": data["main"]["n_dimensions"],
            "n_s": data["main"]["n_states"],
            **relevant,
        }

        return cls(**inpdict, **kwargs)

    @property
    def basekwargs(self):
        """
        Utility method to retrieve the base kwargs of the current instance.
        """

        basekwargs = {}
        for key in ["n_m", "n_s", "n_g"]:
            basekwargs[key] = getattr(self, key)

        return basekwargs

    def as_input(self, exclude_base=False, exclude_underscore=True):
        """Get the instance data as a dictionary ready to be converted to an input file."""

        dct = {}

        dct[self.INPUT_STR_ID] = attr.asdict(
            self,
            filter=partial(
                input_filter,
                exclude_base=exclude_base,
                exclude_underscore=exclude_underscore,
            ),
            value_serializer=input_serialiser,
        )

        return dct
