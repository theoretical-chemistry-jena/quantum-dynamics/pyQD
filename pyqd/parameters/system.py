# -*- coding: utf-8 -*-

""" SystemParameter classes for pyQD """

import logging
import typing
from dataclasses import asdict
from functools import partial
from typing import Any, Dict, Iterable, List, Tuple, Union

import attr
import numpy as np
import pint

from ..exceptions import PyQDInputError
from ..parameters.defaults import AnalyticalSystem, Base
from ..utils.adiabatic import batch_diag_to_vec, batch_vec_to_diag, diagonalize_ham_exc
from ..utils.convert import _list_converter, _mu_converter, _unit_converter
from ..utils.physics import harmonic_osc_1d
from ._base import PyQDParameter

logger = logging.getLogger(__name__)


@attr.s(slots=True)
class AnalyticalSystemParameter(PyQDParameter):
    """
    This should hold potential parameters, the units(?) and the grid parameters.
    Takes a wrapper-prepared dictionary to fill
    """

    DEFAULT_CLS = AnalyticalSystem
    INPUT_STR_ID = "analytical_system"

    UNITS = {
        "omega": ("eV", "hartree"),
        "en_gs": ("eV", "hartree"),
        "en_es": ("eV", "hartree"),
        "pos_gs": ("angstrom", "bohr"),
        "pos_es": ("angstrom", "bohr"),
        "gmin": ("angstrom", "bohr"),
        "gmax": ("angstrom", "bohr"),
        "mass": ("Dalton", "amu"),
    }

    omega: Union[pint.Quantity, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["omega"][-1])
    )
    en_gs: Union[pint.Quantity, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["en_gs"][-1])
    )
    en_es: Union[pint.Quantity, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["en_es"][-1])
    )
    pos_gs: Union[pint.Quantity, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["pos_gs"][-1])
    )
    pos_es: Iterable[Union[pint.Quantity, str]] = attr.ib(
        converter=[_list_converter, partial(_unit_converter, unit=UNITS["pos_es"][-1])]
    )
    gmin: Union[pint.Quantity, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["gmin"][-1])
    )
    gmax: Union[pint.Quantity, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["gmax"][-1])
    )
    mass: Union[pint.Quantity, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["mass"][-1])
    )

    trans_dipole: Dict[str, float] = attr.ib(converter=_mu_converter)

    adiabatic: bool = attr.ib(default=False)
    adiabatic_interactors: typing.Optional[List] = attr.ib(default=None, kw_only=True)

    _xgrid: np.ndarray = attr.ib(default=None, init=False)
    _pes: np.ndarray = attr.ib(default=None, init=False)
    _np: Dict[str, float] = attr.ib(default=None, init=False)

    def __attrs_post_init__(self):

        pot_diff = self.n_s - self.n_m

        if pot_diff > 1 and self.n_m > 1:
            raise NotImplementedError(
                "This combination of n_monomers and n_states is currently "
                "not implemented. Sorry."
            )

        if pot_diff > 1 and len(self.pos_es) == 1:
            raise PyQDInputError(
                "You specified multiple excited states but did not specify enough positional "
                "displacements. Please specify a list of values for pos_es"
            )
        elif pot_diff > 1 and len(self.pos_es) != (pot_diff):
            raise PyQDInputError(
                "Size mismatch for the positional displacements of the excited states: "
                "{} (required) != {} (given). Aborting".format(
                    pot_diff, len(self.pos_es)
                )
            )

        # Make pos_es symmetric if only one is specified
        if len(self.pos_es) == 1:
            logger.warning(
                "You only specified a single value for pos_es. "
                "I assume you want a symmetric potential energy surface."
            )
            self.pos_es *= self.n_s - 1

        if self.adiabatic_interactors is None:
            self.adiabatic_interactors = []

    @classmethod
    def from_defaults(cls, base: Base, **kwargs):  # pylint: disable=arguments-differ
        """
        Method to populate the class from the corresponding dataclass.
        """
        basekw = asdict(base)
        inputs = asdict(cls.DEFAULT_CLS(**kwargs))
        return cls(**basekw, **inputs)

    @property
    def dx(self) -> float:
        """Returns the grid spacing."""
        return np.abs(self.gmax.m - self.gmin.m) / (self.n_g - 1)

    @property
    def minmax(self) -> Tuple[float, float]:
        """Returns the minimum and maximum grid values as a tuple."""
        return (self.gmin.m, self.gmax.m)

    @property
    def xgrid(self) -> np.ndarray:
        """Returns the single-dimension xgrid of the system."""
        if self._xgrid is None:
            self._xgrid = np.linspace(*self.minmax, num=self.n_g)

        return self._xgrid

    @property
    def mu(self) -> List[Tuple[List, float]]:
        """Return the list of transition dipole moments"""
        return self.trans_dipole

    def gen_pes(
        self,
        ext_adiab_interactors: List[Any] = None,
        adiab_t=0.0,
        adiab_dtt=1.0,
    ):
        """
        Generate a potential energy surface.
        Has the possibility to calculate adiabatic potential energy surfaces for
        $n_s = 3$ and $n_m \\in {1,2}$. It assumes, for the moment, that only the
        excited states interact and therefor, the upper 2x2 matrix of the 3x3 matrix
        will be diagonalised.
        """

        shape = [self.n_g] * self.n_m + [self.n_s]
        self._pes = np.zeros(shape, dtype=np.float64)

        pdiff = self.n_s - self.n_m
        if pdiff == 1:
            self._calculate_pes_standard()
        elif pdiff == 2 or (self.n_m == 1 and pdiff > 2):
            self._calculate_pes_1mns()
        else:
            raise NotImplementedError()

        if (
            self.adiabatic and self.adiabatic_interactors is not None
        ) or ext_adiab_interactors is not None:

            if ext_adiab_interactors is None:
                ext_adiab_interactors = []

            if self.n_s != 3 or self.n_m not in [1, 2]:
                raise NotImplementedError(
                    "Only 3x3 potential energy surfaces and 1 or 2 batch dimensions "
                    "are currently implemented."
                )
            vham = batch_vec_to_diag(self._pes)

            # Generate a set of all given adiabatic interactors so that duplicates
            # may be removed. We only want to count the same interactor once.

            ad_interactors = self.adiabatic_interactors + [
                i for i in ext_adiab_interactors if i not in self.adiabatic_interactors
            ]
            for interactor in ad_interactors:
                en_matrix = interactor.all_matrices(
                    t=adiab_t, dtt=adiab_dtt, print_e="matrix", energy_only=True
                )
                vham += en_matrix
                logger.debug(en_matrix)

            diag_nt = diagonalize_ham_exc(vham)
            pes = batch_diag_to_vec(diag_nt.ham_diag)
            return_val = pes, diag_nt
            self._pes = pes

        else:
            return_val = self._pes

        return return_val

    def _calculate_pes_standard(self):

        pot_gs = harmonic_osc_1d(
            self.xgrid, self.pos_gs.m, self.en_gs.m, self.omega.m, self.mass.m
        )

        for ii in range(self.n_s):
            list_of_potentials = [pot_gs] * self.n_m

            if ii != 0:
                pot_exc = harmonic_osc_1d(
                    self.xgrid,
                    self.pos_es[ii - 1].m,
                    self.en_es.m,
                    self.omega.m,
                    self.mass.m,
                )
                list_of_potentials[ii - 1] = pot_exc

            pot = np.sum(np.asarray(np.meshgrid(*list_of_potentials)), axis=0)
            self._pes[..., ii] = pot

    def _calculate_pes_1mns(self):
        pot_gs = harmonic_osc_1d(
            self.xgrid, self.pos_gs.m, self.en_gs.m, self.omega.m, self.mass.m
        )

        potential = [pot_gs]

        for ii in range(self.n_s - 1):
            potential += [
                harmonic_osc_1d(
                    self.xgrid,
                    self.pos_es[ii].m,
                    self.en_es.m,
                    self.omega.m,
                    self.mass.m,
                )
            ]

        self._pes = np.asarray(potential).T

        del potential

    def _meshgridify_conditional(self, list_of_arrays: Iterable[np.ndarray]):

        if len(list_of_arrays) == 0:
            val = list_of_arrays
        elif len(list_of_arrays) == 1:
            val = list_of_arrays[0]
        else:
            val = np.sum(np.asarray(np.meshgrid(*list_of_arrays)), axis=0)

        return val

    @property
    def PES(self):
        """
        Returns the potential energy surface of the system. By default, returns the
        diabatic PES. To get adiabatic PES, use the .gen_pes() method.
        """

        if self._pes is None:
            self.gen_pes()

        return self._pes


# # TODO Finish building
class NumericalSystemParameter(PyQDParameter):
    """
    This should hold potential parameters, the units(?) and the grid parameters.
    Takes a wrapper-prepared dictionary to fill
    """

    INPUT_STR_ID = "numerical_system"

    __slots__ = [
        "gmin",
        "gmax",
    ]

    UNITS = {
        "gmin": ("angstrom", "bohr"),
        "gmax": ("angstrom", "bohr"),
    }

    def __init__(self, **kwargs):

        self.gmin = None
        self.gmax = None

        super().__init__(**kwargs)
