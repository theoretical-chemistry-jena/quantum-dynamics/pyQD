# -*- coding: utf-8 -*-

""" TimeParameter class for pyQD """

import logging
from dataclasses import asdict
from functools import partial

import attr
import numpy as np

from ..exceptions import PyQDInputError
from ..utils.convert import _unit_converter
from ._base import PyQDParameter
from .defaults import Base, Time

logger = logging.getLogger(__name__)


@attr.s(slots=True)
class TimeParameter(PyQDParameter):
    """Parameter building block storing the relevant time parameters."""

    DEFAULT_CLS = Time
    INPUT_STR_ID = "time"
    UNITS = {
        "t_init": ("fs", "a_u_time"),
        "t_final": ("fs", "a_u_time"),
    }

    t_init: float = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["t_init"][-1])
    )
    t_final: float = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["t_final"][-1])
    )
    n_dt: int = attr.ib()

    _tgrid: np.ndarray = attr.ib(default=None, init=False)
    _dtt: float = attr.ib(default=None, init=False)

    @classmethod
    def from_defaults(cls, base: Base, **kwargs):  # pylint: disable=arguments-differ
        """
        Method to populate the class from the corresponding dataclass.
        """
        basekw = asdict(base)
        inputs = asdict(cls.DEFAULT_CLS(**kwargs))
        return cls(**basekw, **inputs)

    @property
    def tgrid(self):
        """
        Returns the discretized time grid.
        """
        if self._tgrid is None:

            if self.t_init == self.t_final:
                raise PyQDInputError("Initial and final times are the same. Exiting.")

            self._tgrid = np.linspace(self.t_init.m, self.t_final.m, num=self.n_dt)

        return self._tgrid

    @property
    def dtt(self):
        """
        Returns the discretized time step size.
        """
        if self._dtt is None:
            self._dtt = float(
                np.abs((self.t_final.m - self.t_init.m) / (self.n_dt - 1))
            )

            if self._dtt == 0.0:
                raise PyQDInputError("Time step has to be non-zero. Exiting.")

        return self._dtt
