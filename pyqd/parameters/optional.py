# -*- coding: utf-8 -*-

""" OptionalParameter class for pyQD """

import datetime
import logging
import string
from dataclasses import asdict
from pathlib import Path
from typing import Union

import attr

from ._base import PyQDParameter
from .defaults import Base, Optional

logger = logging.getLogger(__name__)


@attr.s(slots=True)
class OptionalParameter(PyQDParameter):
    """Helper class holding all the relevant information regarding output, calculation name etc.
    Is technically not necessary for propagation.
    """

    DEFAULT_CLS = Optional
    INPUT_STR_ID = "main"

    output_dir: Union[str, Path] = attr.ib()
    calc_name: str = attr.ib(default=None, kw_only=True)

    nr_frames: int = attr.ib(default=200)
    logfile: Union[None, str, Path] = attr.ib(default=None, kw_only=True)
    debug_mode: bool = attr.ib(default=False)
    append_datetime: bool = attr.ib(default=True)

    _out_directory: Path = attr.ib(default=None, init=False)
    _creation_date: datetime.datetime = attr.ib(
        init=False, default=datetime.datetime.now()
    )

    def __attrs_post_init__(self):
        if self.calc_name is None:
            self.calc_name = self._creation_date.strftime("%Y-%m-%dt%H-%M-%S")

    @classmethod
    def from_defaults(
        cls, base: Base, output_dir: Union[str, Path], calc_name: str, **kwargs
    ):  # pylint: disable=arguments-differ
        """
        Method to populate the class from the corresponding dataclass.
        """
        basekw = asdict(base)
        inputs = asdict(
            cls.DEFAULT_CLS(**kwargs, output_dir=output_dir, calc_name=calc_name)
        )
        return cls(**basekw, **inputs)

    def _create_outputdir(self):
        """
        Safely creates the requested output directory. Has a fallback mechanism.
        """

        date = self._creation_date.strftime("%Y-%m-%dt%H-%M-%S")
        path = Path(self.output_dir).expanduser().resolve() / self.calc_name

        # Optionally, append the current datetime
        if self.append_datetime:
            path /= date

        # Try to make the path.
        try:
            path.mkdir(parents=True, exist_ok=False)
        # Otherwise, generate a new path.
        except FileExistsError as e:

            for letter in string.ascii_letters:
                try:
                    npath = path / letter
                    npath.mkdir(parents=True, exist_ok=False)
                    break
                except FileExistsError:
                    pass
            else:
                raise FileExistsError(
                    "Could not create a suitable output directory."
                ) from e

            path = npath

        return path

    def filename(self, mode, suffix="h5", use_datetime_if_not_calcname=True) -> str:
        """
        Return an appropiate file name for the output files.
        """

        if self.calc_name is not None:
            calc_n = self.calc_name

        elif use_datetime_if_not_calcname:
            formatter = "%Y-%m-%dt%H-%M-%S"
            calc_n = self._creation_date.strftime(formatter)

        else:
            calc_n = "calculation"

        return f"pyQD.{mode}.{calc_n}.{suffix}"

    @property
    def outdir(self):
        """Return the output directory as a pathlib.Path object."""
        if self._out_directory is None:
            self._out_directory = self._create_outputdir()

        return self._out_directory
