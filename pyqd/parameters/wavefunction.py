# -*- coding: utf-8 -*-

""" WavefunctionParameter class for pyQD """

import logging
from dataclasses import asdict
from functools import partial
from typing import TYPE_CHECKING, List, Union

import attr
import numpy as np
import pint

from ..utils.convert import (
    _itp_state_validator,
    _list_converter,
    _state_converter,
    _unit_converter,
)
from ..utils.physics import calc_norm_nd, gaussian_nd_nonnorm
from ._base import PyQDParameter
from .defaults import Base, Wavefunction

if TYPE_CHECKING:
    from .system import AnalyticalSystemParameter


logger = logging.getLogger(__name__)


@attr.s(slots=True)
class WavefunctionParameter(PyQDParameter):
    """
    Wavefunction & propagation parameters.
    Should not be a np.ndarray subclass.
    """

    DEFAULT_CLS = Wavefunction
    INPUT_STR_ID = "wavefunction"
    UNITS = {
        "itp_thresh": ("eV", "hartree"),
        "init_position": ("angstrom", "bohr"),
        "init_sigma": ("angstrom", "bohr"),
    }

    itp_states: int = attr.ib()
    itp_relax_states: List[int] = attr.ib(
        converter=_state_converter, validator=_itp_state_validator
    )
    rtp_init_states: List[int] = attr.ib(
        converter=_state_converter, validator=_itp_state_validator
    )

    # attribs with units
    itp_thresh: Union[pint.Quantity, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["itp_thresh"][-1])
    )
    init_position: List[Union[pint.Quantity, str]] = attr.ib(
        converter=[
            _list_converter,
            partial(_unit_converter, unit=UNITS["init_position"][-1]),
        ]
    )
    init_sigma: List[Union[pint.Quantity, str]] = attr.ib(
        converter=[
            _list_converter,
            partial(_unit_converter, unit=UNITS["init_sigma"][-1]),
        ]
    )

    # Optionals. ITP requested is usually assumed.
    itp_requested: bool = attr.ib(default=True)

    # Internal copy of the .itp_relax_states variable to introduce changes.
    itp_relax_idxs = attr.ib(default=None, init=False)

    def __attrs_post_init__(self):
        # Fix the initial length
        if len(self.init_sigma) == 1 and self.n_m > 1:
            self.init_sigma = self.init_sigma * self.n_m

        if len(self.init_position) == 1 and self.n_m > 1:
            self.init_position = self.init_position * self.n_m

        self.itp_relax_idxs = np.asarray(self.itp_relax_states)

        # TODO Look into this. This seems to throw warnings.
        if np.all(self.itp_relax_idxs != np.arange(self.n_s)):
            self._correct_itp_relax_states()

    # Single-use validators
    @itp_states.validator
    def _itp_states_validator(self, attribute, value):
        if value <= 0:
            raise ValueError(
                f"Tried to set {attribute}, but: number of states must be positive integer."
            )

    @init_sigma.validator
    def _itp_sigma_validator(self, attribute, value):  # pylint: disable=unused-argument
        if not isinstance(value, (list, tuple)):
            value = [value]
        if any([val.m <= 0.0 for val in value]):
            raise ValueError("Sigmas have to be positive, non-zero float values.")

    def _correct_itp_relax_states(self):
        # Correct the state indices for reduced dimensions
        states = range(self.n_s)
        for state in states:
            if state not in self.itp_relax_states:
                self.itp_relax_idxs -= 1
            else:
                break

    @classmethod
    def from_defaults(cls, base: Base, **kwargs):  # pylint: disable=arguments-differ
        """
        Method to populate the class from the corresponding dataclass.
        """
        basekw = asdict(base)
        inputs = asdict(cls.DEFAULT_CLS(**kwargs))
        return cls(**basekw, **inputs)

    def initialize_wavefunction(
        self,
        gridparam: "AnalyticalSystemParameter",
        randomize=False,
        reduced_dim=False,
    ):
        """Initializes a new wavefunction based on the parameters stored in the instance.

        Args:
            gridparam (AnalyticalSystemParameter): AnalyticalSystemParameter instances that
                holds the information on the system.
            randomize (bool, optional): Whether to randomize the wavefunction. Useful for ITP.
                Defaults to False.
            reduced_dim (bool, optional): Reduce the dimension of the wavefunction to only the
                initial states. Useful for performance. Defaults to False.
        Returns:
            [type]: [description]
        """

        mgrid = np.meshgrid(*([gridparam.xgrid] * self.n_m))

        dim = (
            *(mgrid[0].shape),
            self.n_s if not reduced_dim else len(self.itp_relax_states),
        )
        wf = np.zeros(dim, dtype=np.complex128)

        # If multiple initial states are given
        norm_fact = float(len(self.itp_relax_idxs))

        centers = [i.m for i in self.init_position]
        sigmas = [i.m for i in self.init_sigma]

        for state in self.itp_relax_idxs:
            wf[..., state] = gaussian_nd_nonnorm(
                mgrid, centers, sigmas, 1.0, randomize=randomize
            )

            norm = calc_norm_nd(wf[..., int(state)], gridparam.dx, self.n_m)
            wf[..., state] /= np.sqrt(norm_fact * norm)

        return wf
