# -*- coding: utf-8 -*-
"""
Default values for pyQD simulations, packaged as dataclasses.
At the moment, you're advised to use the defaults, modify the
parameters to your liking and then, using `dataclasses.asdict(p)`,
get the data as dict.
"""

# py lint: disable=invalid-name

import typing
from dataclasses import dataclass, field
from typing import Dict, List, Union

import pint


@dataclass
class Base:
    """Base defaults"""

    n_g: int = 256
    n_m: int = 2
    n_s: int = 3


@dataclass
class Optional:
    """Optional defaults"""

    calc_name: str
    output_dir: str
    nr_frames: int = 500
    debug_mode: bool = False
    append_datetime: bool = True
    logfile: typing.Optional[str] = None


@dataclass
class Time:
    """Simulation time defaults"""

    t_init: Union[pint.Quantity, float, str] = "-500 fs"
    t_final: Union[pint.Quantity, float, str] = "1200 fs"
    n_dt: int = 85000


@dataclass
class AnalyticalSystem:
    """Analyical system defaults"""

    gmin: Union[pint.Quantity, float, str] = -380
    gmax: Union[pint.Quantity, float, str] = 380
    omega: Union[pint.Quantity, float, str] = "0.175 eV"
    en_gs: Union[pint.Quantity, float, str] = "0.0 eV"
    en_es: Union[pint.Quantity, float, str] = "2.0 eV"
    pos_gs: Union[pint.Quantity, float, str] = 0
    pos_es: Union[pint.Quantity, float, str] = 20
    mass: Union[pint.Quantity, float, str] = 1.0
    adiabatic: typing.Optional[bool] = False
    adiabatic_interactors: typing.Optional[List] = None
    trans_dipole: Dict = field(default_factory=lambda: {"0->1": 1.0, "0->2": 1.0})


@dataclass
class Wavefunction:
    """Wavefunction defaults"""

    itp_requested: bool = True
    itp_states: str = 1
    itp_thresh: Union[pint.Quantity, float, str] = "1.0e-12 Eh"
    rtp_init_states: List[int] = field(default_factory=lambda: [0])
    itp_relax_states: List[int] = field(default_factory=lambda: [0])
    init_position: Union[pint.Quantity, float, str] = "0.0 angstrom"
    init_sigma: Union[pint.Quantity, float, str] = "0.3 angstrom"
