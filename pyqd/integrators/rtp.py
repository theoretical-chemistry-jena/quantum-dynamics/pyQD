# -*- coding: utf-8 -*-

# pylint: disable=too-many-lines

""" pyQD RTPIntegrator class """

import datetime
import json
import logging
import shutil
from pathlib import Path
from timeit import default_timer

import attr
import numpy as np
import opt_einsum as oe

from ..exceptions import PyQDInputError
from ..interactors import (
    DampingInteractor,
    DummyInteractor,
    FieldParameter,
    JCouplingParameter,
    LegacyPerturbationParameter,
    NanoparticleParameter,
    StaticInteractor,
)
from ..observables import (
    CorrelationSpectrum,
    EigenfunctionCoefficients,
    MomentumEV,
    NormObservable,
    PolarisationSpectrum,
    PositionEV,
    ReducedDensitySpectrum,
    TotalEnergyObservable,
)
from ..parameters import (
    AnalyticalSystemParameter,
    OptionalParameter,
    TimeParameter,
    WavefunctionParameter,
)
from ..utils.convert import interactor_key
from ..utils.io import observables_from_file
from ..utils.physics import build_einsum_op, build_exec_order_recursive
from ._base import QDIntegrator
from .itp import ITPIntegrator


@attr.s(slots=True)
class RTPIntegrator(QDIntegrator):
    """
    This class implements a Split-Operator propagation in real time $t$
    (in contrast to imaginary time $i\\tau$).
    """

    observable_mapping = {
        "norm": NormObservable,
        "total_energy": TotalEnergyObservable,
        "spectrum_polarisation": PolarisationSpectrum,
        "spectrum_correlation": CorrelationSpectrum,
        "spectrum_reduced_density": ReducedDensitySpectrum,
        "eigenfunction_coefficients": EigenfunctionCoefficients,
        "momentum": MomentumEV,
        "position": PositionEV,
    }

    interactors: list = attr.ib(default=lambda x: list())  # The interactors
    observables: dict = attr.ib(
        default=lambda x: dict()
    )  # The observables as a dict with params
    itp_calctr = attr.ib(default=None, kw_only=True)

    _td_interactors = attr.ib(default=None, init=False)
    _td_interactions = attr.ib(default=None, init=False)
    _pot_expr = attr.ib(default=None, init=False)  # The V_e generator expression
    _t_op = attr.ib(default=None, init=False)
    _v_raw = attr.ib(default=None, init=False)
    _u_eff = attr.ib(default=None, init=False)
    _v_op = attr.ib(default=None, init=False)
    _nth_cycle = attr.ib(default=None, init=False)
    _prop_order = attr.ib(default=None, init=False)
    _observable_objs = attr.ib(default=None, init=False)
    _norm_obs = attr.ib(default=None, init=False)

    def __attrs_post_init__(self):
        self._logger = logging.getLogger(__name__)

    @classmethod
    def from_file(
        cls, file, include_norm=True, itp_calctr=None, **kwargs
    ):  # pylint: disable=arguments-differ
        """
        Allows to populate the entire class including parameters from a pyQD input file.
        """

        ikwargs = dict(itp_calctr=itp_calctr, **kwargs)

        ikwargs["potential"] = AnalyticalSystemParameter.from_file(file)
        ikwargs["timeparam"] = TimeParameter.from_file(file)
        ikwargs["wfparam"] = WavefunctionParameter.from_file(file)
        ikwargs["optional"] = OptionalParameter.from_file(file)

        ikwargs["n_m"] = ikwargs["timeparam"].n_m
        ikwargs["n_s"] = ikwargs["timeparam"].n_s
        ikwargs["n_g"] = ikwargs["timeparam"].n_g

        interactors = []

        # Interactors
        for obj in [
            LegacyPerturbationParameter,
            JCouplingParameter,
            StaticInteractor,
            DampingInteractor,
            FieldParameter,
            NanoparticleParameter,
        ]:
            fidx = 0
            while True:
                try:
                    interactors.append(obj.from_file(file, idx=fidx))
                    fidx += 1

                except KeyError:
                    logging.debug("Encountered KeyError")
                    break

                except IndexError:
                    logging.debug("Encountered IndexError")
                    break

        # Observables
        observables = observables_from_file(file, include_norm=include_norm)

        ikwargs["interactors"] = interactors
        ikwargs["observables"] = observables

        return cls(**ikwargs)

    @property
    def norm(self):
        """Easy access to the norm observable"""
        return self._norm_obs

    def _prepare(self, **kwargs):

        # Populate the ITP integrator if requested
        if self.itp_calctr is None and self.wfparam.itp_requested:
            self.itp_calctr = ITPIntegrator(
                **{
                    "n_s": self.n_s,
                    "n_g": self.n_g,
                    "n_m": self.n_m,
                    "potential": self.potential,
                    "timeparam": self.timeparam,
                    "wfparam": self.wfparam,
                    "optional": self.optional,
                }
            )
            self.itp_calctr.propagate_all()

        # Prepare from possible ITP:
        if self.itp_calctr is not None:

            # Set up wavefunction
            self._wf = np.zeros([self.n_g] * self.n_m + [self.n_s], dtype=np.complex128)

            # If the ITP has not yet been executed
            if not self.itp_calctr.relaxed:
                self.itp_calctr.propagate_all()

            # If the wf is relaxed and the RTP and ITP states match
            if self.wfparam.rtp_init_states == self.wfparam.itp_relax_states:
                self._wf[..., self.wfparam.rtp_init_states] = self.itp_calctr.lower[
                    ..., self.wfparam.rtp_init_states, 0
                ]

            # If the wf is relaxed, but it has to be projected to different states.
            # Currently only possible if the ITP state is a single state.
            elif (
                len(self.wfparam.rtp_init_states) > len(self.wfparam.itp_relax_states)
                and len(self.wfparam.itp_relax_states) == 1
            ):
                # New wavefunction
                new_wf = np.zeros(
                    [self.n_g] * self.n_m + [self.n_s], dtype=np.complex128
                )
                new_wf[..., self.wfparam.rtp_init_states] = self.itp_calctr.lower[
                    ..., self.wfparam.itp_relax_states, 0
                ] / np.sqrt(len(self.wfparam.rtp_init_states))

                self._wf = new_wf

            else:
                raise NotImplementedError(
                    "The combination of RTP and ITP "
                    "states with {} and {} states, "
                    "resp. is not implemented.".format(
                        len(self.wfparam.rtp_init_states),
                        len(self.wfparam.itp_relax_states),
                    )
                )

        # If there is no ITPIntegrator
        else:
            self._wf = self.wfparam.initialize_wavefunction(self.potential)

        # Prepare expressions
        self._fftf, self._fftb = self._get_fftw()

        # Prepare propagator
        self._prepare_hamiltonian(**kwargs)

        # Prepare observables
        self._prepare_observables()

        # Prepare files
        self._tmpfile, self._h5file = self._get_tmpfile("ITP")
        self._h5wf = self._h5file.create_group("wf")

        # Frame output
        nr_frames = 500 if self.optional.nr_frames is None else self.optional.nr_frames
        self._nth_cycle = max(1, len(self._step_iterable) // nr_frames)

    def _prepare_hamiltonian(self, backend="numpy"):

        # Sort interactors. Lowest priority goes last
        self.interactors = sorted(self.interactors, key=interactor_key, reverse=True)

        # Which interactors and interactions are actually time-dependent?
        self._td_interactors = [False] + [ia.TIME_DEPENDENT for ia in self.interactors]

        # Add a dummy interactor in case there is no time-dependent interactor present.
        if np.sum(self._td_interactors) == 0:
            self.interactors.append(
                DummyInteractor(**{"n_s": self.n_s, "n_m": self.n_m, "n_g": self.n_m})
            )
            self._td_interactors.append(True)

        self._td_interactions = [False] + [
            ia.TIME_DEPENDENT for ia in self.interactors for _ in range(ia.num)
        ]

        # Get number of propagators & the splits
        nr_interactions = 1 + sum([obj.num for obj in self.interactors])
        splits = [2 ** n for n in range(nr_interactions + 1)]

        # Distribute the necessary splits
        i_idx = 2
        for interactor in self.interactors:
            f_idx = i_idx + interactor.num

            interactor.splits = splits[i_idx:f_idx]
            self._logger.info(
                "Interactor {} got indices {}".format(
                    interactor.__class__, splits[i_idx:f_idx]
                )
            )

            i_idx = f_idx

        # ! Build propagators
        # Kinetic propagator and Hamiltonian
        self._tprop, self._t_op = self.tprop(return_en=True)

        # Potential energy propagator and Hamiltonian
        vprop, self._v_raw = self.vprop(return_en=True)

        self._v_op = None

        # Propagator and energy matrices
        matrices_en = [
            obj.all_matrices(
                self.timeparam.t_init.m, self.timeparam.dtt, print_e="matrix"
            )
            for obj in self.interactors
        ]

        # Unzip the list of tuples
        matrices, hamiltonian_items = list(zip(*matrices_en))

        # Flatten the list
        matrices = [sm for mats in matrices for sm in mats]

        # Update the V Hamiltonian with time-independent components
        for idx, ham in enumerate(hamiltonian_items):
            if not self._td_interactions[idx + 1]:
                self._v_raw += ham

        # Add the Potential Energy Propagator to the full list of propagators
        matrices = [vprop] + matrices

        # The ordering for one potential interactor
        self._prop_order = build_exec_order_recursive(nr_interactions)
        operation = build_einsum_op(len(self._prop_order))

        shape_or_array = [
            matrices[idx].shape if self._td_interactions[idx] else matrices[idx]
            for idx in self._prop_order
        ]

        # Filter constant indices for the opt_einsum optimisation
        consts = [
            ordidx
            for ordidx, matidx in enumerate(self._prop_order)
            if not self._td_interactions[matidx]
        ]

        # If debug mode is enabled, do not perform parallel optimisation
        debugmode = getattr(self.optional, "debug_mode", False)

        self._oebackend = backend

        optimizer = oe.RandomGreedy(parallel=not debugmode, max_repeats=128)

        # Contract the propagator expression
        shapes = [self._tprop.shape, self._wf.shape]
        self._pr_expr = self._prepare_propg_expr(shapes)

        # Contract the "propagator generator"
        self._pot_expr = oe.contract_expression(
            operation, *shape_or_array, constants=consts, optimize=optimizer
        )

        # Initialise the  step iterable / PCOUNTER, which here is "just" the time grid
        self._step_iterable = self.timeparam.tgrid
        self._dtt = self.timeparam.dtt

        return None

    def _prepare_observables(self):
        # Initialise observables
        self._observable_objs = []
        obs_kwargs = {
            "n_m": self.n_m,
            "n_s": self.n_s,
            "n_g": self.n_g,
            "dx": self.potential.dx,
        }

        # Preparation kwargs for the observables
        prep_kwargs = {
            "tgrid": self._step_iterable,
            "tgridsize": len(self._step_iterable),
            "xgrid": self.potential.xgrid,
            "fft_f": self._fftf,
            "mu_input": self.potential.mu,
        }

        itp_cond = any(
            [
                item in self.observables
                for item in ["spectrum_correlation", "eigenfunction_coefficients"]
            ]
        )
        if itp_cond:
            try:
                prep_kwargs.update({"itp_integrator": self.itp_calctr})

            except AttributeError as e:
                raise PyQDInputError(
                    f"Correlation spectra require relaxed ITP. Error message: {e}"
                ) from e

        if "spectrum_polarisation" in self.observables:
            fields = list(
                filter(lambda x: isinstance(x, FieldParameter), self.interactors)
            )
            pol_field = fields[self.observables["spectrum_polarisation"]["field_idx"]]
            prep_kwargs.update({"t_init": pol_field.t_central})

        self._logger.debug(
            "The preparation kwargs for observables: {}".format(prep_kwargs)
        )

        for obs, kwargs in self.observables.items():
            c_obs = obs.replace("-", "_")
            instance = self.observable_mapping[c_obs](**obs_kwargs).obs_prepare(
                **{**prep_kwargs, **kwargs}
            )
            self._observable_objs.append(instance)

            # Add the NormObservable instance to the convenience property.
            if isinstance(instance, NormObservable):
                self._norm_obs = instance

        for obj in self.interactors:
            obj.obs_prepare(**prep_kwargs)

    def _update_v_hamiltonian(self, td_energies):
        """Update the self._V Hamiltonian"""

        self._v_op = self._v_raw.copy()

        for idx, component in enumerate(td_energies):
            if self._td_interactions[idx]:
                self._v_op += component

        return None

    def _update_u_propagator(self, t):
        new_td_m_c_en = [([None], 0.0)] + [
            ia.all_matrices(t, self._dtt, print_e="matrix")
            if self._td_interactors[ii + 1]
            else ([None] * ia.num, 0.0)
            for ii, ia in enumerate(self.interactors)
        ]

        new_td_m, ens = list(zip(*new_td_m_c_en))

        self._update_v_hamiltonian(ens)

        # Flatten the list
        new_td_m = [sm for mats in new_td_m for sm in mats]

        # Build the new list for the propagator expression.
        ordered_new_m = [
            new_td_m[matidx]
            for matidx in self._prop_order
            if self._td_interactions[matidx]
        ]

        # Update the effective V propagator
        self._u_eff = self._pot_expr(*ordered_new_m, backend=self._oebackend)

        return None

    def _propagate_single(self, t, idx=0):
        # Take the timing for debug output
        t_start = default_timer()

        self._update_u_propagator(t)

        # ! Propagation
        # ! First potential "half step" propagation
        self._pr_expr(self._u_eff, self._wf, out=self._wf, backend=self._oebackend)

        # ! START kinetic propagation
        self._wf = self._fftf(input_array=self._wf)

        self._pr_expr(self._tprop, self._wf, out=self._wf, backend=self._oebackend)

        self._wf = self._fftb(input_array=self._wf, normalise_idft=True)
        # ! END kinetic propagation

        # ! Second potential "half step" propagation
        self._pr_expr(self._u_eff, self._wf, out=self._wf, backend=self._oebackend)
        # ! Propagation finished

        self._update_observables(t, idx)

        self._output_wf(t, idx)

        self._logger.debug(
            "Cycle nr. {}:\tExec. time: {:.3f} ms".format(
                idx, (default_timer() - t_start) * 1e3
            )
        )

        return None

    def _output_wf(self, t, idx):
        if idx % self._nth_cycle == 0:
            dset = self._h5wf.create_dataset(name="{:09d}".format(idx), data=self._wf)
            dset.attrs["t"] = t

        return None

    def _update_observables(self, t, idx):

        obs_update_kwargs = {
            "wf": self._wf,
            "operator_v": self._v_op,
            "operator_t": self._t_op,
        }

        for obj in self._observable_objs + self.interactors:
            obj.obs_update(t, idx, **obs_update_kwargs)

        return None

    def _reconstruct_input(self):
        # Build a input YAML file

        json_data = {}
        for obj in [self.optional, self.potential, self.timeparam, self.wfparam]:
            json_data.update(obj.as_input(exclude_underscore=True))

        for interactor in self.interactors:
            data = interactor.as_input(exclude_underscore=True)
            strid = interactor.INPUT_STR_ID

            if "idx" in data[strid] and strid not in json_data:
                json_data[strid] = []

            if "idx" in data[strid] and strid in json_data:
                json_data[strid].append(data[strid])

            else:
                json_data.update(data)

        obs_dict = {}
        for observable in self._observable_objs:
            obs_dict.update(observable.as_input())

        json_data["observables"] = obs_dict

        filename = self.optional.outdir / self.optional.filename("INPUT", suffix="json")
        with open(filename, "w") as f:
            f.write(json.dumps(json_data, indent=4))

    def _postprocess(self):

        self._reconstruct_input()

        # Parameters for the h5file
        self._h5file.attrs["tparam"] = ", ".join(
            str(i)
            for i in (
                self.timeparam.t_init,
                self.timeparam.t_final,
                self.timeparam.n_dt,
            )
        )

        # Time grid
        self._h5file.create_dataset("tgrid", data=self._step_iterable)

        # Dimension and calculation name
        self._h5file.attrs["n_dim"] = self.n_m
        self._h5file.attrs["calc_name"] = str(
            getattr(self.optional, "calc_name", "None")
        )

        # Some useful output
        self._h5file.create_dataset("xgrid", data=self.potential.xgrid)
        self._h5file.create_dataset("e_pot", data=self.potential.PES)

        # Observable output
        for obj in self._observable_objs + self.interactors:
            obj.obs_to_outfile(self._h5file)

        # ITP data (energies)
        if self.itp_calctr is not None:
            h5itp = self._h5file.create_group("ITP")
            h5itp.create_dataset("energies", data=self.itp_calctr.energies)
            h5itp.create_dataset("opt_wf", data=self.itp_calctr.lower)

        # Close the file
        self._h5file.close()

        # Copy the h5file to the output directory
        if hasattr(self, "optional"):
            path = self.optional.outdir
            f = self.optional.filename(mode="RTP")

        else:
            date = datetime.datetime.now().strftime("%Y-%m-%dt%H-%M-%S")
            path = Path(".").resolve()
            f = f"pyQD.ITP.{date}.h5"

        shutil.move(self._tmpfile.name, path / f)

        self._logger.info("The output file is located under {}".format(path / f))

        return self
