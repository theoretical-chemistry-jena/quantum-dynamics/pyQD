# -*- coding: utf-8 -*-

# pylint: disable=too-many-lines

""" pyQD integrator base class """

import multiprocessing
import tempfile

import attr
import h5py
import numpy as np
import opt_einsum as oe
import pyfftw
from tqdm.auto import tqdm

from ..parameters import (
    AnalyticalSystemParameter,
    OptionalParameter,
    TimeParameter,
    WavefunctionParameter,
)
from ..parameters._base import PyQDParameter
from ..utils.physics import calc_k_grid, calc_norm_nd, calc_norm_nd_statewise


@attr.s(slots=True)
class QDIntegrator(PyQDParameter):
    """
    Base class for the pyQD integrators.
    Implements the important functionality for the subclasses.
    """

    # Overwritable FFTW kwargs
    _FFTWKWARGS = {
        "overwrite_input": True,
        "threads": multiprocessing.cpu_count(),
        "planner_effort": "FFTW_MEASURE",
    }

    potential = attr.ib(
        validator=attr.validators.instance_of(AnalyticalSystemParameter)
    )  # SystemParameter instance
    timeparam = attr.ib(
        validator=attr.validators.instance_of(TimeParameter)
    )  # TimeParameter instance
    wfparam = attr.ib(
        validator=attr.validators.instance_of(WavefunctionParameter)
    )  # WavefunctionParameter instance
    optional = attr.ib(
        validator=attr.validators.instance_of(OptionalParameter)
    )  # The OptionalParameter instance

    _step_iterable = attr.ib(factory=lambda: [])
    _fftwkwargs = attr.ib(factory=lambda: QDIntegrator._FFTWKWARGS)
    _pr_expr = attr.ib(init=False, default=None)  # The propagator expression
    _oebackend = attr.ib(init=False, default=None)
    _fftf = attr.ib(init=False, default=None)
    _fftb = attr.ib(init=False, default=None)
    _wf = attr.ib(init=False, default=None)
    _tprop = attr.ib(init=False, default=None)
    _vprop = attr.ib(init=False, default=None)
    _h5file = attr.ib(init=False, default=None)
    _tmpfile = attr.ib(init=False, default=None)
    _h5wf = attr.ib(init=False, default=None)
    _dtt = attr.ib(init=False, default=None)
    _logger = attr.ib(init=False, default=None)

    def _get_tmpfile(self, descriptor):
        _tmpfile = tempfile.NamedTemporaryFile(
            prefix=f"pyQD.{descriptor}.data_", suffix=".h5", delete=False
        )
        _h5file = h5py.File(_tmpfile, "w")

        self._h5file = _h5file
        self._tmpfile = _tmpfile

        return _tmpfile, _h5file

    def norm_statewise(self):
        """Convenience wrapper around pyQD.utils_physics.calc_norm_nd_statewise()"""
        return calc_norm_nd_statewise(self._wf, self.potential.dx, self.n_m)

    def norm_total(self):
        """Convenience wrapper around pyQD.utils_physics.calc_norm_nd_statewise()"""
        return calc_norm_nd(self._wf, self.potential.dx, self.n_m)

    def _get_fftw(self):
        if hasattr(self, "_wf"):
            if self.n_m == 1:
                fftf = pyfftw.builders.fft(self._wf, axis=0, **self._fftwkwargs)
                fftb = pyfftw.builders.ifft(self._wf, axis=0, **self._fftwkwargs)
            else:
                fftaxes = tuple(range(self.n_m))
                fftf = pyfftw.builders.fftn(self._wf, axes=fftaxes, **self._fftwkwargs)
                fftb = pyfftw.builders.ifftn(self._wf, axes=fftaxes, **self._fftwkwargs)

            return fftf, fftb

        return None, None

    def _add_missing_baseparams(self):
        self.n_m = self.timeparam.n_m
        self.n_s = self.timeparam.n_s
        self.n_g = self.timeparam.n_g

    def _prepare_propg_expr(self, shapes):
        return oe.contract_expression("...ij,...j->...i", *shapes, optimize="dp")

    def tprop(self, itp=False, split=1, reduced_itp=False, return_en=False):
        """Generate the kinetic energy propagator and Hamiltonian."""

        if self._tprop is not None and not return_en:
            return self._tprop

        else:
            if reduced_itp:
                mdim = len(self.wfparam.itp_relax_states)
            else:
                mdim = self.n_s

            shape = [self.n_g] * self.n_m + [mdim] * 2
            mat = np.zeros(shape, dtype=np.complex128)

            kgrids = [calc_k_grid(self.n_g, self.potential.dx)] * self.n_m

            kmgrids = np.asarray(np.meshgrid(*kgrids))

            imag = 1.0 if itp else 1.0j

            strength_t = (1 / (2 * self.potential.mass.m)) * np.sum(
                kmgrids ** 2, axis=0
            )
            values = np.exp(-imag * (self.timeparam.dtt / split) * strength_t)

            for ii in range(mdim):
                mat[..., ii, ii] += values

            self._tprop = mat

            if not return_en:
                return self._tprop

            else:

                operator = np.zeros([*strength_t.shape] + [self.n_s, self.n_s])
                for ii in range(self.n_s):
                    operator[..., ii, ii] = strength_t

                return self._tprop, operator

    def vprop(self, itp=False, split=2, reduced_itp=False, return_en=False):
        """Generate the kinetic energy propagator and Hamiltonian."""

        if self._vprop is not None and not return_en:
            return self._vprop

        else:
            if reduced_itp:
                mdim = len(self.wfparam.itp_relax_states)
                real_states = self.wfparam.itp_relax_states
            else:
                mdim = self.n_s
                real_states = range(mdim)

            shape = [self.n_g] * self.n_m + [mdim] * 2
            mat = np.zeros(shape, dtype=np.complex128)

            pes = self.potential.PES

            imag = 1.0 if itp else 1.0j
            values = np.exp(-imag * (self.timeparam.dtt / split) * pes)

            for idx, state in enumerate(real_states):
                mat[..., idx, idx] += values[..., state]

            self._vprop = mat

        if return_en:
            pes_mat = np.zeros([*pes.shape, self.n_s])
            for ii in range(self.n_s):
                pes_mat[..., ii, ii] = pes[..., ii]

            return self._vprop, pes_mat

        else:
            return self._vprop

    def _propagate_single(self, t, idx=0):
        pass

    def _prepare(self):
        pass

    def _postprocess(self):
        return None

    def propagate_all(self, **kwargs):
        """
        Wrapper function to the propagator methods.
        """
        self._prepare(**kwargs)

        for idx, t in enumerate(tqdm(self._step_iterable, dynamic_ncols=True)):
            self._propagate_single(t, idx)

        return_val = self._postprocess()  # pylint: disable=assignment-from-none

        return return_val
