# -*- coding: utf-8 -*-

# pylint: disable=too-many-lines

""" pyQD ITPIntegrator class """

import datetime
import logging
import shutil
from pathlib import Path
from timeit import default_timer
from typing import List, Optional

import attr
import numpy as np
import opt_einsum as oe

from ..exceptions import PyQDInputError
from ..interactors._base import InteractorParameter
from ..parameters import (
    AnalyticalSystemParameter,
    OptionalParameter,
    TimeParameter,
    WavefunctionParameter,
)
from ._base import QDIntegrator


@attr.s(slots=True)
class ITPIntegrator(QDIntegrator):
    """
    Imaginary time propagator class. Useful to generate eigenstate wavefunctions
    of an arbitrary Hamiltonian (e.g. a numerically generated PES). Works by
    propagating 'in imaginary time $t = i\\tau$', making the propagators exponential functions
    that damp higher order contributions faster than lower energy eigenstates, eventually leaving
    only the lowest eigenstate.
    """

    coupling: Optional[InteractorParameter] = attr.ib(default=None, kw_only=True)

    _use_coupling: bool = attr.ib(default=False, kw_only=True)
    _cprop: np.ndarray = attr.ib(default=None, init=False)
    _cexpr = attr.ib(default=None, init=False)

    _normaliser = attr.ib(default=None, init=False)
    _relaxed: bool = attr.ib(default=False)
    _lower: List[np.ndarray] = attr.ib(default=None, init=False)
    _energies = attr.ib(default=None, init=False)
    _op = attr.ib(default=None, init=False)
    _l_expr = attr.ib(default=None, init=False)

    def __attrs_post_init__(self):
        self._logger = logging.getLogger(__name__)

        if self.coupling is not None:
            self._use_coupling = True

    @classmethod
    def from_file(cls, file, **kwargs):  # pylint: disable=arguments-differ
        """Adds functionality to populate the entire instance from a pyQD input file."""

        ikwargs = {**kwargs}

        ikwargs["potential"] = AnalyticalSystemParameter.from_file(file)
        ikwargs["timeparam"] = TimeParameter.from_file(file)
        ikwargs["wfparam"] = WavefunctionParameter.from_file(file)
        ikwargs["optional"] = OptionalParameter.from_file(file)

        ikwargs["n_m"] = ikwargs["timeparam"].n_m
        ikwargs["n_s"] = ikwargs["timeparam"].n_s
        ikwargs["n_g"] = ikwargs["timeparam"].n_g

        return cls(**ikwargs)

    @property
    def lower(self):
        """Return the generated eigenstate wavefunctions."""

        if self._lower is not None:
            return self._lower

    @property
    def energies(self):
        """Return the generated eigenstate energies."""

        if self._energies is not None:
            return self._energies

    @property
    def relaxed(self):
        """Return the relaxation state of the instance."""
        return self._relaxed

    def cprop(self, idx=0, split=4):
        """Introduce a coupling to the ITP."""
        mat = np.eye(len(self.wfparam.itp_relax_idxs), dtype=np.complex128)

        _, strength = self.coupling._matrix(  # pylint: disable=protected-access
            t=0.0, dtt=self.timeparam.dtt, idx=idx, energy_only=True
        )

        idxs = self.coupling.pairs[idx]
        if not all(i in self.wfparam.itp_relax_states for i in idxs):
            raise PyQDInputError("Coupling interactor does not suit the ITP settings.")

        if list(self.wfparam.itp_relax_idxs) == list(self.wfparam.itp_relax_states):
            jlow, jup = idxs
        else:
            jlow, jup = 0, 1

        dtt = -1.0j * self.timeparam.dtt

        mat[jlow, jlow] = mat[jup, jup] = np.cos((dtt / split) * strength)
        mat[jup, jlow] = mat[jlow, jup] = -1j * np.sin(strength * dtt * (1.0 / split))

        return mat

    def _prepare(self, **kwargs):

        # Capital letters denote an important attribute that has to be implemented
        self._step_iterable = list(range(self.wfparam.itp_states))

        # Prepare wavefunction
        self._wf = self.wfparam.initialize_wavefunction(
            self.potential, randomize=True, reduced_dim=True
        )

        # Generate the propagators --> stored in self._tprop & self._vprop
        self._tprop = self.tprop(itp=True, reduced_itp=True)
        self._vprop = self.vprop(itp=True, reduced_itp=True)

        if self._use_coupling:
            self._normaliser = self.norm_statewise
            self._cprop = self.cprop()
            self._cexpr = self._prepare_propg_expr(
                shapes=[self._cprop.shape, self._wf.shape]
            )

        else:
            self._normaliser = self.norm_statewise

        # Generate the FFTW builders
        self._fftf, self._fftb = self._get_fftw()
        self._oebackend = kwargs.get("backend", "numpy")

        # Generate the opt_einsum expression
        shapes = [self._tprop.shape, self._wf.shape]
        self._pr_expr = self._prepare_propg_expr(shapes)

        self._tmpfile, self._h5file = self._get_tmpfile("ITP")
        self._h5wf = self._h5file.create_group("wf")

        self._lower = None
        self._op = "...ji,...j->ji"
        self._energies = []

        return self

    def _propagate_single(self, t, idx=0):
        """
        In ITP, one single propagation is a single ITP vibr. state
        """

        cycle = 0

        if not self._use_coupling:
            e_final = np.asarray([1.0] * len(self.wfparam.itp_relax_states))
            e_start = np.asarray([100.0] * len(self.wfparam.itp_relax_states))
        else:
            e_final = 1.0
            e_start = 100.0

        # import pdb; pdb.set_trace()
        if t > 0:
            self._wf = self.wfparam.initialize_wavefunction(
                self.potential, randomize=True, reduced_dim=True
            )

        # ! Propagate the wavefunction:
        while np.any(np.abs(e_start - e_final) > self.wfparam.itp_thresh.m):
            t_start = default_timer()

            cycle += 1
            e_start = e_final

            if self._lower is not None:
                ovlps = (
                    self._l_expr(self._wf, backend=self._oebackend)
                    * self.potential.dx ** self.n_m
                )
                self._wf -= (1.0 + 0.0j) * np.real(np.sum(ovlps * self._lower, axis=-1))

            # TODO: Proper refactoring.

            # * First quarter coupling
            if self._use_coupling:
                self._wf = self._cexpr(self._cprop, self._wf, backend=self._oebackend)

            # ! First potential "half step" propagation
            self._wf = self._pr_expr(self._vprop, self._wf, backend=self._oebackend)

            # * Second quarter coupling
            if self._use_coupling:
                self._wf = self._cexpr(self._cprop, self._wf, backend=self._oebackend)

            # ! START kinetic propagation
            self._wf = self._fftf(input_array=self._wf)

            self._wf = self._pr_expr(self._tprop, self._wf, backend=self._oebackend)

            self._wf = self._fftb(input_array=self._wf, normalise_idft=True)
            # ! END kinetic propagation

            # * Third quarter coupling
            if self._use_coupling:
                self._wf = self._cexpr(self._cprop, self._wf, backend=self._oebackend)

            # ! Second potential "half step" propagation
            self._wf = self._pr_expr(self._vprop, self._wf, backend=self._oebackend)

            # * Last quarter coupling
            if self._use_coupling:
                self._wf = self._cexpr(self._cprop, self._wf, backend=self._oebackend)

            # Re-normalise the wavefunction (deletes the imaginary part):
            self._wf = (1.0 + 0.0j) * np.real(self._wf)

            # Does that make sense?
            if self._use_coupling:
                norm = np.sum(self._normaliser())
            else:
                norm = self._normaliser()

            e_final = -(1.0 / (2.0 * self.timeparam.dtt)) * np.log(norm)

            self._wf /= np.sqrt(norm)

            # n_2 = self._normaliser()

            # self._logger.info(
            #     "VS: {state}, Cycle: {cc}, Curr. Energy: {e_final:.5e}, "
            #     "Delta: {delta:.5e}, Norm (after renorm): {norm}".format(
            #         state=t,
            #         cc=cycle,
            #         e_final=float(e_final),
            #         delta=(e_start - e_final),
            #         norm=n_2,
            #     )
            # )

            self._logger.debug(
                "Cycle nr. {}:\tExec. time: {:.3f} ms".format(
                    cycle, (default_timer() - t_start) * 1e3
                )
            )

        if self._lower is None:
            self._lower = self._wf.copy()[..., np.newaxis]

        else:
            self._lower = np.concatenate(
                (self._lower, self._wf.copy()[..., np.newaxis]), axis=-1
            )

        self._l_expr = oe.contract_expression(
            self._op,
            *(self._lower, self._wf.shape),
            constants=[0],
            optimize="dp",
        )

        self._energies.append(e_final)

        h5c = self._h5wf.create_dataset(name="{:03d}".format(t), data=self._wf)
        h5c.attrs["total_energy"] = e_final

    def _postprocess(self):

        self._relaxed = True

        self._h5file.create_dataset(name="xgrid", data=self.potential.xgrid)

        self._h5file.close()

        if self.optional is not None:
            path = self.optional.outdir
            f = self.optional.filename(mode="ITP")

        else:
            date = datetime.datetime.now().strftime("%Y-%m-%dt%H-%M-%S")
            path = Path(".")
            f = f"pyQD.ITP.{date}.h5"

        shutil.move(self._tmpfile.name, path / f)

        return self
