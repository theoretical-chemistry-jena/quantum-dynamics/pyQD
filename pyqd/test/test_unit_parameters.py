# -*- coding: utf-8 -*-
"""Unit tests for the pyQD.observables module."""

import logging

# pylint: disable=redefined-outer-name,
from pathlib import Path

import numpy as np
import pint
import pytest

from ..parameters import (
    AnalyticalSystemParameter,
    OptionalParameter,
    TimeParameter,
    WavefunctionParameter,
)
from ..utils.physics import calc_norm_nd

ureg = pint.UnitRegistry()


@pytest.mark.parametrize(  # pylint: disable=not-callable
    "param_class",
    [
        OptionalParameter,
        AnalyticalSystemParameter,
        WavefunctionParameter,
        TimeParameter,
    ],
)
def test_parameters_from_file(param_class, infile):
    """Testing the Parameter by instantiating from files."""
    # TODO
    param_class.from_file(infile)


@pytest.mark.parametrize(  # pylint: disable=not-callable
    "input_dict,expected_filename",
    [
        (
            {
                "output_dir": ".pytest_output/unit_param_opt_1",
                "calc_name": "pytest_opt",
                "debug_mode": True,
                "append_datetime": True,
                "logfile": False,
                "nr_frames": 10,
            },
            "pyQD.TEST.pytest_opt.h5",
        ),
        (
            {
                "output_dir": ".pytest_output/unit_param_opt_2",
                "calc_name": "pytest_opt",
                "debug_mode": True,
                "append_datetime": False,
                "logfile": False,
                "nr_frames": 10,
            },
            "pyQD.TEST.pytest_opt.h5",
        ),
        pytest.param(
            {
                "output_dir": ".pytest_output/unit_param_opt_2",
                "debug_mode": True,
                "append_datetime": False,
                "logfile": False,
                "nr_frames": 10,
            },
            "pyQD.TEST.calculation.h5",
            marks=pytest.mark.xfail,
        ),
        pytest.param(
            {
                "output_dir": ".pytest_output/unit_param_opt_3",
                "debug_mode": True,
                "logfile": False,
                "nr_frames": 10,
            },
            "pyQD.TEST.calculation.h5",
            marks=pytest.mark.xfail,
        ),
        pytest.param(
            {
                "output_dir": ".pytest_output/unit_param_opt_minimal",
            },
            "pyQD.TEST.calculation.h5",
            marks=pytest.mark.xfail,
        ),
    ],
)
def test_optional_parameter_from_dict(input_dict, expected_filename, base_kwargs):
    """Testing the OptionalParameter class by filling from dicts"""
    input_dict.update(base_kwargs)

    instance = OptionalParameter(**input_dict)

    # assert instance.is_complete() == completeness

    # Testing the outdir capabilities
    assert isinstance(instance.outdir, Path)
    assert instance.outdir.exists()

    # Testing the filename capabilities
    fname = instance.filename("TEST", use_datetime_if_not_calcname=False)
    assert fname == expected_filename

    # Date regex: r"[0-9]{4}(-[0-9]{2}){2}t([0-9]{2}-){2}[0-9]{2}"


def test_optional_parameter_from_file_multidir(infile):
    """
    Testing the OptionalParameter class by filling from file and testing the
    capabilities to generate subdirectories in case of existing dirs.
    """

    # Quickly generate the same output dir multiple times
    for _ in range(3):
        instance = OptionalParameter.from_file(infile)

        # Testing the outdir capabilities
        assert isinstance(instance.outdir, Path)
        assert instance.outdir.exists()


@pytest.mark.parametrize(  # pylint: disable=not-callable
    "input_dict,str_kwargs",
    [
        (
            {
                "t_init": -50,
                "t_final": 50,
                "n_dt": 1001,
            },
            False,
        ),
        (
            {
                "t_init": -50,
                "t_final": 500,
                "n_dt": 100001,
            },
            False,
        ),
        (
            {
                "t_init": "-50 fs",
                "t_final": "50 fs",
                "n_dt": 10001,
            },
            True,
        ),
        (
            {
                "t_init": "-50 ns",
                "t_final": "50 ns",
                "n_dt": 10001,
            },
            True,
        ),
        pytest.param(
            {
                "t_init": -50,
                "n_dt": 1001,
            },
            False,
            marks=pytest.mark.xfail,
        ),
        pytest.param(
            {
                "t_init": 0,
                "t_final": 0,
                "n_dt": 1001,
            },
            False,
            marks=pytest.mark.xfail,
        ),
        pytest.param(
            {
                "t_init": -50,
                "t_final": 50,
                "n_dt": 0,
            },
            False,
            marks=pytest.mark.xfail,
        ),
    ],
)
def test_time_parameter_from_dict(base_kwargs, input_dict, str_kwargs):
    """Testing the TimeParameter class by instantiating from dicts"""
    input_dict.update(base_kwargs)

    instance = TimeParameter(**input_dict)
    # assert instance.is_complete() == completeness

    tgrid = instance.tgrid
    dtt = instance.dtt

    if not str_kwargs:
        test_tgrid = np.linspace(
            input_dict["t_init"], input_dict["t_final"], num=input_dict["n_dt"]
        )
    else:
        t_in = ureg.Quantity(input_dict["t_init"]).to("a_u_time").m
        t_fin = ureg.Quantity(input_dict["t_final"]).to("a_u_time").m

        test_tgrid = np.linspace(t_in, t_fin, num=input_dict["n_dt"])

    test_dtt = test_tgrid[1] - test_tgrid[0]

    # Testing the numpy part
    np.testing.assert_almost_equal(tgrid, test_tgrid, decimal=7)
    np.testing.assert_almost_equal(dtt, test_dtt, decimal=7)

    # In the current config, the center points should be whole numbers.
    assert tgrid[instance.n_dt // 2] == int(tgrid[instance.n_dt // 2])


@pytest.mark.parametrize(  # pylint: disable=not-callable
    "input_dict",
    [
        # All normal.
        {
            "itp_requested": True,
            "itp_states": 5,
            "itp_thresh": 0.0,
            "itp_relax_states": [0],
            "rtp_init_states": [0],
            "init_position": 0.0,
            "init_sigma": 0.5,
        },
        # Two ITP states and a single RTP state.
        {
            "itp_requested": True,
            "itp_states": 5,
            "itp_thresh": 0.0,
            "itp_relax_states": [0, 1],
            "rtp_init_states": 0,
            "init_position": 0.0,
            "init_sigma": 0.5,
        },
        # Two ITP states and no RTP state given.
        {
            "itp_requested": True,
            "itp_states": 5,
            "itp_thresh": 0.0,
            "itp_relax_states": [0, 1],
            "rtp_init_states": [],
            "init_position": 0.0,
            "init_sigma": 0.5,
        },
        # Two ITP states and two RTP state given as string.
        {
            "itp_requested": True,
            "itp_states": 5,
            "itp_thresh": 0.0,
            "itp_relax_states": [0, 1],
            "rtp_init_states": "0, 1",
            "init_position": 0.0,
            "init_sigma": 0.5,
        },
        # Parameters missing
        pytest.param(
            {
                "itp_requested": True,
                "itp_states": 5,
                "itp_relax_states": [0, 1],
                "rtp_init_states": "0, 1",
                "init_position": 0.0,
                "init_sigma": 0.5,
            },
            marks=pytest.mark.xfail,
        ),
        # ITP requested, but the number of ITP states is 0.
        pytest.param(
            {
                "itp_requested": True,
                "itp_states": 0,
                "itp_thresh": 0.0,
                "itp_relax_states": [0],
                "rtp_init_states": [1, 2],
                "init_position": 0.0,
                "init_sigma": 0.5,
            },
            marks=pytest.mark.xfail,
        ),
        # Initial sigma is 0.0. That won't work.
        pytest.param(
            {
                "itp_requested": True,
                "itp_states": 1,
                "itp_thresh": 0.0,
                "itp_relax_states": [0],
                "rtp_init_states": [1, 2],
                "init_position": 0.0,
                "init_sigma": 0.0,
            },
            marks=pytest.mark.xfail,
        ),
    ],
)
def test_wavefunction_parameter_from_dict(input_dict, base_kwargs, potential):
    """Testing the wavefunction parameter class by instantiating from dicts."""

    input_dict.update(base_kwargs)
    instance = WavefunctionParameter(**input_dict)

    # assert instance.is_complete()

    for reduced in [False, True]:
        wf = instance.initialize_wavefunction(potential, reduced_dim=reduced)
        np.testing.assert_allclose(calc_norm_nd(wf, potential.dx, potential.n_m), 1.0)

        if reduced:
            assert wf.shape[-1] == len(instance.itp_relax_states)


@pytest.mark.parametrize(  # pylint: disable=not-callable
    "input_dict,expected_mus",
    [
        (
            {
                "omega": 1.0,
                "en_gs": 0.0,
                "en_es": 0.5,
                "pos_gs": 0.0,
                "pos_es": 0.2,
                "gmin": -2.0,
                "gmax": 2.0,
                "mass": 10.0,
                "trans_dipole": {"0->1": 1.0, "0->2": 1.0},
            },
            [((0, 1), 1.0), ((0, 2), 1.0)],
        ),
        (
            {
                "omega": 1.0,
                "en_gs": 0.0,
                "en_es": 0.5,
                "pos_gs": 0.0,
                "pos_es": 0.5,
                "gmin": -1.0,
                "gmax": 1.0,
                "mass": 10.0,
                "trans_dipole": {"0->1": 1.0, "0->2": 1.0},
            },
            [((0, 1), 1.0), ((0, 2), 1.0)],
        ),
        (
            {
                "omega": 1.0,
                "en_gs": 0.0,
                "en_es": 0.5,
                "pos_gs": 0.0,
                "pos_es": 0.5,
                "gmin": -1.0,
                "gmax": 1.0,
                "mass": 10.0,
                "trans_dipole": {"0->1": 1.0, "0->2": 1.0},
            },
            [((0, 1), 1.0), ((0, 2), 1.0)],
        ),
    ],
)
def test_analyt_system_parameter_from_dict(base_kwargs, input_dict, expected_mus):
    """Testing the AnalyticalSystemParameter class by instantiating from dicts."""

    n_g = 127
    base_kwargs["n_g"] = n_g

    input_dict.update(base_kwargs)
    instance = AnalyticalSystemParameter(**input_dict)

    gmin, gmax = input_dict["gmin"], input_dict["gmax"]

    xgrid = instance.xgrid
    test_xgrid = np.linspace(gmin, gmax, num=base_kwargs["n_g"])
    np.testing.assert_allclose(xgrid, test_xgrid)

    dx = instance.dx
    np.testing.assert_allclose(dx, test_xgrid[1] - test_xgrid[0])

    minmax = instance.minmax
    assert minmax == (gmin, gmax)

    mus = instance.mu
    assert mus == expected_mus

    pes = instance.PES
    logging.info(pes.shape)

    assert np.all(
        np.unravel_index(np.argmin(pes[..., 0]), (n_g, n_g))
        == np.asarray([n_g // 2, n_g // 2])
    )
