# -*- coding: utf-8 -*-
"""Reusable fixtures for pyQD tests"""

# pylint: disable=redefined-outer-name

from pathlib import Path

import h5py
import numpy as np
import pyfftw
import pytest

from ..integrators import ITPIntegrator
from ..interactors import FieldParameter
from ..parameters import (
    AnalyticalSystemParameter,
    OptionalParameter,
    TimeParameter,
    WavefunctionParameter,
)
from ..parameters.defaults import Base
from ..utils.physics import calc_norm_nd, gaussian_nd_nonnorm


@pytest.fixture(name="base_kwargs")
def fixture_base_kwargs():
    """Basic parameters n_s, n_m, n_g for population from a dict."""
    return {"n_g": 64, "n_s": 3, "n_m": 2}


@pytest.fixture(name="infile")
def fixture_infile():
    """Common testInput file for the unit tests."""
    return (Path(__file__).parent / "data/testInput_unit_interactors.yaml").resolve()


@pytest.fixture(name="xgrid")
def fixture_xgrid(base_kwargs):
    """Simple fixture for the grid spacing."""
    return np.linspace(-5, 5, num=base_kwargs["n_g"])


@pytest.fixture(name="dx")
def fixture_dx(xgrid):
    """Simple fixture for the grid spacing."""
    return xgrid[1] - xgrid[0]


@pytest.fixture(name="potential")
def fixture_potential(infile):
    """Common potential instance for the unit tests."""
    return AnalyticalSystemParameter.from_file(infile)


@pytest.fixture(name="mu_input")
def fixture_mu_input():
    """Supply a transition dipole moment input to the PolarisationSpectrum class"""
    return [[[0, 1], 1.0], [[0, 2], 1.0]]


@pytest.fixture(name="h5file")
def fixture_h5file(tmpdir):
    """Provides a .h5 file to test the observable printing."""
    return h5py.File(f"{tmpdir}/pyqd-pytest.h5", "w")


@pytest.fixture(name="fftf")
def fixture_fftf(base_kwargs, wavefunc):
    """Provide a FFTW builder function for the momentum-based observables"""
    n_m = base_kwargs["n_m"]
    fftf = pyfftw.builders.fftn(wavefunc, overwrite_input=True, axes=list(range(n_m)))
    return fftf


@pytest.fixture(name="wavefunc")
def fixture_wavefunc(base_kwargs, xgrid, dx):
    """Provide a random wavefunction for the observable testing."""
    bkwargs = base_kwargs

    n_g = bkwargs["n_g"]
    n_m = bkwargs["n_m"]
    n_s = bkwargs["n_s"]

    meshes = np.meshgrid(*[xgrid] * n_m)
    centers = [0.0] * n_m
    sigmas = [0.6] * n_m

    wf = np.zeros([n_g] * n_m + [n_s], dtype=np.complex128)
    wf[..., 0] = gaussian_nd_nonnorm(
        meshes=meshes, centers=centers, sigmas=sigmas, amplitude=1.0
    )

    wf /= np.sqrt(calc_norm_nd(wf, dx, n_m))

    return wf


@pytest.fixture(name="itpintegrator")
def fixture_itpintegrator(base_kwargs):
    """Generate an ITP integrator"""
    base = Base(**base_kwargs)
    syst = AnalyticalSystemParameter.from_defaults(base=base)

    wave = WavefunctionParameter.from_defaults(
        base=base, itp_states=5, itp_relax_states=list(range(base_kwargs["n_s"]))
    )

    opti = OptionalParameter.from_defaults(
        base=base, output_dir=".pytest_output", calc_name="pytest"
    )
    time = TimeParameter.from_defaults(base=base, n_dt=50)

    return ITPIntegrator(
        **base_kwargs, potential=syst, timeparam=time, wfparam=wave, optional=opti
    ).propagate_all()


@pytest.fixture(name="tgrid")
def fixture_tgrid():
    """Generates a timegrid for the observables"""
    return np.linspace(-500, 500, num=1001)


@pytest.fixture(name="prep_kwargs")
def fixture_prep_kwargs(fftf, xgrid, tgrid, mu_input, itpintegrator):
    """Provide prep_kwargs for the observable preparation."""
    prep = {
        "tgridsize": len(tgrid),
        "fft_f": fftf,
        "xgrid": xgrid,
        "mu_input": mu_input,
        "t_init": 0.0,
        "itp_integrator": itpintegrator,
        "init_state": [0],
        "tgrid": tgrid,
    }

    return prep


@pytest.fixture(name="xp_operator")
def fixture_xp_operator(base_kwargs):
    """Gives a very basic operator for total energy observables"""

    single = np.eye(base_kwargs["n_s"])

    return single


@pytest.fixture(name="np_field")
def fixture_npfield(infile):
    """Common potential instance for the unit tests."""

    return FieldParameter.from_file(infile)


@pytest.fixture(name="timep")
def fixture_timep(infile):
    """Common potential instance for the unit tests."""

    return TimeParameter.from_file(infile)
