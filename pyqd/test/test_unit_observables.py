# -*- coding: utf-8 -*-
"""Unit tests for the pyQD.observables module."""

# pylint: disable=redefined-outer-name,not-callable,protected-access

import numpy as np
import pytest

from ..observables import (
    CorrelationSpectrum,
    EigenfunctionCoefficients,
    MomentumEV,
    NormObservable,
    PolarisationSpectrum,
    PositionEV,
    ReducedDensitySpectrum,
    TotalEnergyObservable,
)


@pytest.mark.parametrize(
    "observable,expected_res",
    [
        (NormObservable, [1.0, 0.0, 0.0]),
        (TotalEnergyObservable, [1.0, 1.0]),
        (PositionEV, [[0.0, 0.0], [0.0, 0.0], [0.0, 0.0]]),
        (MomentumEV, [[0.0, 0.0], [0.0, 0.0], [0.0, 0.0]]),
        (CorrelationSpectrum, [0.0, 0.0]),
        (PolarisationSpectrum, [0.0]),
        (EigenfunctionCoefficients, None),
        (ReducedDensitySpectrum, None),
    ],
)
def test_observable_lifecycle_dict(
    observable,
    expected_res,
    dx,
    base_kwargs,
    xp_operator,
    prep_kwargs,
    wavefunc,
    h5file,
):
    """
    Compounded unit test for the relevant observable functions.
    Those are .obs_prepare(), .obs_update(), and .obs_to_outfile()
    """

    base_kwargs.update(**{"dx": dx})

    update_idx = len(prep_kwargs["tgrid"]) // 2

    instance = observable(**base_kwargs).obs_prepare(**prep_kwargs)
    instance.obs_update(
        t=0.0,
        idx=update_idx,
        wf=wavefunc,
        operator_t=xp_operator,
        operator_v=xp_operator,
    )

    if expected_res is not None:
        np.testing.assert_allclose(
            instance._array[..., update_idx], expected_res, atol=1e-12
        )

    for ii in range(instance._array.shape[0]):
        instance._array[ii, :] = np.sin(0.1 * prep_kwargs["tgrid"])

    instance.obs_to_outfile(h5file)
