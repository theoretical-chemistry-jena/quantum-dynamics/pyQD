# -*- coding: utf-8 -*-
"""Unit tests for pyQD.utils_physics"""

# pylint: disable=W0212
import logging

import numpy as np
import opt_einsum
import pyfftw
import pytest

from ..utils.io import process_partners  # , _json_object_hook_dict
from ..utils.physics import (
    _calc_integral_matrix_ev,
    _calc_integral_prop_ev,
    build_einsum_op,
    build_exec_order_recursive,
    calc_autocorrelation,
    calc_energy_t_nd,
    calc_energy_v_nd,
    calc_k_grid,
    calc_momentum_ev,
    calc_norm_2d,
    calc_norm_nd,
    calc_norm_nd_statewise,
    calc_ovlp_nd,
    calc_polarisation_spectrum,
    calc_position_ev,
    calc_position_ev_nonorm,
    gaussian_nd_nonnorm,
    gaussian_norm,
    gen_e_field,
    gen_ptb_v0,
    gen_ptb_v1,
    harmonic_osc_1d,
    trim_automagically,
)

logger = logging.getLogger(__name__)


def test_gaussian():
    """Unit test for the gaussian_norm function"""
    x = np.linspace(-100.0, 100.0, num=500)
    dx = x[1] - x[0]

    sig = 1.0
    mu = 0.0

    gauss_integral = np.sum(gaussian_norm(x, mu, sig)) * dx
    assert np.isclose(gauss_integral, 1.0)


def test_calc_norm_2d():
    """Unit test for the calc_norm_2d function"""
    test_dr = 1
    test_array = np.zeros((100, 100), dtype="complex128")
    test_array[50:60, 50:60] = 10.0
    test_norm = 10000.0

    norm = calc_norm_2d(test_array, test_dr, test_dr)

    assert np.isclose(test_norm, norm)
    return None


def test_calc_norm_nd():
    """Unit test for the calc_norm_nd function"""
    n_dim = 1
    test_dr = 1
    test_array = np.zeros(100, dtype="complex128")
    test_array[50:60] = 10.0 + 1.0j
    test_norm = 1010.0

    # Calc the norm both with and without norm
    norm = calc_norm_nd(test_array, test_dr, n_dim)
    norm_phase = calc_norm_nd(test_array * np.exp(-1.0j * 4), test_dr, n_dim)

    assert np.isclose(test_norm, norm)
    assert np.isclose(norm, norm_phase)

    return None


def test_calc_norm_nd_statewise():
    """Unit test for the calc_norm_nd_statewise function"""
    n_dim = 2
    test_dr = 1.0
    test_array = np.zeros((100, 100, 3), dtype="complex128")
    test_array[50:60, 50:60] = np.asarray([0.01, 10.0 + 5.0j, 20.0])

    expected_res = np.asarray([1.00e-02, 1.25e04, 4.00e04])

    norm = calc_norm_nd_statewise(test_array, test_dr, n_dim)
    norm_phase = calc_norm_nd_statewise(test_array * np.exp(-1.0j * 5), test_dr, n_dim)

    assert np.all(np.isclose(expected_res, norm))
    assert np.all(np.isclose(norm_phase, norm))

    return None


def test_gaussian_nd_nonnorm():
    """Unit test for the gaussian_nd_nonnorm function"""

    for dim in range(1, 4):
        meshes = np.meshgrid(*[np.linspace(-10.0, 10, num=251)] * dim)
        centers = [0.0] * dim
        sigma = [0.5] * dim
        amplitude = 1.35

        gauss = gaussian_nd_nonnorm(meshes, centers, sigma, amplitude)

        # Is te amplitude correct?
        assert np.isclose(np.nanmax(gauss), amplitude)

        # Symmetry of the gaussian
        assert np.allclose(gauss, gauss.T)

        # Data type. Should be float
        assert gauss.dtype == np.float64

    return None


def test_harmonic_osc_1d():
    """Unit test for the harmonic_osc_1d function"""
    xgrid = np.linspace(-5, 5, num=501)
    minx = 0
    miny = 1.4
    omega = 0.1
    mass = 5

    # Array operation
    harm = harmonic_osc_1d(xgrid, minx, miny, omega, mass)

    assert isinstance(harm, np.ndarray)
    assert np.nanmin(harm) == 1.4
    assert np.argmin(harm) == 250
    assert np.isclose(harm[300], 1.425)

    # Single float operation (x = 2)
    harm = harmonic_osc_1d(xgrid[350], minx, miny, omega, mass)
    assert isinstance(harm, float)
    assert np.isclose(harm, 1.5)

    return None


def test_calc_ovlp_nd():
    """Unit test for the calc_ovlp_nd function"""
    n_m = 1

    # 1D --> Hermite polynomials as test functions
    xgrid = np.linspace(-50, 50, 50001)
    dx = np.abs(xgrid[1] - xgrid[0])

    h_1 = 2 * xgrid  # H_1
    h_2 = 4 * xgrid ** 2 - 2  # H_2

    # Mismatched
    ovlp12 = calc_ovlp_nd(h_1, h_2 * np.exp(-(xgrid ** 2)), dx, n_m)
    assert np.isclose(ovlp12, 0.0)

    # Matched (compare to analytical integral)
    integ = np.sqrt(np.pi) * 2 ** 2 * np.math.factorial(2)
    ovlp22 = calc_ovlp_nd(h_2, h_2 * np.exp(-(xgrid ** 2)), dx, n_m)
    assert np.isclose(ovlp22, integ)

    return None


def test_gen_e_field():
    """Unit test for the gen_e_field function"""

    e_0 = 10.0
    omega = 1.0
    sigma = 0.5
    t_central = 0.0

    # Test as ndarray
    t = np.linspace(-500, 500, num=10001)
    field = gen_e_field(t, e_0, omega, sigma, t_central)

    assert np.argmax(field) == 5000
    assert np.nanmax(field) == e_0
    assert np.all(field[::1] == field)
    # Test as float
    t = np.pi / 2
    omega = 1 / (2 * np.pi)
    field = gen_e_field(t, e_0, omega, sigma, t_central)

    assert np.isclose(field, 0.0)

    return None


def test_build_exec_order_recursive():
    """Unit test for the build_exec_order_recursive function"""

    for ii in range(20):
        order = build_exec_order_recursive(ii)

        if ii > 0:
            assert np.nanmax(order) == ii - 1

        assert len(order) == 2 ** ii - 1

    return None


def test_build_einsum_op():
    """Unit test for the build_einsum_op function"""

    rand = np.random.rand(64, 64, 3, 3)

    for ii in range(1, 1002, 20):

        logging.debug(ii)
        operation = build_einsum_op(ii)
        assert len(operation) == 5 * ii + 7 + ii - 1

        if (ii - 1) % 100 == 0:
            logging.debug("contracting n={}".format(ii))
            opt_einsum.contract(operation, *[rand] * ii)


def test_gen_ptb_v0():
    """Unit test for the gen_ptb_v0 function"""

    # Testing as array
    t = np.linspace(-50, 450, num=1001)
    s_on = 30
    s_off = 30

    t_on = 50
    t_on_idx = np.argmin(np.abs(t - t_on))
    t_off = 350
    t_off_idx = np.argmin(np.abs(t - t_off))

    ampl = 5.0

    ptb = gen_ptb_v0(t, s_on, s_off, t_on, t_off, ampl)

    assert np.isclose(ptb[t_on_idx], ampl)
    assert np.isclose(ptb[t_off_idx], ampl)
    assert np.all(ptb[t > t_off] < ampl)
    assert np.all(ptb[t < t_on] < ampl)

    # Testing as float
    ptb_on = gen_ptb_v0(t_on, s_on, s_off, t_on, t_off, ampl)
    ptb_off = gen_ptb_v0(t_off, s_on, s_off, t_on, t_off, ampl)

    assert np.isclose(ptb_on, ampl)
    assert np.isclose(ptb_off, ampl)


def test_gen_ptb_v1():
    """Unit test for the gen_ptb_v1 function"""

    t = np.linspace(-50, 450, num=1001)

    s_off = 1
    expon = 2
    rel_ampl = 5.0

    e_0 = 1.0
    t_central = 200.0
    # e_0, omega, sigma, t_central
    fieldparam = (e_0, 1.0, 50, t_central)

    t_central_idx = np.argmin(np.abs(t - t_central))
    logging.debug(t_central_idx)

    ptb = gen_ptb_v1(t, s_off, rel_ampl, 0.0, expon, *fieldparam)
    ptb_2pi = gen_ptb_v1(t, s_off, rel_ampl, 2.0 * np.pi, expon, *fieldparam)

    assert np.isclose(ptb[t_central_idx], rel_ampl * e_0)
    assert np.allclose(ptb, ptb_2pi)
    assert np.all(ptb <= rel_ampl * e_0)

    ptb_hpi_f = gen_ptb_v1(t_central, s_off, rel_ampl, 0.5 * np.pi, expon, *fieldparam)
    ptb_2pi_f = gen_ptb_v1(t_central, s_off, rel_ampl, 2.0 * np.pi, expon, *fieldparam)

    assert np.isclose(ptb_hpi_f, 0.0)
    assert np.isclose(ptb[t_central_idx], ptb_2pi_f)


def test__calc_integral_matrix_ev():
    """Unit test for the _calc_integral_matrix_ev function"""

    n_g = 64
    n_s = 3
    dx = 1.0
    n_m = 2

    wf = np.random.rand(n_g, n_g, n_s)
    operator = np.eye(n_s)

    # Show that when employing the identity operator, the integral is the norm.
    integ = _calc_integral_matrix_ev(wf, operator, dx, n_m)
    norm = calc_norm_nd(wf, dx, n_m)

    assert np.isclose(integ, norm)

    # TODO More to do?


def test__calc_integral_prop_ev():
    """Unit test for the _calc_integral_prop_ev function"""

    n_g = 63
    n_m = 2
    dx = 1.0

    centers = [0.0, 0.0]

    xgrid = np.linspace(-5, 5, num=n_g, dtype=np.float64)
    mgrid = np.meshgrid(*[xgrid] * n_m)

    wf = gaussian_nd_nonnorm(mgrid, centers, [0.5, 0.5], 1.0)[..., np.newaxis]

    # The operator is the position meshgrid
    operator = mgrid

    exp_val = _calc_integral_prop_ev(wf, operator, dx, n_m)

    assert np.allclose(centers, exp_val)

    # TODO Any more tests required here?


def test_calc_k_grid():
    """Unit test for the calc_k_grid function"""

    for n_g in range(16, 256):
        x = np.linspace(-5, 5, n_g)
        dx = x[1] - x[0]

        k = calc_k_grid(n_g, dx)

        dk = np.abs(k[1] - k[0])

        assert dk == (2 * np.pi) / (dx * n_g)
        assert np.all(
            np.logical_or(
                np.abs(k) <= (np.pi / dx), np.isclose(np.abs(k), (np.pi / dx))
            )
        )
        assert len(k[k == 0]) == 1
        assert len(k[k > 0]) == n_g // 2
        assert len(k[k < 0]) == n_g // 2 - (1 if n_g % 2 == 0 else 0)


def test_calc_energy_vt_nd():
    """Unit test for the calc_energy_v_nd function"""

    n_g = 512
    n_m = 1

    # Test arrangement: Get a 1D harmonic oscillator and the corr. analytical solutions
    omega = 0.5
    mass = 2.0

    x = np.linspace(-5, 5, num=n_g)
    dx = x[1] - x[0]

    k = calc_k_grid(n_g, dx)

    # Expand the dimensions of the raw operators
    x_op = x[..., np.newaxis, np.newaxis]
    k_op = k[..., np.newaxis, np.newaxis]

    # The analytical wavefunction is based on Hermite polynomials H_0(x) = 1
    wf_0 = (
        1
        * ((mass * omega) / (np.pi * 1.0)) ** 0.25
        * np.exp(-0.5 * mass * omega * x ** 2)
    )
    wf_0 = wf_0[..., np.newaxis].astype(np.complex128)

    # Calculate the operators
    v_operator = 0.5 * mass * omega ** 2 * x_op ** 2
    t_operator = (1 / (2 * mass)) * k_op ** 2
    fft_fwd = pyfftw.builders.fft(wf_0, axis=0)

    en_0 = 0.5 * omega

    en_v = calc_energy_v_nd(wf_0, v_operator, dx, n_m)
    en_t = calc_energy_t_nd(wf_0, t_operator, fft_fwd, dx, n_g, n_m)

    logging.debug(en_t)
    logging.debug(en_v)

    assert np.isclose(en_v, en_t)
    assert np.isclose(en_v + en_t, en_0)


def test_calc_position_momentum_ev():
    """Unit test for the calc_position_ev and calc_momentum_ev functions"""

    n_g = 16

    xgrid = np.linspace(-5, 5, num=n_g, dtype=np.float64)
    dx = xgrid[1] - xgrid[0]

    kgrid = calc_k_grid(n_g, dx)
    dk = np.abs(kgrid[1] - kgrid[0])

    for n_m in range(1, 5):
        centers = [0.0] * n_m
        sigmas = [0.5] * n_m

        mxgrid = np.meshgrid(*[xgrid] * n_m)
        mkgrid = np.meshgrid(*[kgrid] * n_m)

        wf = gaussian_nd_nonnorm(mxgrid, centers, sigmas, 1.0)[..., np.newaxis]
        fft_fwd = pyfftw.builders.fftn(wf, axes=list(range(n_m)))

        # The operator is the position meshgrid
        x_operator = mxgrid
        k_operator = mkgrid

        norm = calc_norm_nd_statewise(wf, dx, n_m)

        x_exp_val = calc_position_ev(wf, x_operator, dx, n_m, norm)
        x_exp_val_nonorm = calc_position_ev_nonorm(wf, x_operator, dx, n_m)

        k_exp_val = calc_momentum_ev(wf, k_operator, fft_fwd, dk, 1, n_m)

        assert np.allclose(x_exp_val, x_exp_val_nonorm)
        assert np.allclose(centers, x_exp_val)

        logging.debug(k_exp_val)
        assert np.allclose(k_exp_val, 0.0)


def test_calc_polarisation_ev():
    """Unit test for the calc_polarisation_ev function"""

    # This test works as long as the test__calc_integral_matrix_ev works.
    assert True


def test_calc_autocorrelation():
    """Unit test for the calc_autocorrelation function"""

    n_g = 63
    n_s = 3
    n_m = 2
    dx = 1.0

    wf = (
        np.random.randn(
            n_g,
            n_g,
            n_s,
        )
        + 1.0j * np.random.rand(n_g, n_g, n_s)
    )
    norm = calc_norm_nd_statewise(wf, dx, n_m)

    for t in [0.0, 2 * np.pi]:
        e_0 = 1.0

        autocorr = calc_autocorrelation(wf, wf, e_0, t, dx, n_m)

        assert np.allclose(autocorr, norm)

    autocorr_t1 = calc_autocorrelation(wf, wf, e_0, 1.0, dx, n_m)
    assert not np.allclose(autocorr_t1, norm)


def test_trim_automagically():
    """Unit test for the trim_automagically function"""

    test_array = np.zeros(5001)

    # Does it work with delta spikes?
    test_array[1000] = test_array[4000] = 1
    mask = trim_automagically(test_array, 0)

    assert len(mask[mask.nonzero()]) == 3000

    # Does it work with Gaussians?
    test_array = np.arange(5001, dtype=np.float64)
    test_array = np.exp(-0.5 * (test_array - 500) ** 2) + np.exp(
        -0.5 * (test_array - 4500) ** 2
    )

    mask = trim_automagically(test_array, 0)
    assert len(mask[mask.nonzero()]) == 4000

    # Does it work with plateaus?
    test_array = np.zeros(5001)
    test_array[1000:4000] = 1

    # TODO Why is that so?
    mask = trim_automagically(test_array, 0)
    assert len(mask[mask.nonzero()]) == 2999


def test_calc_polarisation_spectrum():
    """Unit test for the calc_polarisation_spectrum function"""

    t = np.linspace(-10.0, 500.0, num=10000)

    for ii in range(1, 20):

        # Generate the base signal
        signal = np.sin(1.0 * t).astype(np.complex128) + 1.0j * np.sin(
            1.0 * t + np.pi / 2
        )

        # Add more signals
        for i in range(1, ii):
            omega = float(2 * i) ** (-1)
            signal *= np.sin(omega * t)

        # Calculate the artificial spectrum
        _, spec, _ = calc_polarisation_spectrum(signal, t, 1.0, io_units=("eV", "eV"))

        # Test the imaginary part
        spec_imag = np.imag(spec)
        avrg = spec_imag.sum() / spec_imag.size

        # Test is that the imaginary part is a small constant value
        assert np.allclose(spec_imag, avrg)
        assert np.allclose(spec_imag - avrg, 0.0)

        # TODO More to test here?


@pytest.mark.parametrize(  # pylint: disable=not-callable
    "partner_input,kwargs,expected_result,expected_nr",
    (
        (
            [0],
            {"include_self": True, "indexed_start_zero": True},
            [(0, 0)],
            1,
        ),
        (
            [0, 1],
            {"include_self": True, "indexed_start_zero": True},
            [(0, 0), (1, 1)],
            2,
        ),
        (
            [0, 1],
            {"include_self": False, "indexed_start_zero": True},
            [],
            0,
        ),
        (
            "0",
            {"include_self": True, "indexed_start_zero": True},
            [(0, 0)],
            1,
        ),
        (
            "0, 1",
            {"include_self": True, "indexed_start_zero": True},
            [(0, 1)],
            1,
        ),
        (
            "0",
            {"include_self": False, "indexed_start_zero": True},
            [],
            0,
        ),
        (
            ["0, 1", "1"],
            {"include_self": False, "indexed_start_zero": True},
            [(0, 1)],
            1,
        ),
        (
            ["0, 1", "1"],
            {"include_self": True, "indexed_start_zero": True},
            [(0, 1), (1, 1)],
            2,
        ),
        (
            [1, 2],
            {"include_self": True, "indexed_start_zero": False},
            [(0, 0), (1, 1)],
            2,
        ),
        (
            [1, 2],
            {"include_self": True, "indexed_start_zero": True},
            [(1, 1), (2, 2)],
            2,
        ),
        (
            [1, 2],
            {"include_self": True, "indexed_start_zero": True},
            [(1, 1), (2, 2)],
            2,
        ),
        (
            ["1", "2"],
            {"include_self": True, "indexed_start_zero": True},
            [(1, 1), (2, 2)],
            2,
        ),
        (["1", "2"], {"include_self": False, "indexed_start_zero": True}, [], 0),
    ),
)
def test_process_partners(partner_input, kwargs, expected_result, expected_nr):
    """Unit test for the process_partners function"""

    n_i, p_output = process_partners(partner_input, **kwargs)

    assert p_output == expected_result
    assert n_i == expected_nr
