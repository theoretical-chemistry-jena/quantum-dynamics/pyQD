# -*- coding: utf-8 -*-

"""Functional tests for pyQD"""

# pylint: disable=protected-access
import logging
from pathlib import Path

import numpy as np
import pytest

from ..integrators import RTPIntegrator

HERE = Path(__file__).parent
logger = logging.getLogger(__name__)


@pytest.mark.parametrize(
    # Input file, number of opt. interactors, opt. interactions, number of observables
    "input_file,n_interactors,n_interactions,n_observables",
    [
        ("testInput_full_1D.yaml", 5, 6, 6),
        ("testInput_full_1D-1M3S.yaml", 5, 8, 6),
        ("testInput_1M4S.yaml", 3, 8, 6),
        ("testInput_full_2D.yaml", 5, 8, 6),
        ("testInput_full_3D.yaml", 5, 10, 6),
        ("testInput_minimal.yaml", 1, 1, 1),
        ("testInput_noField_valid.yaml", 2, 2, 5),
        ("testInput_field_dict_valid.yaml", 2, 3, 6),
        ("testInput_field_list_valid.yaml", 2, 3, 6),
        ("testInput_noJ_valid.yaml", 2, 6, 6),
        ("testInput_ptb_v0_valid.yaml", 2, 2, 4),
        ("testInput_ptb_v1_list_valid.yaml", 3, 6, 6),
        ("testInput_ptb_v1_dict_valid.yaml", 3, 6, 6),
        pytest.param("testInput_ptb_v1_invalid.yaml", 0, 0, 0, marks=pytest.mark.xfail),
        pytest.param(
            "testInput_ptb_v1_noField_invalid.yaml", 0, 0, 0, marks=pytest.mark.xfail
        ),
    ],
)
def test_cli_inputs_undamped(input_file, n_interactors, n_interactions, n_observables):
    """
    This parametrized pytest function tests the "normal functioning" of the input files.
    """
    obj = RTPIntegrator.from_file(HERE / "data" / input_file)

    calctr = obj.propagate_all()

    norm_obs = obj.norm

    l_ia = [ia.num for ia in calctr.interactors]
    logger.debug(list(zip(l_ia, calctr.interactors)))

    # Most important diagnostic: Is the norm always equal to 1.0?
    assert np.allclose(np.sum(norm_obs._array, axis=0), 1.0)

    assert sum(l_ia) == n_interactions
    assert len(calctr.interactors) == n_interactors
    assert len(calctr._observable_objs) == n_observables
    assert calctr.optional.outdir.exists()
    assert len(list(calctr.optional.outdir.glob("*RTP*h5"))) > 0


@pytest.mark.parametrize(
    "input_file,n_interactors,n_interactions,n_observables",
    [
        ("testInput_full_2D_damping_diab.yaml", 6, 9, 6),
        ("testInput_full_2D_damping_adiab.yaml", 6, 9, 6),
    ],
)
def test_cli_inputs_damped(input_file, n_interactors, n_interactions, n_observables):
    """
    This parametrized pytest function tests the "normal functioning" of the input files.
    """
    obj = RTPIntegrator.from_file(HERE / "data" / input_file)

    calctr = obj.propagate_all()

    norm_obs = obj.norm

    l_ia = [ia.num for ia in calctr.interactors]
    logger.debug(list(zip(l_ia, calctr.interactors)))

    # Most important diagnostic: Is the norm always equal to 1.0?
    assert np.all(np.sum(norm_obs._array, axis=0)[-1] < 1.0)

    assert sum(l_ia) == n_interactions
    assert len(calctr.interactors) == n_interactors
    assert len(calctr._observable_objs) == n_observables
    assert calctr.optional.outdir.exists()
    assert len(list(calctr.optional.outdir.glob("*RTP*h5"))) > 0
