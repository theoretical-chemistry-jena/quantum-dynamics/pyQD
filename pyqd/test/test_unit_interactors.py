# -*- coding: utf-8 -*-
"""Unit tests for the interactors."""


# pylint: disable=protected-access,redefined-outer-name
import logging

import numpy as np
import pytest

from ..interactors import (
    DummyInteractor,
    FieldParameter,
    JCouplingParameter,
    LegacyPerturbationParameter,
    NanoparticleParameter,
    StaticInteractor,
)
from ..utils.convert import interactor_key

logger = logging.getLogger(__name__)


@pytest.mark.parametrize(  # pylint: disable=not-callable
    "iact, num, energy",
    [
        (DummyInteractor, 1, [0.0]),
        (JCouplingParameter, 1, [1.0]),
        (FieldParameter, 2, [-1.0, -1.0]),
        (NanoparticleParameter, 3, [-5.0, -5.0, -5.0]),
        (LegacyPerturbationParameter, 1, [1.0]),
    ],
)
def test_lifecycle_from_file(
    iact, num, energy, infile, h5file
):  # pylint: disable=redefined-outer-name
    """Tests the relevant interactor methods using the automated population."""

    # Populate the individual items from a file
    # kwargs = {"field": np_field, "system": potential}
    instance = iact.from_file(infile)

    assert instance.num == num

    # Build the matrices and get the energies
    split = [1] * instance.num

    mats = instance.all_matrices(t=0.0, dtt=1.0, splits=split)

    matsl, energylist = instance.all_matrices(
        t=0.0, dtt=1.0, splits=split, print_e="list"
    )

    matsm, energymat = instance.all_matrices(
        t=0.0, dtt=1.0, splits=split, print_e="matrix"
    )

    # Test the propagator matrices
    assert np.all(
        [
            np.logical_and(np.all(matl == matm), np.all(mat == matm))
            for mat, matl, matm in zip(mats, matsl, matsm)
        ]
    )
    assert np.all([mat.shape == (3, 3) for mat in matsl + matsm])
    assert instance.num == len(matsm)
    assert instance.num == len(matsm)

    # Test the operator output
    assert energy == energylist
    assert isinstance(energymat, np.ndarray)
    assert energymat.shape == (3, 3)

    # Test the observable capabilities
    instance.obs_prepare(tgridsize=101)
    instance.obs_update(t=0.0, idx=50)
    instance.obs_to_outfile(h5file)

    if instance.TIME_DEPENDENT:
        assert len(instance._array.shape) == 2
        assert instance._array.shape[-1] == 101
        assert np.allclose(instance._array[:, 50], energy)


@pytest.mark.parametrize(  # pylint: disable=not-callable
    "iact, kwargs",
    [
        (DummyInteractor, {"splits": [1]}),
        (DummyInteractor, {}),
        (JCouplingParameter, {"splits": [1], "pairs": [(1, 2)], "j_coupling": 1.0}),
        pytest.param(
            JCouplingParameter,
            {"splits": [1], "j_coupling": 1.0},
            marks=pytest.mark.xfail,
        ),
        (
            FieldParameter,
            {
                "splits": [1],
                "idx": 0,
                "e_0": 1.0,
                "t_central": 0.0,
                "sigma": 50,
                "omega": 0.1,
                "mu": [((0, 1), 1.0), ((0, 2), 1.0)],
                "time": None,
            },
        ),
        pytest.param(
            FieldParameter,
            {
                "splits": [1],
                "idx": 0,
                # "e_0": 1.0,
                "t_central": 0.0,
                "sigma": 50,
                "omega": 0.1,
                "mu": [((0, 1), 1.0), ((0, 2), 1.0)],
            },
            marks=pytest.mark.xfail,
        ),
        (
            NanoparticleParameter,
            {
                "splits": [1, 1, 1],
                "sigma_off": 1.0,
                "amplitude_diag": 5.0,
                "amplitude_od": 5.0,
                "included_diag": [(1, 1)],
                "field": None,
                "time": None,
                "np_r0": 1.0,
                "np_dr": 1.0,
                "np_decay": 0.0,
                "decay": "gaussian",
                "phase": 0.0,
            },
        ),
        pytest.param(
            NanoparticleParameter,
            {
                "splits": [1, 1, 1],
                "sigma_off": 1.0,
                "amplitude_diag": 5.0,
                "amplitude_od": 5.0,
                "included_diag": [(1, 1)],
                "field": None,
                # "np_r0": 1.0,
                "np_dr": 1.0,
                "np_decay": 0.0,
                "decay": "gaussian",
                "phase": 0.0,
            },
            marks=pytest.mark.xfail,
        ),
        (
            LegacyPerturbationParameter,
            {
                "splits": [1],
                "state": 0,
                "time_on": -50.0,
                "time_off": 50.0,
                "sigma_on": 10.0,
                "sigma_off": 10.0,
                "amplitude": 1.0,
            },
        ),
        pytest.param(
            LegacyPerturbationParameter,
            {
                "splits": [1],
                "state": 0,
                "time_on": -50.0,
                # "time_off": 50.0,
                "sigma_on": 10.0,
                "sigma_off": 10.0,
                "amplitude": 1.0,
            },
            marks=pytest.mark.xfail,
        ),
        pytest.param(
            LegacyPerturbationParameter,
            {
                "splits": ["1,2"],
                "state": 0,
                "time_on": -50.0,
                # "time_off": 50.0,
                "sigma_on": 10.0,
                "sigma_off": 10.0,
                "amplitude": 1.0,
            },
            marks=pytest.mark.xfail,
        ),
    ],
)
def test_population_from_dict(iact, kwargs, base_kwargs, np_field, timep):
    """Tests the relevant interactor methods using the manual population."""

    full_args = {**kwargs, **base_kwargs}

    # Provide the field for the NanoParticleParameter
    if "field" in full_args:
        full_args["field"] = np_field

    if "time" in full_args:
        full_args["time"] = timep

    instance = iact(**full_args)

    # assert instance.is_complete()

    assert isinstance(instance._matrix(t=0.0, dtt=1.0, idx=0), np.ndarray)
    assert isinstance(instance._matrix(t=0.0, dtt=1.0, idx=0, print_e=True), tuple)


def test_interactor_comparison(infile, potential):
    """Small test to assure the comparison capabilities of the interactors"""

    static = StaticInteractor.from_file(infile)
    dummy = DummyInteractor.from_file(infile)
    jcoupler = JCouplingParameter.from_file(infile)
    field = FieldParameter.from_file(infile)
    nanop = NanoparticleParameter.from_file(infile, system=potential)
    legacy = LegacyPerturbationParameter.from_file(infile)

    # Identity
    for interactor in [dummy, jcoupler, field, legacy, nanop]:
        assert interactor == interactor

    # Comparison
    slist = sorted([dummy, static, jcoupler, field, legacy, nanop], key=interactor_key)

    assert slist == [dummy, static, jcoupler, field, legacy, nanop]


# @pytest.mark.parametrize(
#     "inputdata,expected_num,expected_out",
#     [
#         ("1", 1, [[1, 1]]),
#         ("1,2", 1, [[1, 2]]),
#         ("1, 2", 1, [[1, 2]]),
#         ("1, 2", 1, [[1, 2]]),
#         (["1, 1", "1, 2"], 2, [[1, 1], [1, 2]]),
#         ([1], 1, [[1, 1]]),
#         ([[1, 1]], 1, [[1, 1]]),
#         ([[1, 1], [1, 2], [2, 2]], 3, [[1, 1], [1, 2], [2, 2]]),
#     ],
# )
# def test_update_pairs(inputdata, expected_num, expected_out, base_kwargs):
#     """Unit testing the process_partners() function."""
#     numpairs = InteractorParameter(**base_kwargs).update_pairs(
#         inputdata, indexed_start_zero=True, include_self=True
#     )

#     logging.info(numpairs)
#     num, pairs = numpairs

#     assert num == expected_num
#     assert pairs == expected_out
