# -*- coding: utf-8 -*-

# pylint: disable=c-extension-no-member

"""
Utilities to generate adiabatic PES for wavefunction optimisation.
"""
# import itertools as it
import logging
from collections import namedtuple
from typing import Tuple

import numba as nb
import numpy as np

logger = logging.getLogger(__name__)
DiagReturn = namedtuple("DiagReturn", ["umat", "umat1", "ham_diag"])


@nb.njit(parallel=True, fastmath=True)
def transform_ham_2d(
    umat: np.ndarray, umat1: np.ndarray, ham: np.ndarray
) -> np.ndarray:

    """
    Numba-based basis batch transformation for 2D (M x M x 3 x 3) potential energy surfaces.

    Args:
        umat (np.ndarray): Forward transformation matrix (MxM x 3x3)
        umat1 (np.ndarray): Inverse of umat (MxM x 3x3)
        ham (np.ndarray): Potential energy hamiltonian (MxM x 3x3)

    Raises:
        NotImplementedError: If ndim of the Hamiltonian is not 4.
        NotImplementedError: If the number of states is not equal to 3.

    Returns:
        np.ndarray: Basis-transformed Hamiltonian.
    """

    # pylint: disable=not-an-iterable, invalid-name

    # For the compiler
    if not ham.ndim == 4:
        raise NotImplementedError("ndim of the Hamiltonian != 4")
    if not ham.shape[-2] == 3 or not ham.shape[-1] == 3:
        raise NotImplementedError("Shape of the Hamiltonian != (3, 3)")

    # op: stij, stjk, stkl -> stil
    res = np.zeros_like(ham)

    # Do the loop
    for s in nb.prange(ham.shape[0]):
        for t in nb.prange(ham.shape[1]):

            acc = np.zeros((3, 3))

            for i in range(3):
                for j in range(3):
                    for k in range(3):
                        for l in range(3):
                            acc[i, l] += (
                                umat[s, t, i, j] * ham[s, t, j, k] * umat1[s, t, k, l]
                            )
            res[s, t] = acc

    return res


@nb.njit(parallel=True, fastmath=True)
def transform_ham_1d(
    umat: np.ndarray, umat1: np.ndarray, ham: np.ndarray
) -> np.ndarray:
    """
    Numba-based basis batch transformation for 1D potential energy surfaces.

    Args:
        umat (np.ndarray): Forward transformation matrix (MxM x 3x3)
        umat1 (np.ndarray): Inverse of umat (MxM x 3x3)
        ham (np.ndarray): Potential energy hamiltonian (MxM x 3x3)

    Raises:
        NotImplementedError: If ndim of the Hamiltonian is not 3.
        NotImplementedError: If the number of states is not equal to 3.

    Returns:
        np.ndarray: Basis-transformed Hamiltonian.
    """

    # pylint: disable=not-an-iterable, invalid-name
    # For the compiler
    if not ham.ndim == 3:
        raise NotImplementedError("ndim of the Hamiltonian != 3")
    if not ham.shape[-2] == 3 or not ham.shape[-1] == 3:
        raise NotImplementedError("Shape of the Hamiltonian != (3, 3)")

    # op: sij, sjk, skl -> sil
    res = np.zeros_like(ham)

    # Do the loop
    for s in nb.prange(ham.shape[0]):

        acc = np.zeros((3, 3))

        for i in range(3):
            for j in range(3):
                for k in range(3):
                    for l in range(3):
                        acc[i, l] += umat[s, i, j] * ham[s, j, k] * umat1[s, k, l]
        res[s] = acc

    return res


@nb.njit(fastmath=True)
def hermitian_get_umat(mat: np.ndarray, idxs: Tuple[int, int] = (1, 2)) -> np.ndarray:
    """
    This function is a Sympy generated function to calculate
    the transformation (diagonalisation) matrix P of a 2x2 matrix
    of the form [[a, b], [b, c]].

    ```python
    a, b, c = sym.symbols("a b c")

    M = sym.Matrix([
        [a, b],
        [b, c]
    ])

    P, D = M.diagonalize()

    printer = NumPyPrinter()
    printer.doprint(sym.simplify(P))

    ```

    Args:
        mat (np.ndarray): Real-valued 2D matrix to calculate the transformation
            matrix from.
        indices (Tuple[int]): Specifies the indices that make up the 2x2 matrix. Defaults to (1,2)

    Returns:
        np.ndarray: Inverse of the diagonalisation matrix of the input matrix.
    """
    umat = np.eye(mat.shape[0])

    v_d1 = mat[idxs[0], idxs[0]]
    coup = mat[idxs[1], idxs[0]]
    v_d2 = mat[idxs[1], idxs[1]]

    intermed = v_d1 ** 2 - 2 * v_d1 * v_d2 + v_d2 ** 2 + 4 * coup ** 2

    umat[idxs[0], idxs[0]] = 0.5 * (v_d1 - v_d2 - np.sqrt(intermed)) / coup
    umat[idxs[0], idxs[1]] = 0.5 * (v_d1 - v_d2 + np.sqrt(intermed)) / coup
    umat[idxs[1], idxs[0]] = umat[idxs[1], idxs[1]] = 1.0

    return umat


@nb.njit(fastmath=True)
def hermitian_get_umat_inv(
    mat: np.ndarray, idxs: Tuple[int, int] = (1, 2)
) -> np.ndarray:
    """
    This function is a Sympy generated function to calculate the inverse of the  transformation
    matrix P of a 2x2 matrix of the form [[a, b], [b, c]].

    The code is based on the following Sympy code:
    ```python
    a, b, c = sym.symbols("a b c")

    M = sym.Matrix([
        [a, b],
        [b, c]
    ])

    P, D = M.diagonalize()

    printer = NumPyPrinter()
    printer.doprint(sym.simplify(P**-1))

    ```

    Args:
        mat (np.ndarray): Real-valued symmetric 2D matrix to calculate the transformation
            matrix from.
        indices (Tuple[int]): Specifies the indices that make up the 2x2 matrix. Defaults to (1,2)

    Returns:
        np.ndarray: Inverse of the diagonalisation matrix of the input matrix.
    """
    umat1 = np.eye(mat.shape[0])

    v_d1 = mat[idxs[0], idxs[0]]
    coup = mat[idxs[1], idxs[0]]
    v_d2 = mat[idxs[1], idxs[1]]

    intermed = v_d1 ** 2 - 2 * v_d1 * v_d2 + 4 * coup ** 2 + v_d2 ** 2

    umat1[idxs[0], idxs[0]] = -coup / np.sqrt(intermed)
    umat1[idxs[1], idxs[0]] = coup / np.sqrt(intermed)

    umat1[idxs[0], idxs[1]] = (
        0.5 * (v_d1 - v_d2 + np.sqrt(intermed)) / np.sqrt(intermed)
    )
    umat1[idxs[1], idxs[1]] = (
        0.5 * (-v_d1 + v_d2 + np.sqrt(intermed)) / np.sqrt(intermed)
    )

    return umat1


@nb.njit(parallel=True, fastmath=True)
def diagonalize_ham_exc(ham: np.ndarray, idxs: Tuple[int, int] = (1, 2)) -> DiagReturn:
    """Entry routine for the diagonalisation of a 1D (M x 3 x 3) or 2D (M x M x 3 x 3)
    (with gridsize M) Hamiltonian. Uses a Sympy-generated diagonalisation algorithm for
    a real-valued 2x2 matrix.

    Args:
        ham (np.ndarray): Hamiltonian batch matrix.
        idxs (Tuple[int, int], optional): Tuple of the indices that span the 2x2 submatrix.
            Defaults to (1, 2).

    Raises:
        NotImplementedError: If the dimensionality of the hamiltonian is
            "ham.ndim not in {3, 4}".

    Returns:
        DiagReturn: NamedTuple wrapper containing the forward transformation matrix ("umat"),
            inverse of this matrix ("umat1") and the diagonalised Hamiltonian ("ham_diag").
    """

    # pylint: disable=not-an-iterable

    umat = np.zeros_like(ham)
    umat1 = np.zeros_like(ham)

    if ham.ndim == 3:
        # 1D PES
        for i in nb.prange(ham.shape[0]):
            umat[i, :] = hermitian_get_umat(ham[i], idxs=idxs)
            umat1[i, :] = hermitian_get_umat_inv(ham[i], idxs=idxs)

    elif ham.ndim == 4:
        # 2D PES
        for i in nb.prange(ham.shape[0]):
            for j in nb.prange(ham.shape[1]):
                umat[i, j, ...] = hermitian_get_umat(ham[i, j, ...], idxs=idxs)
                umat1[i, j, ...] = hermitian_get_umat_inv(ham[i, j, ...], idxs=idxs)

    else:
        raise NotImplementedError(
            "Only 1D and 2D vibronic Hamiltonians are currently supported."
        )

    # Calculate the diagonalised Hamiltonian
    if ham.ndim == 4:
        ham_diag = transform_ham_2d(umat, umat1, ham)
    else:
        ham_diag = transform_ham_1d(umat, umat1, ham)

    return DiagReturn(umat=umat, umat1=umat1, ham_diag=ham_diag)


def batch_vec_to_diag(mat: np.ndarray):
    """Convert a batch vector with arbitrary batch dimensions to a batch diagonal matrix.

    Args:
        mat (np.ndarray): Batch diagonal matrix (MxMx...xM x N)

    Returns:
        np.ndarray: Batch vector (MxMx...xM x NxN)
    """

    batch_m = np.zeros((*mat.shape[:-1], mat.shape[-1], mat.shape[-1]))
    diag = np.arange(mat.shape[-1])

    batch_m[..., diag, diag] = mat

    return batch_m


def batch_diag_to_vec(mat: np.ndarray):
    """Convert a diagonal matrix batch with arbitrary batch dimensions back to a batch vector.

    Args:
        mat (np.ndarray): Batch diagonal matrix (MxMx...xM x NxN)

    Returns:
        np.ndarray: Batch vector (MxMx...xM x N)
    """

    return np.diagonal(mat, axis1=-2, axis2=-1).copy()
