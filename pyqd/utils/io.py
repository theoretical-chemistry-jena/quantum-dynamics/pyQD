# -*- coding: utf-8 -*-
"""pyQD utilities for IO purposes."""

import json
import logging
from datetime import datetime
from pathlib import Path
from typing import List, Tuple, Union

import matplotlib.cm as cmx
import matplotlib.colors as col
import matplotlib.pyplot as plt
import numpy as np
import pint
import tqdm.auto as tqdm
import yaml

from ..exceptions import PyQDInputError

logger = logging.getLogger(__name__)


# ! Process partner information for perturbation and pair-wise interactions
def process_partners(
    partner_input: Union[str, List[Union[str, int]]],
    include_self: bool = False,
    indexed_start_zero: bool = False,
) -> List[Tuple[int, int]]:
    """Process partnering information for interacting (typically off-diagonal) terms.
        Handles specific information such as "1", [1], 1, etc.

    Args:
        partner_input (Union[str, List[str, int]]):
            Input as specified in the .yaml or .json file.
        n_s (int): Number of states.
        include_self (bool, optional): [description]. Defaults to False.
        indexed_start_zero(bool, optional):
    Returns:
        List[Tuple[int, int]]: [description]
    """

    if not isinstance(partner_input, (list, tuple)):
        partner_input = [str(partner_input)]

    partner_list = []

    # If the input is indexed form zero, no need to subtract anything
    redux = 0 if indexed_start_zero else 1

    for ii in partner_input:

        try:
            splitd = ii.split(",")
        except AttributeError:
            splitd = [ii]

        if len(splitd) == 1 and include_self:
            splitd += splitd
        elif len(splitd) == 1 and not include_self:
            continue
        else:
            pass

        aux = tuple(int(jj) - redux for jj in splitd)

        if any([j < 0 for j in aux]):
            raise PyQDInputError("Negative partner indices are not allowed!")

        partner_list.append(aux)

    nr_interaction = len(partner_list)

    logger.debug(partner_list)
    return nr_interaction, partner_list


# def _json_object_hook_dict(data: Dict):
#     """Basis from https://stackoverflow.com/questions/6578986/
#     Object hook to dictionary. Reads pyQD input files and intrinsicly converts the
#     strings to pint.Quantity(s) if possible. If that fails,
#     it's assumed to be a string parameter.

#     Args:
#         data (Dict): JSON-decoded dictionary used by the json.loads function.

#     Returns:
#         Dict: Processed json dictionary.
#     """
#     for key, value in data.items():
#         if isinstance(value, str):
#             try:
#                 data[key] = value
#             except (UndefinedUnitError, DimensionalityError, DefinitionSyntaxError):
#                 aux = value.split(" ")
#                 if len(aux) == 1:
#                     data[key] = aux[0]
#                     logger.debug("String parameter '{}'".format(aux[0]))
#                 else:
#                     logger.warning(
#                         "Dimensionless number parameter '{}'. Is that correct?".format(
#                             value
#                         )
#                     )
#                     data[key] = ureg.Quantity(aux[0], "dimensionless")

#     return data


def open_yaml_or_json(file: Union[Path, str]):
    if not isinstance(file, Path):
        file = Path(file).resolve()

    with open(file, "r", encoding="utf-8") as f:
        data = f.read()

    if file.suffix == ".yaml":
        datastr = json.dumps(yaml.load(data, Loader=yaml.FullLoader))

    elif file.suffix == ".json":
        datastr = data
    else:
        raise PyQDInputError("Input format not understood.")

    data = json.loads(datastr)  # _json_object_hook_dict

    return data


def observables_from_file(file, include_norm=True):
    """Get the observables part from an input file"""
    data = open_yaml_or_json(file)

    try:
        obs = data["observables"]
    except KeyError:
        obs = {}

    if include_norm:
        obs["norm"] = {}

    return obs


def input_serialiser(_, __, value):
    """
    Helper function to correctly serialise a field of the PyQDParameter instances.

    pathlib.Path --> str.
    datetime.datetime --> str.
    pint.Quantity --> str

    """

    if isinstance(value, Path):
        return str(value)

    if isinstance(value, datetime):
        return value.isoformat()

    if isinstance(value, pint.Quantity):
        return str(value)

    if isinstance(value, np.ndarray):
        return repr(value)

    return value


def input_filter(attrib, _, exclude_base=False, exclude_underscore=False):

    if exclude_underscore and attrib.name[0] == "_":
        return False

    if exclude_base and attrib.name in ["n_s", "n_m", "n_g"]:
        return False

    return True


class TqdmStream(object):
    """
    Basic tqdm stream object to use logging with tqdm.
    """

    @classmethod
    def write(cls, msg):
        """Stream message to tqdm.write"""
        tqdm.tqdm.write(msg, end="")


def get_current_output_file(directory, fpattern="*RTP*.h5"):
    """Quickly get the most current output file from a directory."""
    # Get the subdirectories
    subdirs = sorted([x for x in directory.glob("*") if x.is_dir()])

    # Use the last subdirectory
    try:
        subdir = subdirs[-1]
    except IndexError:
        subdir = directory

    # Get the RTP file from there. This may fail
    try:
        file = list(subdir.glob(fpattern))[0]
        return file

    except IndexError as e:
        logging.warning(e)
        return None


def interpolate_colormap(num, cm_name="viridis", darker=0.0):
    """
    Interpolate a specified colormap onto `num` points.
    """

    cmap = plt.get_cmap(cm_name)

    c_norm = col.Normalize(vmin=0, vmax=num - 1)
    scalar_map = cmx.ScalarMappable(norm=c_norm, cmap=cmap)

    color_vals = [list(scalar_map.to_rgba(i))[:-1] for i in range(num)]

    if darker > 0.0:
        color_vals = col.rgb_to_hsv(color_vals)

        c_vals = [list(tup) for tup in zip(*color_vals)]
        c_vals[-1][:] = [ii * (1.0 - darker) for ii in c_vals[-1]]
        color_vals = list(zip(*c_vals))

        color_vals = list(col.hsv_to_rgb(color_vals))

    # print(colorVals)
    return color_vals
