# -*- coding: utf-8 -*-
"""
This is a collection of handy conversion factors used for evaluation and interally.
"""

import logging
from typing import Union

import pint

from ..exceptions import PyQDInputError
from .io import process_partners

logger = logging.getLogger(__name__)
ureg = pint.UnitRegistry()

# Energy units
AU2EV = ureg.Quantity("1 / a_u_time").to("1 eV", "sp").m
EH2EV = ureg.Quantity("1 hartree").to("1 eV", "sp").m
AU2FS = ureg.Quantity("1 a_u_time").to("1 fs", "sp").m

# Units of space
BH2ANG = ureg.Quantity("1 bohr").to("1 angstrom", "sp").m


def _pair_converter(pairs, include_self=False, indexed_start_zero=True):
    """
    Update the pair information by parsing the input pairs information to numeric
    information.
    """

    if isinstance(pairs, (list, tuple)):
        try:
            auxl = [
                isinstance(item, (list, tuple)) and len(item) == 2 for item in pairs
            ]
            auxl.append([isinstance(i, int) for item in pairs for i in item])

            if all(auxl):
                return pairs

        except TypeError:
            pass

    _, pairs = process_partners(
        pairs, include_self=include_self, indexed_start_zero=indexed_start_zero
    )
    return pairs


def _unit_converter(str_unit, unit):
    """Converts a string input into the corresponding pint quantity."""

    try:
        if isinstance(str_unit, (list, tuple)):
            return [pint.Quantity(u).to(unit, "sp") for u in str_unit]

        return pint.Quantity(str_unit).to(unit, "sp")

    except pint.DimensionalityError as e:
        logger.debug(
            "Could not convert '{unit1}' to '{unit2}'. "
            "Assuming a float parameter".format(unit1=e.units1, unit2=e.units2)
        )

        if isinstance(str_unit, (list, tuple)):
            return [pint.Quantity(u, "dimensionless") for u in str_unit]

        return pint.Quantity(str_unit, "dimensionless")


def _state_converter(state_var):
    # Init_state should be a list
    if not isinstance(state_var, list):
        try:
            state_var = [int(state_var)]
        except ValueError:
            state_var = [int(i) for i in state_var.split(",")]
    else:
        state_var = [int(i) for i in state_var]

    return state_var


def _itp_state_validator(instance, attribute, value):
    possib = list(range(instance.n_s))
    if any([i not in possib for i in value]):
        raise ValueError(
            f"Illegal state specifier for attr {attribute}: {value} not all in {possib}."
        )


def _mu_converter(value: Union[dict, list]):

    if isinstance(value, dict):
        mu_lot = []

        for key, val in value.items():
            try:
                idxs = tuple(int(i) for i in key.split("->"))
                mu_lot.append((idxs, float(val)))

            except ValueError as e:
                raise PyQDInputError(
                    "Malformed value for 'trans_dipole' encountered. "
                    "Input has to be 'idx1 -> idx2' in the input file."
                ) from e

    else:
        # TODO
        mu_lot = value

    return mu_lot


def _list_converter(value):
    if not isinstance(value, (tuple, list)):
        return [value]

    return value


def _decay_converter(value):
    if isinstance(value, str):
        value = 2 if value == "gaussian" else 1
    elif isinstance(value, int) and value in [1, 2]:
        pass
    else:
        raise ValueError("Incorrect value for the decay.")

    return value


def interactor_key(item):
    """
    Key function to sort Interactor instances in a list using the sorted() function
    """
    try:
        return (
            item.__class__.PRIORITY,
            item._num,  # pylint: disable=protected-access
            item.idx,
        )
    except AttributeError:
        return (item.__class__.PRIORITY, item._num)  # pylint: disable=protected-access
