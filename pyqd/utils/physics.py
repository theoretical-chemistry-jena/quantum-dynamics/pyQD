# -*- coding: utf-8 -*-
"""pyQD utilities for physical quantities."""

# -*- coding: utf-8 -*-
from typing import Callable, List, Optional, Tuple, Union

import numba
import numpy as np
import opt_einsum as oe
from pint import UnitRegistry

from ..exceptions import PyQDPhysicsError

ureg = UnitRegistry()


def gaussian_norm(
    x: Union[np.ndarray, float], mu: float, sig: float
) -> Union[np.ndarray, float]:
    """Compute the normalised (textbook version) of a gaussian probability distribution
    function in 1D, on a single x float value or a np.ndarray position space grid.

    Args:
        x (Union[np.ndarray, float]): Point or grid at which to calculate the gaussian distribution.
        mu (float): Center value of the distribution.
        sig (float): Sigma width of the distribution.

    Returns:
        Union[np.ndarray, float]: The gaussian 1D probability distribution function.
    """
    return 1.0 / (np.sqrt(2.0 * np.pi) * sig) * np.exp(-(((x - mu) / sig) ** 2) / 2)


def gaussian_nd_nonnorm(
    meshes: List[np.ndarray],
    centers: List[float],
    sigmas: List[float],
    amplitude: float,
    randomize: bool = False,
) -> np.ndarray:
    """Compute a non-normalised (integral != 1) isotropic gaussian normal distribution function
    in arbitrary 'n' dimensions.

    Args:
        meshes (List[np.ndarray]): List of 'n' 1D position grids
        centers (List[float]): List of center coordinates in each of the 'n' dimensions.
        sigma (float): Sigma width of the resulting gaussian. Currently only isotropic sigmas
            are supported
        amplitude (float): Maxmimum value of the resulting distribution
        randomize (bool, optional): Shall the distribution be randomised in every point? Useful for
            imaginary time propagation as a chaotic initial guess wavefunction. Defaults to False.

    Returns:
        np.ndarray: Non-normalised n-dimensional gaussian stored in an n-dimensional np.ndarray
    """
    expon = np.zeros(meshes[0].shape)

    for mesh, center, sigma in zip(meshes, centers, sigmas):
        expon += (mesh - center) ** 2 / sigma ** 2

    if randomize:
        rndm = (np.random.randn(*expon.shape) * 10) - 5
    else:
        rndm = 1.0

    return rndm * amplitude * np.exp(-expon)


# @numba.jit(nopython=True, parallel=True)
def calc_k_grid(n_g: float, dr: float) -> np.ndarray:
    """Calculate the 1D wave vector grid from the position space grid spacing

    Args:
        n_g (float): Number of gridpoints
        dr (float): Position space grid spacing

    Returns:
        np.ndarray: Momentum space grid usable for T propagator definition
    """

    dk = (2 * np.pi) / (dr * n_g)

    k = np.array(
        [ii * dk if ii <= n_g // 2 else -(n_g - ii) * dk for ii in range(n_g)],
        dtype="float64",
    )

    return k


def harmonic_potential_2d(
    xmesh: np.ndarray, ymesh: np.ndarray, center: Tuple[float, float], e_min: float
) -> np.ndarray:
    """DEPRECATED: Potential energy of a quantum harmonic oscillator of a
    a potential energy surface for the potential energy hamiltonian,
    with a given slope.

    Args:
        xmesh (np.ndarray): Numpy meshgrid for the first dimension.
        ymesh (np.ndarray): Numpy meshgrid for the second dimension.
        center (Tuple[float, float]): Tuple giving the minimum position in each dimension.
        e_min (float): Minimum energy of the potential.

    Returns:
        np.ndarray: 2D potential energy surface of a Quantum harmonic oscillator
    """
    x_0, y_0 = center
    zdata = 0.001 * ((xmesh - x_0) ** 2 + (ymesh - y_0) ** 2) + e_min
    return zdata


def harmonic_osc_1d(
    x: Union[np.ndarray, float], minx: float, miny: float, omega: float, mass: float
) -> Union[np.ndarray, float]:
    """Potential energy of a quantum harmonic oscillator at a given x-coordinate or
    a potential energy surface for the potential energy hamiltonian

    Args:
        x (Union[np.ndarray, float]): Spatial coordinate, either an array or a float, in [a.u.]
        minx (float): Spatial minimum of the harmonic oscillator potential well, in [a.u.]
        miny (float): Energetic minimum of the harmonic oscillator potential well, in [a.u.]
        omega (float): Slope of the potential well in units of energy, in [a.u.]
        mass (float): Reduced mass of the system, in [a.u.]

    Returns:
        Union[np.ndarray, float]: Potential energy along the given coordinate.
    """
    return 0.5 * mass * omega ** 2 * (x - minx) ** 2 + miny


@numba.jit(nopython=True)
def calc_norm_2d(wf: np.ndarray, dx: float, dy: float) -> float:
    """Calculate the norm of a 2D wavefunction with non-isotropic X- and Y-coordinates

    Args:
        wf (np.ndarray): 2D array holding the discretised wavefunction
        dx (float): Grid spacing in the first dimension
        dy (float): Grid spacing in the second dimension

    Returns:
        float: The norm of the wavefunction: < Psi(x,y) | Psi(x,y) >
    """
    return np.sum(np.real(np.conjugate(wf) * wf)) * dx * dy


@numba.jit(nopython=True)
def calc_norm_nd(wf: np.ndarray, dr: float, n_m: int) -> float:
    """Calculate the norm of a n-dimensional wavefunction with isotropic spatial coordinates

    Args:
        wf (np.ndarray): n-dimensional array holding the discretised wavefunction
        dx (float): Grid spacing in each dimension dimension
        n_m (int): Dimensionality of the wavefunction

    Returns:
        float: The norm of the wavefunction: < Psi(x1, x2, ..., xn) | Psi(x1, x2, ..., xn) >
    """
    return np.sum(np.real(np.conjugate(wf) * wf)) * dr ** n_m


def calc_norm_nd_statewise(wf: np.ndarray, dr: float, n_m: int) -> np.ndarray:
    """Calculate the norm of a n-dimensional wavefunction with isotropic spatial coordinates

    Args:
        wf (np.ndarray): n-dimensional array holding the discretised wavefunction
        dx (float): Grid spacing in each dimension dimension
        n_m (int): Dimensionality of the wavefunction

    Returns:
        float: The norm of the wavefunction: < Psi(x1, x2, ..., xn) | Psi(x1, x2, ..., xn) >
    """
    axes = tuple(np.arange(n_m))
    return np.sum(np.real(np.conjugate(wf) * wf), axis=axes) * dr ** n_m


@numba.jit(nopython=True)
def calc_ovlp_nd(wf0: np.ndarray, wf1: np.ndarray, dr: float, n_m: int) -> float:
    """Calculate the norm of a n-dimensional wavefunction with isotropic spatial coordinates

    Args:
        wf (np.ndarray): n-dimensional array holding the discretised wavefunction
        dx (float): Grid spacing in each dimension dimension
        n_m (int): Dimensionality of the wavefunction

    Returns:
        float: The norm of the wavefunction: < Psi(x1, x2, ..., xn) | Psi(x1, x2, ..., xn) >
    """
    return np.sum(np.real(np.conjugate(wf0) * wf1)) * dr ** n_m


# @numba.jit(nopython=True)
def gen_e_field(
    t: Union[float, np.ndarray],
    e_0: float,
    omega: float,
    sigma: float,
    t_central: float,
    shift: Optional[float] = 0.0,
    expon: int = 2,
    rwa: bool = False,
) -> Union[float, np.ndarray]:
    """Calculate a classical electric field pulse (i.e., laser pulse) from the formula
    E(t) = exp(-beta * (t-t0)**2) * E0 * cos(2 * pi * omega * (t-t0)).

    Args:
        t (Union[float, np.ndarray]): Float or numpy.ndarray of the time in [a.u.]
        e_0 (float): Central laser field amplitude in [a.u.]
        omega (float): Laser (non-angular) frequency in [1 / a.u.]
        beta (float): Width of the laser pulse in [1 / (a.u.)**2].
            Zero for non-pulsed electric field
        t_central (float): Time of the maximum of the laser pulse in [a.u.]
        shift (float): Phase shift. Default: 0.0
        expon (int): Exponent of the decay function. Default: 2 (Gaussian line shape)

    Returns:
        Union[float, np.ndarray]: Electric field strength at t in [a.u.]

    """
    envelope = np.exp(-((1.0 / sigma) ** expon) * (t - t_central) ** expon) * e_0

    if rwa:
        return envelope * np.exp(-1.0j * 2 * np.pi * omega * (t - t_central) + shift)
    else:
        return envelope * np.cos(2 * np.pi * omega * (t - t_central) + shift)


def build_exec_order_recursive(nr_propagators: int) -> List[int]:
    """Function recursively builds a Split operator splitting pattern based on the
    number of monomers given.

    Example:
        2 Propagators --> [1, 0, 1]
        3 Propagators --> [2, 1, 2, 0, 2, 1, 2]
        ...

    Args:
        nr_propagators (int): Number of propagators to prepare a splitting pattern

    Returns:
        List[int]: Returns the split (index) list for the specified number of propagators
    """

    def _rec(array):
        """
        Local recursion function
        """
        if len(array) == 0:
            return array
        elif len(array) == 1:
            return array

        else:
            larr, mid, uarr = (
                array[: len(array) // 2],
                array[len(array) // 2],
                array[len(array) // 2 + 1 :],
            )

            return np.concatenate(
                (_rec(larr + 1), np.asarray(mid), _rec(uarr + 1)), axis=None
            )

    length = 2 ** (nr_propagators) - 1
    array = np.zeros(length, dtype="int8")
    return _rec(array)


def build_einsum_op(nr_of_matrices: int) -> str:
    """Build the einsum expression for the multi-split propagation scheme.
    Produces operations for batch matrix multiplications of n matrices;
    (n-1) successive multiplication steps.
    The batch dimensions are the first to third-to-last.

    Example:
        n = 2: "...ab,...bc->...ac"
        n = 3: "...ab,...bc,...cd->...ad"
        ...

    Args:
        nr_of_matrices (int): Number of matrices involved in the batch matrix multiplication

    Returns:
        str: Einsum operation string
    """

    all_letters = "".join([oe.get_symbol(ii) for ii in range(nr_of_matrices + 1)])
    einsumlst = []

    # Make n-1 matrix multiplication steps
    for i in range(nr_of_matrices):
        # einsumlst.append(l + al[i+1] + "...")
        einsumlst.append("..." + all_letters[i] + all_letters[i + 1])

    einsumstr = ",".join(einsumlst)
    einsumstr = einsumstr + "->" + "..." + einsumstr[3] + einsumstr[-1]

    return einsumstr


# @numba.njit()
def gen_ptb_v0(
    t: np.ndarray, s_on: float, s_off: float, t_on: float, t_off: float, ampl: float
):
    """Generate a time-dependent perturbation based on the publication of
    J. Wehner, A. Schubert, V. Engel, Chem. Phys. Lett., 541 (2012) 49-53
    The temporal shape of the perturbation resembles a gaussian distribution from "-inf"
    tot_on, then plateau-ing from t_on to t_off and a gaussian falloff from t_off to "+inf"
    On- and offset parameters can be tuned to fit the specific needs.

    Args:
        t (float): Current time in unit [t]
        s_on (float): sigma parameter of the onset gaussian distribution in unit [1/t**2]
        s_off (float): sigma parameter of the offset gaussian distribution in unit [1/t**2]
        t_on (float): start time of the plateau region in unit [t]
        t_off (float): end time of the plateau region in unit [t]
        ampl (float): amplitude of the plateau region in energy units.

    Returns:
        float: Value of the perturbation in units of energy (same unit as ampl).
    """

    ptb = np.where(
        t_on < t,
        np.where(
            t < t_off, ampl, ampl * np.exp(-((1 / s_off) ** 2) * (t - t_off) ** 2)
        ),
        ampl * np.exp(-((1 / s_on) ** 2) * (t - t_on) ** 2),
    )

    return ptb


# @numba.jit(nopython=True)
def gen_ptb_v1(
    t: Union[np.ndarray, float],
    s_off: float,
    rel_ampl: float,
    shift: float,
    expon: int,
    e_0: float,
    omega: float,
    sigma: float,
    t_central: float,
) -> Union[np.ndarray, float]:
    """Generates a time-dependent perturbation based on field parameters provided.
    The field is phase-shifted by the 'shift' arg and the decreasing flank is altered by means
    of the 's_off' parameter, which is a relative modifier to the field 'sigma' parameter.

    Args:
        t (Union[np.ndarray, float]): Time array or distinct point in time.
        s_off (float): Relative gaussian sigma parameter. Multiplied with the field-based 'sigma'.
        rel_ampl (float): Relative amplitude, relative to the field's amplitude.
        shift (float): Phase shift in multiples of pi.
        expon (int): Exponent of the decreasing flank. '1' means an exponential decay,
            '2' means an Gaussian decay.
        e_0 (float): Amplitude of the field at t_central.
        omega (float): (Circular) frequency $\\omega$ of the field.
        sigma (float): Gaussian sigma parameter, aka standard deviation.
        t_central (float): Time point at which the maximum amplitude is reached.

    Returns:
        float: Perturbation strength as np.ndarray or float.
    """

    ptb = np.where(
        t < t_central,
        rel_ampl * gen_e_field(t, e_0, omega, sigma, t_central, shift=shift),
        rel_ampl
        * gen_e_field(
            t, e_0, omega, sigma * s_off, t_central, shift=shift, expon=expon
        ),
    )

    return ptb


def _calc_integral_matrix_ev(
    wf: np.ndarray, operator: np.ndarray, dx: float, n_m: int
) -> float:
    """Calculate the full-system numerical integral, used to calculate
    expectancy values for matrix-based full-system operators,
    usually system energy.

    Args:
        wf (np.ndarray): Wavefunction, with batch dimensions as the first n_m dims
        operator (np.ndarray): Operator matrix, with batch dimensions as the first n_m dims
        dx (float): Grid spacing for isometric grids.
        n_m (int): Number of batch dimensions (-> number of wavefunction dimensions)

    Returns:
        float: Numerical integral value.
    """
    path = [(0, 1), (0, 1)]
    operation = "...a,...ab,...b->"

    return (
        oe.contract(operation, *(np.conjugate(wf), operator, wf), optimize=path)
        * dx ** n_m
    )


def _calc_integral_prop_ev(wf, operator, dx, n_m):
    """
    Calculate the expectation value of a state- and position-dependent operator, such as the
    position and momentum operators $\\hat{{x}}$ and $\\hat{{p}}$, respectively.

    Args:
        wf (np.ndarray): Wavefunction, with batch dimensions as the first n_m dims
        operator (np.ndarray): Operator array, with state dimensions as the first dimension.
        dx (float): Grid spacing for isometric grids.
        n_m (int): Number of batch dimensions (-> number of wavefunction dimensions)

    Returns:
        float: Integral value
    """
    path = [(0, 1), (0, 1)]
    operation = "...b,c...,...b->bc"

    return np.real(
        oe.contract(operation, *(np.conjugate(wf), operator, wf), optimize=path)
        * dx ** n_m
    )


def calc_energy_v_nd(
    wf: np.ndarray,
    operator: np.ndarray,
    dx: float,
    n_m: int,
) -> float:
    """
    Calculate the normalised potential energy value, based on
    https://youtu.be/XQKV-hpsurs?list=PLUl4u3cNGP60cspQn3N9dYRPiyVWDd80G&t=1507

    Args:
        wf (np.ndarray): Wavefunction, with batch dimensions as the first n_m dims
        operator (np.ndarray): Hamiltonian array, with batch dimensions as the first n_m dimension.
        dx (float): Grid spacing for isometric grids.
        n_m (int): Number of batch dimensions (-> number of wavefunction dimensions)

    """

    norm = calc_norm_nd(wf, dx, n_m)
    integral = np.real(_calc_integral_matrix_ev(wf, operator, dx, n_m))

    return np.divide(integral, norm, where=norm != 0)


def calc_energy_t_nd(
    wf: np.ndarray,
    operator: np.ndarray,
    fft_fwd: Callable,
    dx: float,
    n_g: int,
    n_m: int,
) -> float:
    """
    Calculate the normalised kinetic energy value, based on
    https://youtu.be/XQKV-hpsurs?list=PLUl4u3cNGP60cspQn3N9dYRPiyVWDd80G&t=1507

    Args:
        wf (np.ndarray): Wavefunction, with batch dimensions as the first n_m dims
        operator (np.ndarray): Hamiltonian array, with batch dimensions as the first n_m dimension.
        fft_fwd: (pyFFTW.builder): pyFFTW builder callable to do the FFT,
            optimised for the wavefunction.
        dx (float): Grid spacing for isometric grids.
        n_g (float): Number of grid points.
        n_m (int): Number of batch dimensions (-> number of wavefunction dimensions)

    """

    wf_p = wf.copy()
    wf_p = fft_fwd(wf_p)
    dk = (2 * np.pi) / (n_g * dx)

    norm = calc_norm_nd(wf_p, dk, n_m)

    integral = np.real(_calc_integral_matrix_ev(wf_p, operator, dk, n_m))

    return integral / norm


def calc_position_ev(
    wf: np.ndarray, operator: List[np.ndarray], dx: float, n_m: int, norm: np.ndarray
) -> np.ndarray:
    """Calculate the momentum expectancy value of a full wavefunction in n dimensions.
    Works by calculating the momentum expectancy value in momentum space.

    Args:
        wf (np.ndarray): Real-time propagated full wavefunction.
        operator (List[np.ndarray]): Position operator x as np.meshgrid.
        dx (float): Position grid spacing.
        n_m (int): Number of grid dimensions.

    Returns:
        np.ndarray: (n_s, n_m)-sized array of the current momentum expectancy value.
    """
    norms = np.stack([norm] * n_m, axis=1)
    pos = np.divide(
        _calc_integral_prop_ev(wf, operator, dx, n_m),
        norms,
        where=np.logical_not(np.isclose(norms, 0, atol=1e-10)),
    )

    return pos


def calc_position_ev_nonorm(
    wf: np.ndarray, operator: List[np.ndarray], dx: float, n_m: int
) -> np.ndarray:
    """Calculate the momentum expectancy value of a full wavefunction in n dimensions.
    Works by calculating the momentum expectancy value in momentum space.

    Args:
        wf (np.ndarray): Real-time propagated full wavefunction.
        operator (List[np.ndarray]): Position operator x as np.meshgrid.
        dx (float): Position grid spacing.
        n_m (int): Number of grid dimensions.

    Returns:
        np.ndarray: (n_s, n_m)-sized array of the current momentum expectancy value.
    """
    norm = calc_norm_nd_statewise(wf, dx, n_m)
    norms = np.stack([norm] * n_m, axis=1)

    pos = np.divide(
        _calc_integral_prop_ev(wf, operator, dx, n_m),
        norms,
        where=np.logical_not(np.isclose(norms, 0, atol=1e-10)),
    )

    return pos


def calc_momentum_ev(
    wf: np.ndarray,
    operator: np.ndarray,
    fft_fwd: Callable,
    dk: float,
    n_s: int,
    n_m: int,
) -> np.ndarray:
    """Calculate the momentum expectancy value of a full wavefunction in n dimensions.
    Works by calculating the momentum expectancy value in momentum space.

    Args:
        wf (np.ndarray): Real-time propagated state wavefunction.
        operator (np.ndarray): Momentum operator p as np.meshgrid.
        fft_fwd: pyFFTW builder callable to do the FFT, optimised for the wavefunction.
        dx (float): Position grid spacing.
        n_s (int): Number of states in the system.
        n_m (int): Number of grid dimensions.

    Returns:
        np.ndarray: (n_s, n_m)-sized array of the current momentum expectancy value.
    """
    # ! dx is used here for the integral (intead of dk), as we use the
    # ! positional norm from the propagation.
    # ! This is numerically equivalent, but needs to be noted.

    wf_p = wf.copy()
    wf_p = fft_fwd(input_array=wf_p)

    norms = np.zeros((n_s, n_m), dtype=np.float64)

    norm = calc_norm_nd_statewise(wf_p, dk, n_m)
    norms = np.stack([norm] * n_m, axis=-1)

    mom = np.divide(
        _calc_integral_prop_ev(wf_p, operator, dk, n_m),
        norms,
        where=np.logical_not(np.isclose(norms, 0, atol=1e-10)),
    )

    return mom


def trim_automagically(time_data: np.ndarray, t_init_idx: int) -> np.ndarray:
    """https://stackoverflow.com/a/9667121 Finds maxima in the real part of
        the polarisation function and builds a mask from that.

    Args:
        time_data (np.ndarray): Raw time data array.
        t_init_idx (int): Index of the t_initial value on the corresponding time grid.

    Returns:
        np.ndarray: The boolean mask array to trim the time data and the time grid with.
    """
    max_idxs = (np.diff(np.sign(np.diff(np.real(time_data)))) < 0).nonzero()[0] + 1
    mask = np.zeros(time_data.shape, dtype=bool)
    trimmed_max = max_idxs[max_idxs > t_init_idx]
    try:
        mask[trimmed_max[0] : trimmed_max[-1]] = True
    except IndexError as e:
        raise PyQDPhysicsError("Sorry, I haven't found any maxima.") from e

    return mask


def calc_polarisation_ev(wf: np.ndarray, operator, dx, n_m):
    """Calculate the polarisation expectancy value of a full wavefunction.
        As the recording typically is != t_init, propagation time is required to check.
        After FFT the polarisation function should give a physically
        meaningful vibronic spectrum.

    Args:
        wf (np.ndarray): Real-time propagated single state wavefunction
        operator (np.ndarray): Polarisation operator mu.
        t (float): Current propagation time
        dx (float): Position grid spacing
        n_m (int): Number of grid dimensions

    Returns:
        float: [description]
    """
    return _calc_integral_matrix_ev(wf, operator, dx, n_m)


def calc_polarisation_spectrum(
    pol_timedata: np.ndarray,
    tgrid: np.ndarray,
    t_start: float,
    real_input: bool = False,
    trim_idx: Optional[int] = None,
    decay: Optional[float] = None,
    io_units: Tuple[str, str] = ("1 / a_u_time", "eV"),
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Calculate the Polarisation-function based electronic spectrum from the polarisation function.

    Args:
        pol_timedata (np.ndarray): Polarisation function.
        tgrid (np.ndarray): Discrete time grid, corresponding to the polarisation function.
        t_start (float): At which time should the polarisation function be used?
        trim_idx (Optional[int], optional): Manual override for the right trimming index.
            If none is given, the trim index will be approximated automagically. Defaults to None.
        decay (Optional[float], optional): Manual override for the decay time constant.
            Determines the line shape of the spectrum. Defaults to None.
        io_units (Tuple[str, str], optional): Unit conversion tuple for the frequency grid.
            Tuple has to consist of two pint-readable unit strings.
            Defaults to ("1 / a_u_time", "eV").

    Returns:
        Tuple[np.ndarray, np.ndarray, np.ndarray]: Tuple of
            1. The frequency grid.
            2. The spectrum in it's raw complex form
            3. The absolute value of the spectrum.
    """

    # Build a small unit converter
    unit_conv = ureg.Quantity(1, io_units[0]).to(io_units[-1], "sp").m

    # Get the index of the propagation start time
    t_init_idx = np.argmin(np.abs(tgrid - t_start))

    if trim_idx is None:
        mask = trim_automagically(pol_timedata, t_init_idx)

    else:
        mask = np.zeros(pol_timedata.shape, dtype=bool)
        mask[t_init_idx : -max(1, trim_idx)] = True

    if not np.any(mask):
        raise PyQDPhysicsError(
            "The mask removed all the elements. "
            "You might not have propagated long enough to record one entire period."
        )

    pol_trunc = pol_timedata[mask]
    tgr_trunc = tgrid[mask]

    if decay is None:
        decay = (tgr_trunc[-1] - tgr_trunc[0]) / 3

    pol_trunc *= np.exp(-((tgr_trunc - tgr_trunc[0]) ** 2) / decay ** 2)

    pol_mir = np.zeros(2 * pol_trunc.size - 1, dtype=np.complex128)
    pol_mir[: pol_trunc.size] = pol_trunc[:]
    pol_mir[pol_trunc.size :] = np.conjugate(pol_trunc[:0:-1])

    fftfunc = np.fft.rfft if real_input else np.fft.ifft
    fftfreqfunc = np.fft.rfftfreq if real_input else np.fft.fftfreq

    spec_p = fftfunc(pol_mir)
    spec_p_abs = np.imag(spec_p) ** 2 + np.real(spec_p) ** 2

    wgrid = fftfreqfunc(pol_mir.size, tgr_trunc[1] - tgr_trunc[0]) * unit_conv

    return wgrid, spec_p, spec_p_abs


def calc_autocorrelation(
    wf: np.ndarray, wf0: np.ndarray, e_0: float, t: float, dx: float, n_m: int
):
    """Calculate the autocorrelation of a wavefunction with the
        initial (relaxed) wavefunction that was propagated by exp(-i*E0*t).
        That gives (after FFT) the "energy-correct" vibrationally
        resolved spectrum.

    Args:
        wf (np.ndarray): Real-time propagated single state wavefunction
        wf0 (np.ndarray): Initial wavefunction as calculated by ITP
        e0 (float): Energy of the initial wavefunction
        t (float): Current propagation time
        dx (float): Position grid spacing
        n_m (int): Number of grid dimensions

    Returns:
        float: [description]
    """
    axes = tuple(np.arange(n_m))
    return (
        np.sum(np.real(np.exp(1j * e_0 * t) * np.conjugate(wf0) * wf), axis=axes)
        * dx ** n_m
    )


def find_nearest_timepoint(value: float, tgrid: np.ndarray):
    """
    Finds the nearest discretized time grid point based on standard time parameters.
    Idea comes from https://stackoverflow.com/a/2566508.

    Args:
        value (float): Time point to look for.
        tparam (Tuple[float, float, int]): Standard time parameter tuple.
    Returns:
        float: The nearest time value
    """

    idx = (np.abs(tgrid - value)).argmin()
    return tgrid[idx]
