# -*- coding: utf-8 -*-

"""Total energy observable for pyQD."""

import logging

import attr
import h5py
import numpy as np
from pint import UnitRegistry

from ..utils.physics import calc_energy_t_nd, calc_energy_v_nd
from ._base import ObservableParameter

logger = logging.getLogger(__name__)
ureg = UnitRegistry()


@attr.s(slots=True)
class TotalEnergyObservable(ObservableParameter):
    """
    Observable calculating the total energy of the system, separated in
    potential energy and kinetic energy.
    """

    INPUT_STR_ID = "total_energy"
    fftf = attr.ib(default=None, init=False)

    def obs_prepare(
        self, tgridsize, fft_f, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Prepare the output arrays and store the pyFFTW builder."""
        self._array = np.zeros((2, tgridsize), dtype=np.float64)
        self.fftf = fft_f
        return self

    def obs_update(
        self, t, idx, wf, operator_v, operator_t, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Update the observable arrays."""
        self._array[0, idx] = calc_energy_v_nd(wf, operator_v, self.dx, self.n_m)
        self._array[1, idx] = calc_energy_t_nd(
            wf, operator_t, self.fftf, self.dx, self.n_g, self.n_m
        )

        return None

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Create a new h5 group and print the arrays in there."""
        h5e = h5file.create_group("total_energy")
        h5e.create_dataset("energyT", data=self._array[:1, :])
        h5e.create_dataset("energyV", data=self._array[-1:, :])

        return None
