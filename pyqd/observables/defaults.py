# -*- coding: utf-8 -*-
""" pyQD defaults for the observables """

from dataclasses import dataclass, field
from typing import Dict


@dataclass
class Observables:
    """Observable defaults."""

    position: Dict = field(default_factory=dict)
    momentum: Dict = field(default_factory=dict)
    total_energy: Dict = field(default_factory=dict)
    spectrum_correlation: Dict = field(default_factory=dict)
    spectrum_reduced_density: Dict = field(default_factory=dict)
    spectrum_polarisation: Dict = field(default_factory=lambda: {"field_idx": 0})
