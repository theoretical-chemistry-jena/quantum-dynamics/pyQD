# -*- coding: utf-8 -*-
"""EigenfunctionCoefficients observable for pyQD."""

import logging

import attr
import h5py
import numpy as np
import opt_einsum as oe
from pint import UnitRegistry

from ..exceptions import PyQDInputError
from ._base import ObservableParameter

logger = logging.getLogger(__name__)
ureg = UnitRegistry()


@attr.s(slots=True)
class EigenfunctionCoefficients(ObservableParameter):
    """
    Calculate the correlation spectrum based on the autocorrelation function.
    Requires a relaxed groundstate wavefunction as input.
    """

    INPUT_STR_ID = "eigenfunction_coefficients"
    UNITS = {"dx": ("angstrom", "bohr"), "itp_energies": ("eV", "hartree")}

    itp_wfs = attr.ib(default=None, init=False)
    itp_energies = attr.ib(default=None, init=False)
    tgrid = attr.ib(default=None, init=False)
    _expr = attr.ib(default=None, init=False)

    def obs_prepare(
        self, tgridsize, itp_integrator, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Prepare the observable array and the other parameters required.
        """

        if not itp_integrator.relaxed:
            raise PyQDInputError(
                "The ITP integrator you provided is not relaxed. Aborting."
            )

        self.itp_wfs = itp_integrator.lower.astype(np.complex128)
        if self.itp_wfs.shape[-2] != self.n_s:
            raise PyQDInputError(
                "The ITP integrator did optimize wavefunctions for all states in your system. "
                "You won't be able to get any meaningful information like that. Aborting."
            )
        if self.itp_wfs.shape[-2] < 5:
            logger.warning(
                "You calculated {} vibronic ITP states. I recommend you optimise at least"
                "5 (better 10) states, to be able to include significant parts of "
                "your wavefunction.".format(self.itp_wfs.shape[-2])
            )

        self.itp_energies = np.asarray(itp_integrator.energies).T
        self._array = np.zeros(
            (*self.itp_wfs.shape[self.n_m :], tgridsize), dtype=np.float64
        )

        opn = "...ij,...i->ij"
        self._expr = oe.contract_expression(
            opn,
            *(np.conjugate(self.itp_wfs), self.itp_wfs.shape[:-1]),
            constants=[0],
            optimize="dp",
        )

        return self

    def obs_update(
        self, t, idx, wf, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Update the observable array.
        """
        self._array[..., idx] = np.real(self._expr(wf, backend="numpy"))

        return None

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Calculate the spectrum and the frequency grid, and store everything to the .h5 output file.
        """

        h5file.create_dataset("coefficients", data=self._array)

        return None
