# -*- coding: utf-8 -*-

"""Norm observable for pyQD."""

import logging

import attr
import h5py
import numpy as np

from ..utils.physics import calc_norm_nd_statewise
from ._base import ObservableParameter

logger = logging.getLogger(__name__)


@attr.s(slots=True)
class NormObservable(ObservableParameter):
    """
    Most basic implementation of the norm (or state-wise population) observable.
    Is automatically used in every QDIntegrator computation, unless otherwise requested.
    """

    INPUT_STR_ID = "norm"
    _loginfo = attr.ib(default=None)

    def __attrs_post_init__(self):
        self._loginfo = (
            "Norm @ {:5.0f} (t={:.3f} a.u.): "
            + "; ".join([f"state {ii}: {{:.5f}}" for ii in range(self.n_s)])
            + ". Cumultd: {:.5f}"
        )

    def obs_prepare(self, tgridsize, **kwargs):  # pylint: disable=unused-argument
        """Prepare the observable array."""

        self._array = np.zeros((self.n_s, tgridsize), dtype=np.float64)

        return self

    def obs_update(
        self, t, idx, wf, **kwargs
    ):  # pylint: disable=unused-argument,arguments-differ
        """Update the observable array in each time step."""

        norm = calc_norm_nd_statewise(wf, self.dx, self.n_m)
        self._array[:, idx] = norm

        logger.info(self._loginfo.format(idx, t, *norm, np.sum(norm)))

    def obs_to_outfile(
        self,
        h5file: h5py.File,
    ):  # pylint: disable=unused-argument, arguments-differ
        """Print the observable array to the .h5 file."""

        super().obs_to_outfile(h5file, "norm")
