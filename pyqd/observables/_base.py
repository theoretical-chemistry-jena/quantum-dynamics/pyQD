# -*- coding: utf-8 -*-
"""ObservableParameter base class for pyQD."""

import logging

import attr

from ..parameters._base import PyQDParameter

logger = logging.getLogger(__name__)


@attr.s(slots=True)
class ObservableParameter(PyQDParameter):
    """
    Base class for observables used in pyQD. Implements the most basic functions.
    """

    INPUT_STR_ID = ""
    dx: float = attr.ib()
    _array = attr.ib(default=None, init=False)

    UNITS = {"dx": ("angstrom", "bohr")}

    def as_input(self, **kwargs):  # pylint: disable=arguments-differ

        unused_kwargs = ["gs_wf", "fftf", "tgrid"]
        rawdict = super().as_input(exclude_base=True, **kwargs)

        for unused in unused_kwargs:
            if unused in rawdict[self.INPUT_STR_ID]:
                rawdict[self.INPUT_STR_ID].pop(unused)

        return rawdict

    @property
    def data(self):
        """Return the storage array for easier plotting"""
        return self._array

    def obs_prepare(self, tgridsize, **kwargs):  # pylint: disable=unused-argument
        """
        Dummy method for the observables preparation functionality.
        Subclass arguments may differ due to different requirements.
        """
        return self

    def obs_update(self, t, idx, **kwargs):  # pylint: disable=unused-argument
        """
        Dummy method for the observables updating functionality.
        Subclass arguments may differ due to different requirements.
        """
        return None

    def obs_to_outfile(self, h5file, name):
        """
        Dummy method for the observables outputting functionality.
        Subclass arguments differ due to the names being parsed here.
        """
        h5file.create_dataset(name, data=self._array)

        return None
