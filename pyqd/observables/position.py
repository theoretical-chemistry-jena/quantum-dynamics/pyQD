# -*- coding: utf-8 -*-
"""Position expectation value observable for pyQD."""

import logging

import attr
import h5py
import numpy as np
from pint import UnitRegistry

from ..utils.physics import calc_position_ev_nonorm
from ._base import ObservableParameter

logger = logging.getLogger(__name__)
ureg = UnitRegistry()


@attr.s(slots=True)
class PositionEV(ObservableParameter):
    """
    Expectation value of the position operator $\\hat{{x}}$ as time-dependent observable
    """

    INPUT_STR_ID = "position"

    _x_operator = attr.ib(default=None, init=False)

    def obs_prepare(
        self, tgridsize, xgrid, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Prepare the observable array and calculate the position operator.
        """
        self._x_operator = np.meshgrid(*([xgrid] * self.n_m))
        self._array = np.zeros((self.n_s, self.n_m, tgridsize), dtype=np.float64)

        return self

    def obs_update(
        self, t, idx, wf, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Observable array is updated.
        """

        self._array[..., idx] = calc_position_ev_nonorm(
            wf, self._x_operator, self.dx, self.n_m
        )

        return None

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Observable array is added to the h5file given.
        """

        super().obs_to_outfile(h5file, "position")

        return None
