# -*- coding: utf-8 -*-

"""polarisation function spectrum observable for pyQD."""

import logging

import attr
import h5py
import numpy as np
from pint import UnitRegistry

from ..exceptions import PyQDPhysicsError
from ..utils.convert import _unit_converter
from ..utils.physics import calc_polarisation_ev, calc_polarisation_spectrum
from ._base import ObservableParameter

logger = logging.getLogger(__name__)
ureg = UnitRegistry()


@attr.s(slots=True)
class PolarisationSpectrum(ObservableParameter):
    """
    Calculate the polarisation function and the corresponding spectrum as an observable.
    """

    INPUT_STR_ID = "spectrum_polarisation"
    UNITS = {"dx": ("angstrom", "bohr"), "t_init": ("fs", "a_u_time")}

    tgrid = attr.ib(default=None, init=False)
    t_init = attr.ib(default=None, init=False)
    _mu_operator = attr.ib(default=None, init=False)
    _spectrum = attr.ib(default=None, init=False)
    _spectrum_abs = attr.ib(default=None, init=False)
    _wgrid = attr.ib(default=None, init=False)

    def obs_prepare(
        self, tgridsize, mu_input, t_init, tgrid, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Prepare the observable array and calculate the momentum operator.
        """
        self.t_init = _unit_converter(t_init, unit=self.UNITS["t_init"][-1])
        self.tgrid = tgrid

        self._mu_operator = np.zeros((self.n_s, self.n_s), dtype=np.float64)

        for pair in mu_input:
            (jlow, jup), mu = pair
            self._mu_operator[jlow, jup] = mu

        self._array = np.zeros((1, tgridsize), dtype=np.complex128)

        return self

    def obs_update(
        self, t, idx, wf, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Update the polarisation function array.
        """
        self._array[:, idx] = calc_polarisation_ev(
            wf, self._mu_operator, self.dx, self.n_m
        )

        return None

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Calculate the polarisation spectrum and store everything in the .h5 output file.
        """
        h5p = h5file.create_group("polarisation")

        try:
            (
                self._wgrid,
                self._spectrum,
                self._spectrum_abs,
            ) = calc_polarisation_spectrum(self._array[0, :], self.tgrid, self.t_init.m)
            h5p.create_dataset("freqgrid", data=self._wgrid)
            h5p.create_dataset("spectrum", data=self._spectrum)
            h5p.create_dataset("spectrum_abs", data=self._spectrum_abs)

        except PyQDPhysicsError as e:
            logger.error(
                "Something went wrong while calculating polarisation"
                " spectra. Sorry.\n\tError: message: {}".format(e)
            )

        h5p.create_dataset("timedata", data=self._array)

        return None
