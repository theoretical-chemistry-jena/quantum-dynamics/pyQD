# -*- coding: utf-8 -*-
"""Reduced density observable for pyQD."""

import logging

import attr
import h5py
import numpy as np
import opt_einsum as oe
from pint import UnitRegistry

from ._base import ObservableParameter

logger = logging.getLogger(__name__)
ureg = UnitRegistry()


@attr.s(slots=True)
class ReducedDensitySpectrum(ObservableParameter):
    """
    Calculate the polarisation function and the corresponding spectrum as an observable.
    """

    INPUT_STR_ID = "spectrum_reduced_density"
    UNITS = {"dx": ("angstrom", "bohr"), "t_init": ("fs", "a_u_time")}

    tgrid = attr.ib(default=None, init=False)
    _mu_operator = attr.ib(default=None, init=False)
    _spectrum = attr.ib(default=None, init=False)
    _spectrum_abs = attr.ib(default=None, init=False)
    _wgrid = attr.ib(default=None, init=False)
    _expr = attr.ib(default=None, init=False)

    def obs_prepare(
        self, tgridsize, mu_input, tgrid, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Prepare the observable array and calculate the momentum operator.
        """
        self.tgrid = tgrid
        self._mu_operator = np.zeros((self.n_s, self.n_s), dtype=np.float64)

        for pair in mu_input:
            (jlow, jup), mu = pair
            self._mu_operator[jlow, jup] = mu
            self._mu_operator[jup, jlow] = mu

        # logger.info("mu operator @ reduced density:\n{}".format(self._mu_operator))

        operator = "...i,...j->ij"
        wfdim = [self.n_g] * self.n_m + [self.n_s]
        self._expr = oe.contract_expression(operator, wfdim, wfdim, optimize="dp")
        self._array = np.zeros((self.n_s, self.n_s, tgridsize), dtype=np.complex128)

        return self

    def obs_update(
        self, t, idx, wf, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Update the polarisation function array.
        """
        self._array[..., idx] = (
            self._expr(wf, np.conjugate(wf), backend="numpy") * self.dx ** self.n_m
        )

        return None

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Calculate the polarisation spectrum and store everything in the .h5 output file.
        """
        operator = "ab,bci->aci"
        polarisation = np.trace(
            oe.contract(operator, self._mu_operator, self._array), axis1=0, axis2=1
        )

        spectrum = np.fft.rfft(np.real(polarisation))
        spectrum_abs = np.real(spectrum) ** 2 + np.imag(spectrum) ** 2
        wgrid = np.fft.rfftfreq(len(self.tgrid), d=(self.tgrid[1] - self.tgrid[0]))

        h5p = h5file.create_group("reduced-density")
        h5p.create_dataset("density", data=self._array)
        h5p.create_dataset("polarisation", data=polarisation)
        h5p.create_dataset("spectrum", data=spectrum)
        h5p.create_dataset("spectrum_abs", data=spectrum_abs)
        h5p.create_dataset("freqgrid", data=wgrid)

        return None
