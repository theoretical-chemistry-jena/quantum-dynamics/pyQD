# -*- coding: utf-8 -*-
"""Momentum expectation value observable for pyQD."""

import logging

import attr
import h5py
import numpy as np
from pint import UnitRegistry

from ..utils.physics import calc_k_grid, calc_momentum_ev
from ._base import ObservableParameter

logger = logging.getLogger(__name__)
ureg = UnitRegistry()


@attr.s(slots=True)
class MomentumEV(ObservableParameter):
    """
    Expectation value of the momentum operator $\\hat{{p}}$ as time-dependent observable
    """

    INPUT_STR_ID = "momentum"
    _p_operator = attr.ib(default=None, init=False)
    fftf = attr.ib(default=None, init=False)
    _dk = attr.ib(default=None, init=False)

    def obs_prepare(
        self, tgridsize, fft_f, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Prepare the observable array and calculate the momentum operator.
        """

        pgrid = calc_k_grid(self.n_g, self.dx)

        self._p_operator = np.meshgrid(*[pgrid] * self.n_m)
        self._dk = pgrid[1] - pgrid[0]
        self._array = np.zeros((self.n_s, self.n_m, tgridsize), dtype=np.float64)
        self.fftf = fft_f

        return self

    def obs_update(
        self, t, idx, wf, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Update the observable array.
        """

        self._array[..., idx] = calc_momentum_ev(
            wf, self._p_operator, self.fftf, self._dk, self.n_s, self.n_m
        )

        return None

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Observable array is added to the h5file given.
        """

        super().obs_to_outfile(h5file, "momentum")
