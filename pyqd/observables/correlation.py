# -*- coding: utf-8 -*-
"""CorrelationSpectrum observable for pyQD."""

import logging

import attr
import h5py
import numpy as np

from ..exceptions import PyQDInputError
from ..utils.convert import ureg
from ..utils.physics import calc_autocorrelation
from ._base import ObservableParameter

logger = logging.getLogger(__name__)


@attr.s(slots=True)
class CorrelationSpectrum(ObservableParameter):
    """
    Calculate the correlation spectrum based on the autocorrelation function.
    Requires a relaxed groundstate wavefunction as input.
    """

    INPUT_STR_ID = "spectrum_correlation"
    UNITS = {"dx": ("angstrom", "bohr"), "gs_energy": ("eV", "hartree")}

    gs_wf = attr.ib(default=None, init=False)
    gs_energy = attr.ib(default=None, init=False)
    tgrid = attr.ib(default=None, init=False)
    _spectrum = attr.ib(default=None, init=False)
    _wgrid = attr.ib(default=None, init=False)
    _wf_mask = attr.ib(default=None, init=False)

    def obs_prepare(
        self, tgridsize, itp_integrator, tgrid, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Prepare the observable array and the other parameters required.
        """

        self._array = np.zeros((self.n_s - 1, tgridsize), dtype=np.float64)
        r_init_state = itp_integrator.wfparam.rtp_init_states
        i_state = itp_integrator.wfparam.itp_relax_states

        if len(r_init_state) > 1:
            raise PyQDInputError(
                "With correlation spectra, only a single initial RTP state is allowed."
            )

        if len(i_state) > 1:
            logger.warning(
                "You calculated multiple ITP relaxed states. For correlation"
                "spectra, we need exactly one ground state wavefunction"
                "I'll go ahead and take the first state, nr. '{}', only".format(
                    i_state[0]
                )
            )
            i_state = [i_state[0]]

        init_state = itp_integrator.wfparam.rtp_init_states[0]

        iwf = itp_integrator.lower[..., i_state, 0]
        self.gs_wf = np.concatenate([iwf.copy() for _ in range(self.n_s - 1)], axis=-1)
        self.gs_energy = float(np.asarray(itp_integrator.energies)[0, 0])

        self._wf_mask = np.ones(self.n_s, dtype=bool)
        self._wf_mask[init_state] = 0

        self.tgrid = tgrid

        return self

    def obs_update(
        self, t, idx, wf, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Update the observable array.
        """
        self._array[:, idx] = calc_autocorrelation(
            wf[..., self._wf_mask],
            self.gs_wf,
            self.gs_energy,
            self.tgrid[idx],
            self.dx,
            self.n_m,
        )

        return None

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """
        Calculate the spectrum and the frequency grid, and store everything to the .h5 output file.
        """
        self._spectrum = np.abs(np.sum(np.fft.fft(self._array), axis=0)) ** 2
        unit_conv = ureg.Quantity(1, "1 / a_u_time").to("eV", "sp").m
        self._wgrid = (
            np.fft.fftfreq(self.tgrid.size, self.tgrid[1] - self.tgrid[0]) * unit_conv
        )

        h5p = h5file.create_group("correlation")
        h5p.create_dataset("timedata", data=self._array)
        h5p.create_dataset("freqgrid", data=self._wgrid)
        h5p.create_dataset("spectrum", data=self._spectrum)

        return None
