# -*- coding: utf-8 -*-


""" Nanoparticle interactor classes for pyQD """


import logging
from dataclasses import asdict
from functools import partial
from typing import List, Optional, Union

import attr
import h5py
import numpy as np
import pint

from ..parameters import TimeParameter
from ..parameters.defaults import Base
from ..utils.convert import _decay_converter, _pair_converter, _unit_converter
from ._base import InteractorParameter
from .defaults import Nanoparticle
from .field import FieldParameter

logger = logging.getLogger(__name__)

# pylint: disable=C0302


@attr.s(slots=True, order=True)
class NanoparticleParameter(InteractorParameter):
    """Interactor class for a QDIntegrator. Implements a modelled nanoparticle-system
    interaction by means of a phase-shifted asymmetric electric field derived from an
    user-supplied incident laser field (as a FieldParameter object).
    The args do not have to be supplied one-by-one. A populate_from_file() method is
    available to include data from a suitable pyQD input file.

    Args:
        // the super() args: pairs, n_g, n_m, n_s
        sigma_off (float): Relative width of decreasing flank the pulse
        amplitude_diag (float): Relative ampl. acting on the diagonal
        amplitude_od (float): Relative ampl. acting the off-diagonal. Does not include
            the transition dipole moment
        field (FieldParamter): The associated FieldParameter instance
        np_r0 (float): Initial distance from NP in bohr
        np_dr (float): Distance of monomers from each other in bohr
        np_decay (float): Field Decay constants in 1/bohr
        decay (str): Decay type: Either "gaussian" or "exponential"
        phase (float): Const. phase shift of the NP response, in fractions of Pi

    Returns:
        NanoparticleParameter: The NanoparticleParameter instance, usable in QDIntegrators.
    """

    DEFAULT_CLS = Nanoparticle
    INPUT_STR_ID = "perturbation"
    PRIORITY = 100
    TIME_DEPENDENT = True
    UNITS = {
        "detuning": ("eV", "1 / a_u_time"),
        "np_r0": ("angstrom", "bohr"),
        "np_dr": ("angstrom", "bohr"),
        "np_decay": ("1 / angstrom", "1 / bohr"),
        "phase": ("degree", "radian"),
        "phase_shift": ("degree", "radian"),
    }

    sigma_off: float = attr.ib()
    amplitude_diag: float = attr.ib()
    amplitude_od: float = attr.ib()

    np_r0: Union[float, pint.Quantity, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["np_r0"][-1])
    )
    np_dr: Union[float, pint.Quantity, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["np_dr"][-1])
    )
    np_decay: Union[float, pint.Quantity, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["np_decay"][-1])
    )

    field: Union[FieldParameter, dict] = attr.ib(
        validator=attr.validators.instance_of((FieldParameter, dict)),
    )

    time: Union[TimeParameter, np.ndarray, list] = attr.ib(
        validator=attr.validators.instance_of((TimeParameter, list))
    )

    decay: str = attr.ib(
        validator=attr.validators.in_(["exponential", "gaussian", 1, 2]),
        converter=_decay_converter,
    )  # Decay type: "gaussian" or "exponential"

    phase: Union[str, float, pint.Quantity] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["phase"][-1])
    )

    included_diag: List[int] = attr.ib(
        converter=partial(_pair_converter, include_self=True, indexed_start_zero=True),
        factory=list,
    )

    detuning: Union[str, float, pint.Quantity] = attr.ib(
        default=0.0, converter=partial(_unit_converter, unit=UNITS["detuning"][-1])
    )

    phase_shift: Union[str, float, pint.Quantity] = attr.ib(
        default=0.0, converter=partial(_unit_converter, unit=UNITS["phase_shift"][-1])
    )

    include_shg: bool = attr.ib(default=False)
    shg_amplitude: float = attr.ib(default=1.0)

    _amplitudes = attr.ib(default=None, init=False)

    def __attrs_post_init__(self):
        if isinstance(self.field, dict):
            self.field = FieldParameter(
                n_s=self.n_s, n_g=self.n_g, n_m=self.n_m, time=self.time, **self.field
            )

        # self.included_diag = _pair_converter(self.included_diag, include_self=True)
        self._num, self.pairs = len(self.included_diag), self.included_diag

        # Now, the off-diagonal elements are appended
        self.align_idxs()

    @classmethod
    def from_defaults(
        cls,
        base: Base,
        field: FieldParameter,
        **kwargs,
    ):  # pylint: disable=arguments-differ
        """
        Method to populate the class from the corresponding dataclass.
        """

        basekw = asdict(base)
        inputs = asdict(cls.DEFAULT_CLS(**kwargs))

        return cls(**basekw, **inputs, field=field)

    @classmethod
    def from_file(
        cls,
        file: str,
        idx=0,
        time: Optional[TimeParameter] = None,
        **kwargs,
    ):  # pylint: disable=arguments-differ

        if time is None:
            time = TimeParameter.from_file(file)

        return super().from_file(file, idx=idx, time=time)

    def obs_prepare(
        self, tgridsize: int, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Initialize the observable array."""
        self._array = np.zeros((self._num, tgridsize))

        return self

    def obs_update(
        self, t: float, idx: int, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Update the observable array."""
        ptb = [self._ptb(t, i) for i in range(self._num)]
        self._array[:, idx] = ptb

        return None

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Output the observable array to the provided h5 file."""
        super().obs_to_outfile(h5file, "nanoparticle")

        return None

    def align_idxs(self):
        """Align indices between the field subclass and the 'own' indices."""

        # ? How should this scale? Via +1 or -1? It's -µ*E
        self._amplitudes = [-1.0 * self.amplitude_diag] * self._num

        # Add the field
        self._num += self.field.num
        self.pairs += self.field.pairs

        # Update the amplitudes to incorporate the off-diagonals.
        # ! This should now bring both fields "in line".
        self._amplitudes += [-mu * self.amplitude_od for _, mu in self.field.mu]

    def _strength(
        self,
        t: Union[float, np.ndarray],
        rel_ampl: float,
        harmonic: int = 1,
        phaseshift: float = 0.0,
    ):
        return np.where(
            t < self.field.t_central.m,
            # Use a gaussian type field envelope
            rel_ampl
            * self.field.field(
                t,
                phase=(self.phase.m + phaseshift),
                harmonic=harmonic,
                detuning=self.detuning.m,
            ),
            # Use either a gaussian or exponential type field envelope
            rel_ampl
            * self.field.field(
                t,
                expon=self.decay,
                sigma=self.sigma_off,
                phase=(self.phase.m + phaseshift),
                harmonic=harmonic,
                detuning=self.detuning.m,
            ),
        )

    def _ptb(self, t: Union[float, np.ndarray], idx: int):
        _, jup = self.pairs[idx]

        # Distance-dependent phase shift. Currently linearly dependent on the
        # monomer index.
        phaseshift = self.phase_shift.m * (jup - 1)

        # Calculate the raw strength
        ptb = self._strength(t, self._amplitudes[idx], phaseshift=phaseshift)

        # Calculate the SHG portion
        if self.include_shg:
            ptb += self.shg_amplitude * self._strength(
                t, self._amplitudes[idx], harmonic=2, phaseshift=phaseshift
            )

        # Weigh the strength with the distance from the NP
        ptb *= np.exp(
            -self.np_decay.m  # pylint: disable=invalid-unary-operand-type)
            * (self.np_r0.m + self.np_dr.m * (jup - 1))
        )

        return ptb

    def _matrix(
        self, t: float, dtt: float, idx: int, print_e: bool = False, energy_only=False
    ):

        mat = np.eye(self.n_s, dtype="complex128")

        jlow, jup = self.pairs[idx]

        ptb = self._ptb(t, idx)

        # Short circuit the function
        if energy_only:
            return None, ptb

        if jlow == jup:
            mat[jlow, jlow] = np.exp(-1.0j * (dtt / self.splits[idx]) * ptb)

        else:
            mat[jlow, jlow] = mat[jup, jup] = np.cos((dtt / self.splits[idx]) * ptb)
            mat[jup, jlow] = mat[jlow, jup] = -1j * np.sin(
                ptb * dtt * (1.0 / self.splits[idx])
            )

        if print_e:
            return mat, ptb
        else:
            return mat
