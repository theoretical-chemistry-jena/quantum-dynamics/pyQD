# -*- coding: utf-8 -*-

""" Time-independent static interactor classes for pyQD """

import logging
from dataclasses import asdict
from functools import partial
from pathlib import Path
from typing import List, Union

import attr
import h5py
import numpy as np
import pint

from ..exceptions import PyQDInputError
from ..parameters.defaults import Base
from ..utils.convert import _list_converter, _pair_converter, _unit_converter
from ._base import InteractorParameter
from .defaults import Static

logger = logging.getLogger(__name__)

# pylint: disable=C0302


@attr.s(slots=True, order=True)
class DummyInteractor(InteractorParameter):
    """
    Pseudo time-dependent to be added in case of a minimal setup.
    """

    INPUT_STR_ID = "dummy"
    UNITS = {}
    PRIORITY = 0
    TIME_DEPENDENT = True

    _num = attr.ib(default=1)
    splits = attr.ib(factory=lambda: [1])
    pairs = attr.ib(factory=lambda: [(0, 0)], converter=_pair_converter)

    @classmethod
    def from_defaults(cls, base: Base, **kwargs):  # pylint: disable=arguments-differ
        """Populate the dummy interactor from defaults."""

        base_kw = asdict(base)
        return cls(**base_kw, **kwargs)

    def obs_prepare(
        self, tgridsize, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Prepare the observable capabilities. Nothing to do here."""

        # Although this might not make too much sense,
        # it's required for time-dependent interactors.
        self._array = np.zeros((1, tgridsize))

        return self

    def obs_update(
        self, t, idx, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Update the observable in each time step. Nothing to do here."""

        return None

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Output the observable information to the file. Nothing to do here."""
        return None

    def _matrix(
        self, t: float, dtt: float, idx: int, print_e: bool = False, energy_only=False
    ):

        if energy_only:
            return None, 0.0

        mat = np.eye(self.n_s, dtype=np.complex128)

        if print_e:
            return mat, 0.0

        else:
            return mat


@attr.s(slots=True, order=True, hash=True)
class StaticInteractor(InteractorParameter):
    """
    Time-independent propagator to implement a static shift in energy.
    """

    INPUT_STR_ID = "static_energy_shift"
    UNITS = {"delta": ("eV", "hartree")}
    PRIORITY = 1
    TIME_DEPENDENT = False
    DEFAULT_CLS = Static

    delta: Union[str, pint.Quantity] = attr.ib(
        converter=[_list_converter, partial(_unit_converter, unit=UNITS["delta"][-1])]
    )
    state: List[int] = attr.ib(converter=partial(_pair_converter, include_self=True))

    def __attrs_post_init__(self):
        self._num, self.pairs = len(self.state), self.state

        if not self._num == len(self.delta):
            raise PyQDInputError(
                "Incorrect number of deltas given. {} != {}".format(
                    self._num, len(self.delta)
                )
            )

    @classmethod
    def from_defaults(cls, base: Base, **kwargs):  # pylint: disable=arguments-differ
        """Populate the class from defaults."""
        basekw = asdict(base)
        inputs = asdict(cls.DEFAULT_CLS(**kwargs))
        return cls(**basekw, **inputs)

    @classmethod
    def from_file(
        cls, file: Union[Path, str], idx=0
    ):  # pylint: disable=arguments-differ
        """Populate the class from a file."""
        return super().from_file(file, idx=idx)

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Output the observable information to the file. Nothing to do here."""
        return None

    def _matrix(
        self, t: float, dtt: float, idx: int, print_e: bool = False, energy_only=False
    ):

        static_shift = self.delta[idx].m

        # Short ciruit
        if energy_only:
            return None, static_shift

        curr_split = self.splits[idx]
        jlow, _ = self.pairs[idx]

        mat = np.eye(self.n_s, dtype=np.complex128)
        mat[jlow, jlow] = np.exp(-1.0j * static_shift * (dtt / curr_split))

        if print_e:
            return mat, static_shift

        else:
            return mat
