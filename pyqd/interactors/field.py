# -*- coding: utf-8 -*-

""" Electric field interactor class for pyQD """

import logging
from dataclasses import asdict
from functools import partial
from pathlib import Path
from typing import List, Optional, Tuple, Union

import attr
import h5py
import numpy as np
import pint

from ..exceptions import PyQDInputError
from ..parameters import AnalyticalSystemParameter, TimeParameter
from ..parameters.defaults import Base
from ..utils.convert import _mu_converter, _unit_converter
from ..utils.physics import find_nearest_timepoint, gen_e_field
from ._base import InteractorParameter
from .defaults import Field

logger = logging.getLogger(__name__)

# pylint: disable=C0302


@attr.s(slots=True, order=True)
class FieldParameter(InteractorParameter):
    """Parameter object to hold relevant information on a single fields.
    Subclass of the PyQDParameter object. Completeness requires the
    following inputs, provided as kwargs or in an input file using the
    populate_from_file() method:

    Args:
        // the super() args: pairs, n_g, n_m, n_s
        e_0 (float): Maximal amplitude of the electric field.
        omega (float): Central frequency of the electric field.
        t_central (float): Time at which the electric field is at its maximum.
        sigma (float): Gaussian sigma (standard deviation) of the envolope function.
        mu (List[Tuple[Tuple[int, int], float]]): Information on the transition dipole
            moments between individual states. If no transition is given,
            it's not considered. Can be provided by the SystemParameter class.

    Returns:
        FieldParameter: The corresponding field parameter object.
    """

    DEFAULT_CLS = Field
    INPUT_STR_ID = "fields"
    PRIORITY = 80
    TIME_DEPENDENT = True
    UNITS = {
        "e_0": ("volt / meter", "a_u_electric_field"),
        "omega": ("eV", "1 / a_u_time"),
        "t_central": ("fs", "a_u_time"),
        "sigma": ("fs", "a_u_time"),
    }

    e_0: Union[str, pint.Quantity] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["e_0"][-1])
    )
    omega: Union[str, pint.Quantity] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["omega"][-1])
    )
    t_central: Union[str, pint.Quantity] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["t_central"][-1])
    )
    sigma: Union[str, pint.Quantity] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["sigma"][-1])
    )

    mu: Union[List[Tuple[Tuple[int, int], float]]] = attr.ib(
        validator=attr.validators.instance_of((list, tuple)), converter=_mu_converter
    )

    time: Union[TimeParameter, np.ndarray, list] = attr.ib(
        validator=attr.validators.instance_of((TimeParameter, list))
    )

    _obs_mu: np.ndarray = attr.ib(default=None, init=False)
    idx: int = attr.ib(default=0)

    def __attrs_post_init__(self):

        if self._num is None or self.pairs is None:
            # logger.warning("The mu value: {}".format(self.mu))
            self._num, self.pairs = len(self.mu), list(zip(*self.mu))[0]

        # Get time information
        if isinstance(self.time, TimeParameter):
            t = self.time.tgrid

        elif isinstance(self.time, (list, np.ndarray)):
            t = self.time

        else:
            raise PyQDInputError("Did not understand the tgrid you provided.")

        # ? Can we savely that the time grid units and the t_central units are
        # ? identical?
        t_unit = self.t_central.u
        if not np.any(t == self.t_central.m):
            tc_new = pint.Quantity(find_nearest_timepoint(self.t_central.m, t), t_unit)
            logger.info(
                "Field: central time not on time grid, "
                "readjusted from {} to {}".format(self.t_central, tc_new)
            )
            self.t_central = tc_new

    @classmethod
    def from_defaults(
        cls,
        base: Base,
        mu: Union[AnalyticalSystemParameter, tuple, list],
        time: Union[TimeParameter, np.ndarray],
        **kwargs,
    ):  # pylint: disable=arguments-differ
        """
        Method to populate the class from the corresponding dataclass.
        """

        # If the provided mu is a SystemParameter class, then extract the
        # transition dipole right away.
        if isinstance(mu, AnalyticalSystemParameter):
            mu = mu.mu

        basekw = asdict(base)
        inputs = asdict(cls.DEFAULT_CLS(**kwargs))

        return cls(**basekw, **inputs, mu=mu, time=time)

    @classmethod
    def from_file(
        cls,
        file: Union[Path, str],
        time: Optional[TimeParameter] = None,
        idx=0,
    ):  # pylint: disable=arguments-differ

        if time is None:
            time = TimeParameter.from_file(file)

        return super().from_file(file, idx=idx, time=time)

    def obs_prepare(
        self, tgridsize, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Initialize the observable array."""
        self._obs_mu = np.asarray(list(zip(*self.mu))[-1])
        self._array = np.zeros((len(self._obs_mu), tgridsize))

        return self

    def obs_update(
        self, t, idx, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Update the observable array."""
        self._array[:, idx] = np.negative(self._obs_mu * self.field(t))

        return None

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Output the observable array to the h5 file."""

        self._obs_mu = self._obs_mu[:, np.newaxis]
        raw_arr = np.negative(
            np.divide(self._array, self._obs_mu, where=self._obs_mu != 0.0)
        )[:1, :]

        h5p = h5file.create_group(f"field_{self.idx}")
        h5p.create_dataset("mus", data=self._obs_mu)
        h5p.create_dataset("field", data=self._array)
        h5p.create_dataset("field_raw", data=raw_arr)

        return None

    def field(
        self,
        t: Union[float, np.ndarray],
        expon: int = 2,
        sigma: float = 1.0,
        phase: float = 0.0,
        harmonic: int = 1,
        detuning: float = 0.0,
    ):
        """Return the field at a point in time t (or vectorised using a numpy.ndarray)"""

        return gen_e_field(
            t,
            self.e_0.m,
            (self.omega.m - detuning) * harmonic,
            self.sigma.m * sigma,
            self.t_central.m,
            shift=phase,
            expon=expon,
        )

    def _matrix(
        self, t: float, dtt: float, idx: int, print_e: bool = False, energy_only=False
    ):

        mat = np.eye(self.n_s, dtype="complex128")

        (jlow, jup), mu = self.mu[idx]
        f = -mu * self.field(t)

        # Short ciruit
        if energy_only:
            return None, f

        mat[jlow, jlow] = mat[jup, jup] = np.cos((dtt / self.splits[idx]) * f)
        mat[jup, jlow] = mat[jlow, jup] = -1j * np.sin(
            f * dtt * (1.0 / self.splits[idx])
        )

        if print_e:
            return mat, f
        else:
            return mat
