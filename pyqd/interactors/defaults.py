# -*- coding: utf-8 -*-
"""
Default values for pyQD simulations, packaged as dataclasses.
At the moment, you're advised to use the defaults, modify the
parameters to your liking and then, using `dataclasses.asdict(p)`,
get the data as dict.
"""

# py lint: disable=invalid-name

import typing
from dataclasses import dataclass, field
from typing import Any, List, Union

import pint


@dataclass
class Static:
    """Static energy shift defaults"""

    state: Union[List[int], int]
    delta: Union[pint.Quantity, float, str] = "0.1 eV"


@dataclass
class Damping:
    """Damping interactor defaults"""

    state: Union[List[int], int]
    tau: Union[pint.Quantity, float, str] = "215 fs"
    gamma: Union[pint.Quantity, float, str] = None
    adiabatic: bool = False
    adiabatic_interactors: typing.Optional[List[Union[Any]]] = None


@dataclass
class JCoupling:
    """JCoupling defaults"""

    j_coupling: Union[pint.Quantity, float, str] = "0.1 eV"
    pairs: List[str] = field(default_factory=lambda: ["1, 2"])


@dataclass
class Field:
    """Field defaults"""

    sigma: Union[pint.Quantity, float, str] = "150 fs"
    e_0: Union[pint.Quantity, float, str] = "5e8 V/m"
    t_central: Union[pint.Quantity, float, str] = "0.0  fs"
    omega: Union[pint.Quantity, float, str] = "2.3 eV"


@dataclass
class Nanoparticle:
    """Nanoparticle defaults"""

    phase: Union[pint.Quantity, float, str] = "0.0 * pi rad"
    phase_shift: Union[pint.Quantity, float, str] = "0.0 * pi rad"
    amplitude_diag: float = 3.0
    amplitude_od: float = 3.0
    included_diag: List[int] = field(default_factory=lambda: [])
    sigma_off: float = 2.0
    decay: str = "exponential"
    detuning: Union[pint.Quantity, float, str] = 0.0
    np_r0: Union[pint.Quantity, float, str] = "0 angstrom"
    np_dr: Union[pint.Quantity, float, str] = "4 angstrom"
    np_decay: Union[pint.Quantity, float, str] = "0.3 1/angstrom"


@dataclass
class Legacy:
    """Default class for the LegacyPerturbationParameter."""

    state: int
    time_on: Union[pint.Quantity, float, str] = "0 fs"
    time_off: Union[pint.Quantity, float, str] = "1000"
    sigma_on: Union[pint.Quantity, float, str] = "200 fs"
    sigma_off: Union[pint.Quantity, float, str] = "200 fs"
    amplitude: Union[pint.Quantity, float, str] = "0.1 eV"
