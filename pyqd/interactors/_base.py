# -*- coding: utf-8 -*-
""" Interactor base class for pyQD """

import logging
from typing import List, Tuple, Union

import attr
import numpy as np

from ..exceptions import PyQDInputError
from ..parameters._base import PyQDParameter

logger = logging.getLogger(__name__)

# pylint: disable=C0302


@attr.s(slots=True, order=True)
class InteractorParameter(PyQDParameter):
    """Super class for the QD interactors. An interactor is a component of the
    potential energy Hamiltonian V. This class implements basic behavior that is
    necessary for the QD calculation. For custom implementation, a class needs the
    following methods:

    ._matrix(self, dtt, idx, print_e=False): Provides the propagator pair matrix
        exp(-i * I_n * (dt / s)) with I_n the energy matrix of the interaction.
    .obs_prepare(self, tgridsize, **kwargs): Prepares the "observable" output, such as
        the _array used for the time-domain output of the interaction energy.
    .obs_update(self, t, idx, **kwargs): Updates the "observable" _array at each time step.
    .obs_to_outfile(self, h5file, name):

    It furthermore requires the following class attributes:

    PRIORITY (int): Numeric value used for ordering of the different interactors. Usually,
        low-demanding interactors should get a low priority.
    TIME_DEPENDENT (bool): Is the interaction time-dependent? Used to determine whether
        the propagator has to be recalculated in each time step.
    UNITS (dict): If the interactor should be able to use the _populate_from_file() method
        implemented by PyQDParameter , the interactor needs a mapping from input file keys
        to pint units.

    IMPORTANT: The PyQDParameter objects all use the Python feature __slots__ that requires
        the user to think about the necessary class variables beforehand. The rest will
        be picked up by the self.is_complete check function and have to be set prior to
        use in a integrator. Those slots prepended with an underscore '_' character
        are considered non-input slots and are hence not required for a successful check.
    """

    PRIORITY = 0
    TIME_DEPENDENT = True
    UNITS = {}

    pairs: List[Union[str, Tuple[int, int], List[int]]] = attr.ib(
        default=None, kw_only=True
    )
    splits: List[int] = attr.ib(default=None, kw_only=True)

    _num: int = attr.ib(default=None, init=False)
    _array: np.ndarray = attr.ib(default=None, init=False)

    @property
    def num(self):
        """Return the number of interactions"""
        return self._num

    @property
    def data(self):
        """Return the storage array for easier plotting"""
        return self._array

    # Observable interface
    def obs_prepare(self, **kwargs):  # pylint: disable=unused-argument
        """
        Dummy method for the observables preparation functionality.
        Subclass arguments may differ due to different requirements.
        """
        return self

    def obs_update(self, t, idx, **kwargs):  # pylint: disable=unused-argument
        """
        Dummy method for the observables updating functionality.
        Subclass arguments may differ due to different requirements.
        """
        return None

    def obs_to_outfile(self, h5file, name):
        """
        Dummy method for the observables outputting functionality.
        Subclass arguments differ due to the names being parsed here.
        """
        h5file.create_dataset(name, data=self._array)

    # Interactor / propagator interface
    def update_splits(self, splits: List[int]):
        """Update the splits of the instance based on the list of splits provided.
        Checks whether the correct number of splits is given.

        Args:
            splits (List[int]): Splits to be used for generating the propgators.

        """
        if len(splits) == self._num:
            self.splits = splits

        else:
            raise PyQDInputError(
                f"The splits list does not match the number of pairs. "
                f"'{len(splits)}' != '{self._num}'."
            )

    def _matrix(
        self,
        t: float,
        dtt: float,
        idx: int,
        print_e: bool = False,
        energy_only: bool = False,
    ):  # pylint: disable=unused-argument

        if print_e:
            return None, None

        return None

    def all_matrices(self, t, dtt, splits=None, print_e=None, energy_only=False):
        """Unified method to return all the propagator matrices based on the ._matrix method
        implemented by each interactor subclass.
        """

        # Is the splits method provided?
        if (not energy_only) and (splits is not None and splits != self.splits):
            self.update_splits(splits)

        # Check the req. output format
        if print_e is not None:
            if print_e in ["matrix", "list"]:
                printv = print_e
            else:
                printv = False
        else:
            printv = False

        # Predefine "res" to avoid possible outbounded messages.
        res = None

        # Is the printv argument given?
        if printv == "matrix":
            res = np.zeros((self.n_s, self.n_s), dtype=np.float64)
        elif printv == "list":
            res = []

        elif not printv:
            pass

        else:
            raise KeyError(
                "This should never be called. I did not understand. "
                "Please specify either 'matrix' or 'list'"
            )

        matrices = []

        for idx in range(self._num):
            mat, energy = self._matrix(
                t, dtt, idx, print_e=True, energy_only=energy_only
            )

            matrices.append(mat)

            if printv == "list":
                res.append(energy)

            elif printv == "matrix":
                jlow, jup = self.pairs[idx]

                if jlow == jup:
                    res[jlow, jup] += energy
                else:
                    res[jlow, jup] += energy
                    res[jup, jlow] += energy

        if energy_only:
            return res

        elif printv:
            return matrices, res

        else:
            return matrices
