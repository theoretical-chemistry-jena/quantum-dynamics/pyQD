# -*- coding: utf-8 -*-

""" Damping interactor class for pyQD """

import logging
from dataclasses import asdict
from functools import partial
from pathlib import Path
from typing import Dict, List, Optional, Union

import attr
import h5py
import numpy as np
import opt_einsum as oe
import pint

from ..exceptions import PyQDInputError
from ..parameters import AnalyticalSystemParameter
from ..parameters.defaults import Base
from ..utils.convert import _list_converter, _pair_converter, _unit_converter
from ._base import InteractorParameter
from .defaults import Damping
from .jcoupling import JCouplingParameter
from .static import StaticInteractor

logger = logging.getLogger(__name__)

# pylint: disable=C0302


@attr.s(slots=True, order=True, hash=True)
class DampingInteractor(InteractorParameter):
    """
    Time-independent propagator to implement a damping (depopulation of a state).
    Accepts lifetimes via the 'tau' parameter and decay rates via 'gamma'.
    Gamma takes precedence over tau. Defaults to using a value of tau.

    This propagator is NOT conserving the system norm so be careful about using
    it and interpreting the results!
    """

    INPUT_STR_ID = "gamma_tau_damping"
    UNITS = {
        "gamma": ("1 / fs", "1 / a_u_time"),
        "tau": ("fs", "a_u_time"),
    }
    PRIORITY = 101
    TIME_DEPENDENT = False
    DEFAULT_CLS = Damping
    ELIGIBLE_AD_INTERACTORS = {
        JCouplingParameter.INPUT_STR_ID: JCouplingParameter,
        StaticInteractor.INPUT_STR_ID: StaticInteractor,
    }

    tau: Union[List[str], str, pint.Quantity] = attr.ib(
        converter=[_list_converter, partial(_unit_converter, unit=UNITS["tau"][-1])]
    )
    state: List[int] = attr.ib(converter=partial(_pair_converter, include_self=True))
    gamma: Optional[Union[List[str], str, pint.Quantity]] = attr.ib(
        default=None,
        kw_only=True,
    )

    adiabatic: bool = attr.ib(default=False)
    system: Optional[AnalyticalSystemParameter] = attr.ib(default=None, kw_only=True)
    adiabatic_interactors: Optional[List[Union[Dict, InteractorParameter]]] = attr.ib(
        default=None, kw_only=True
    )

    # Internal switch
    use_gamma: bool = attr.ib(default=False, init=False)
    _adiabatic_interactors: Optional[List[InteractorParameter]] = attr.ib(
        default=None, init=False
    )

    def __attrs_post_init__(self):

        if self.gamma is not None:
            self.use_gamma = True

            # Do the converting afterwards.
            self.gamma = _list_converter(self.gamma)
            self.gamma = _unit_converter(self.gamma, unit=self.UNITS["gamma"][-1])

        if self.adiabatic and self.system is None:
            raise PyQDInputError(
                "If you want adiabatic damping, you need to specify a"
                "AnalyticalSystemParameter instance."
            )
        elif (
            self.adiabatic
            and self.system is not None
            and self.adiabatic_interactors is None
        ):
            raise PyQDInputError(
                "If you want adiabatic damping, you need to specify a"
                "AnalyticalSystemParameter instance as well as at least one adiabatic interactor."
            )
        else:
            pass

        if self.adiabatic and self.adiabatic_interactors is not None:
            # Now do the generation/adding of interactors
            list_of_interactors = []
            for interactor in self.adiabatic_interactors:
                actor = self._instantiate_from_dict_or_instance(interactor)
                list_of_interactors.append(actor)

            self._adiabatic_interactors = list_of_interactors

        # self._num is set to 1 as we can have the whole interaction in one matrix without hassle
        self._num, self.pairs = 1, self.state

    def _instantiate_from_dict_or_instance(self, interactor):
        if isinstance(interactor, dict) and len(interactor) == 1:
            key = list(interactor.keys())[0]
            try:
                actor = self.ELIGIBLE_AD_INTERACTORS[key](
                    **self.basekwargs, **interactor[key]
                )
            except KeyError as e:
                raise PyQDInputError(
                    "The interactor you passed to the Damping is not"
                    "eligible for the adiabatisation."
                ) from e
        elif (
            isinstance(interactor, InteractorParameter)
            and interactor.INPUT_STR_ID in self.ELIGIBLE_AD_INTERACTORS
        ):
            actor = interactor

        return actor

    @classmethod
    def from_defaults(
        cls, base: Base, system: AnalyticalSystemParameter, **kwargs
    ):  # pylint: disable=arguments-differ
        """Populate the class from defaults."""
        basekw = asdict(base)
        inputs = asdict(cls.DEFAULT_CLS(**kwargs))
        return cls(**basekw, **inputs, system=system)

    @classmethod
    def from_file(
        cls,
        file: Union[Path, str],
        system: Optional[AnalyticalSystemParameter] = None,
        idx=0,
    ):  # pylint: disable=arguments-differ
        """Populate the class from a file."""

        if system is None:
            system = AnalyticalSystemParameter.from_file(file)

        return super().from_file(file, idx=idx, system=system)

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Output the observable information to the file. Nothing to do here."""
        return None

    def _matrix(
        self, t: float, dtt: float, idx: int, print_e: bool = False, energy_only=False
    ):

        # Short ciruit. Damping does not correspond to a real-valued energy,
        # therefore returns 0.
        if energy_only:
            return None, 0.0

        mat = np.eye(self.n_s, dtype=np.complex128)
        curr_split = self.splits[idx]

        for i, pair in enumerate(self.pairs):
            if self.use_gamma:
                damping_freq = self.gamma[i].m
            else:
                damping_freq = 1 / self.tau[i].m

            # Convert the damping to a imaginary value.
            damping_freq *= -1.0j

            jlow, _ = pair
            mat[jlow, jlow] = np.exp(-1.0j * damping_freq * (dtt / curr_split))

        if self.adiabatic:
            _, (umat, umat1, __) = self.system.gen_pes(
                ext_adiab_interactors=self._adiabatic_interactors
            )
            opn = "...ij,jk,...kl->...il"
            mat = oe.contract(opn, umat1, mat, umat)

        if print_e:
            return mat, 0.0
        else:
            return mat
