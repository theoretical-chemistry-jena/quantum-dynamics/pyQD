# -*- coding: utf-8 -*-

""" Time-dependent legacy perturbation interactor class for pyQD """


import logging
from dataclasses import asdict
from functools import partial
from typing import Union

import attr
import h5py
import numpy as np
import pint

from ..exceptions import PyQDPhysicsError
from ..parameters.defaults import Base
from ..utils.convert import _pair_converter, _unit_converter
from ..utils.physics import gen_ptb_v0
from ._base import InteractorParameter
from .defaults import Legacy

logger = logging.getLogger(__name__)

# pylint: disable=C0302


@attr.s(slots=True, order=True)
class LegacyPerturbationParameter(InteractorParameter):
    """This class implements the time-dependent 'legacy' perturbation as discussed in
    Wehner et al., CPL, 2012, 49-53, DOI: 10.1016/j.cplett.2012.05.034. The profile is
    composed of a gaussian increase to the amplitude, a plateau region in the interval
    [t_on, t_off] and a gaussian decrease to 0 from there.

    Args:
        // the super() args: n_g, n_m, n_s
        state (int): Which state is perturbed?
        time_on (float): Start time of the constant region.
        time_off (float): End time of the constant region.
        sigma_on (float): Gaussian sigma of the rising flank.
        sigma_off (float): Gaussian sigma of the decreasing flank.
        amplitude (float): Energy shift in Hartree.

    Returns:
        LegacyPerturbationParameter: The interactor instance for use in the QDIntegrators.
    """

    INPUT_STR_ID = "legacy_perturbation"
    DEFAULT_CLS = Legacy
    PRIORITY = 90
    TIME_DEPENDENT = True
    UNITS = {
        "time_on": ("fs", "a_u_time"),
        "time_off": ("fs", "a_u_time"),
        "sigma_on": ("fs", "a_u_time"),
        "sigma_off": ("fs", "a_u_time"),
        "amplitude": ("eV", "hartree"),
    }

    state: int = attr.ib(converter=partial(_pair_converter, include_self=True))
    time_on: Union[pint.Quantity, float, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["time_on"][-1])
    )
    time_off: Union[pint.Quantity, float, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["time_off"][-1])
    )
    sigma_on: Union[pint.Quantity, float, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["sigma_on"][-1])
    )
    sigma_off: Union[pint.Quantity, float, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["sigma_off"][-1])
    )
    amplitude: Union[pint.Quantity, float, str] = attr.ib(
        converter=partial(_unit_converter, unit=UNITS["amplitude"][-1])
    )

    def __attrs_post_init__(self):
        self._num, self.pairs = len(self.state), self.state

    @classmethod
    def from_defaults(cls, base: Base, **kwargs):  # pylint: disable=arguments-differ
        """
        Populate the class from the defaults.
        Defaults can easily be overridden.
        """
        base_kw = asdict(base)
        default_kw = asdict(cls.DEFAULT_CLS(**kwargs))

        return cls(**base_kw, **default_kw)

    @classmethod
    def from_file(cls, file: str, idx=0):  # pylint: disable=arguments-differ
        return super().from_file(file, idx=idx)

    def obs_prepare(
        self, tgridsize, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Prepare the observable array."""
        self._array = np.zeros((1, tgridsize))

        return self

    def obs_update(
        self, t, idx, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Update the observable array."""
        ptb = [self._ptb(t) for i in range(self._num)]
        self._array[:, idx] = ptb

        return None

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Output the observable to a h5py file."""
        super().obs_to_outfile(h5file, "legacy")

        return None

    def _ptb(self, t):
        return gen_ptb_v0(
            t,
            self.sigma_on.m,
            self.sigma_off.m,
            self.time_on.m,
            self.time_off.m,
            self.amplitude.m,
        )

    def _matrix(
        self, t: float, dtt: float, idx: int, print_e: bool = False, energy_only=False
    ):

        mat = np.eye(self.n_s, dtype="complex128")

        jlow, jup = self.pairs[idx]
        ptb = float(self._ptb(np.array(t, ndmin=1)))

        if energy_only:
            return None, ptb

        if jlow == jup:
            mat[jlow, jlow] = np.exp(-1.0j * (dtt / self.splits[idx]) * ptb)
        else:
            raise PyQDPhysicsError(
                "Legacy perturbation is only allowed for diagonal interaction."
            )

        # m = np.expand_dims(m, axis=list(range(self.n_m)))

        if print_e:
            return mat, ptb
        else:
            return mat
