# -*- coding: utf-8 -*-

""" Time-independent J coupling interactor class for pyQD """

import logging
from dataclasses import asdict
from functools import partial
from pathlib import Path
from typing import List, Tuple, Union

import attr
import h5py
import numpy as np

from ..parameters.defaults import Base
from ..utils.convert import _list_converter, _pair_converter, _unit_converter
from ._base import InteractorParameter
from .defaults import JCoupling

logger = logging.getLogger(__name__)

# pylint: disable=C0302


@attr.s(slots=True, order=True, hash=True)
class JCouplingParameter(InteractorParameter):
    """Simple time-independent interactor that provides electronic coupling between
    states. Can also be used as a template interactor.

    Args:
        // the super() args: pairs, n_g, n_m, n_s
        j_coupling: Union[float, List[float]]: The electronic coupling elements
        for the pairings.

    Returns:
        The JCouplingParameter instance, usable for .
    """

    DEFAULT_CLS = JCoupling
    INPUT_STR_ID = "jcouplings"
    UNITS = {"j_coupling": ("eV", "hartree")}
    PRIORITY = 10
    TIME_DEPENDENT = False

    j_coupling = attr.ib(
        converter=[
            _list_converter,
            partial(_unit_converter, unit=UNITS["j_coupling"][-1]),
        ]
    )

    pairs: List[Union[str, Tuple[int, int], List[int]]] = attr.ib(
        converter=_pair_converter
    )

    def __attrs_post_init__(self):
        self._num = len(self.pairs)
        if len(self.j_coupling) == 1:
            self.j_coupling *= self._num

    @classmethod
    def from_defaults(cls, base: Base, **kwargs):  # pylint: disable=arguments-differ
        """
        Method to populate the class from the corresponding dataclass.
        """
        basekw = asdict(base)
        inputs = asdict(cls.DEFAULT_CLS(**kwargs))
        return cls(**basekw, **inputs)

    @classmethod
    def from_file(
        cls, file: Union[Path, str], idx=0
    ):  # pylint: disable=arguments-differ
        return super().from_file(file, idx=idx)

    def obs_to_outfile(
        self, h5file: h5py.File, **kwargs
    ):  # pylint: disable=unused-argument, arguments-differ
        """Output the observable information to the file. Nothing to do here."""
        return None

    def _matrix(
        self, t: float, dtt: float, idx: int, print_e: bool = False, energy_only=False
    ):

        mat = np.eye(self.n_s, dtype="complex128")

        jlow, jup = self.pairs[idx]
        f = self.j_coupling[idx].m

        # Short ciruit
        if energy_only:
            return None, f

        mat[jlow, jlow] = mat[jup, jup] = np.cos((dtt / self.splits[idx]) * f)
        mat[jup, jlow] = mat[jlow, jup] = -1j * np.sin(
            f * dtt * (1.0 / self.splits[idx])
        )

        # m = np.expand_dims(m, axis=list(range(self.n_m)))

        if print_e:
            return mat, f
        else:
            return mat
