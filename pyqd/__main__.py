# -*- coding: utf-8 -*-
"""Main module and argparsing."""
# -*- coding: utf-8 -*-

import argparse
import logging
import os
import sys
from pathlib import Path

from .integrators import RTPIntegrator
from .utils.io import TqdmStream


def parse_args(args):
    """Argparsing for pyQD."""
    parser = argparse.ArgumentParser(
        description="Python package to compute the numerical solution of the time-dependent "
        "Schrödinger equation by means of the split operator method. Handles both user-supplied "
        "numerical potential as well as analyical model systems consisting of n x 2 quantum "
        "harmonic oscillators. Infos and the documentation can be found at: "
        "https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/pyQD"
    )
    parser.add_argument(
        "inputfile",
        help="Input file in .json or .yaml format. Accepts both absolute and relative paths.",
    )
    parser.add_argument(
        "--verbose",
        "-v",
        help=(
            "Change the verbosity of the output to the DEBUG level. "
            "Prints much more info to the screen."
        ),
        action="store_true",
    )

    return parser.parse_args(args)


def main(argv=None):
    """Main function / entrypoint to pyQD."""

    if argv is None:
        argv = sys.argv[1:]

    args = parse_args(argv)

    # Set up logging
    log_format = "[{levelname:^10}] @ {name}: {message}"

    if args.verbose:
        log_level = "DEBUG"
    elif "LOGLEVEL" in os.environ:
        log_level = os.environ["LOGLEVEL"]
    else:
        log_level = "WARNING"

    logging.basicConfig(
        level=log_level, format=log_format, stream=TqdmStream, style="{"
    )

    # Get the inputfile and run the calculation
    ifile = Path(args.inputfile).expanduser().resolve()
    RTPIntegrator.from_file(ifile).propagate_all()

    print("\n\nSimulation finished successfully.")


if __name__ == "__main__":
    main()
