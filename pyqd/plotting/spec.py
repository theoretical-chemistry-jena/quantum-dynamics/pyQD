# -*- coding: utf-8 -*-

"""
Experimental small CLI tool to plot various electronic spectra from pyQD .h5 output files.
"""

import argparse
import logging
from pathlib import Path

import h5py
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pint

from ..exceptions import PyQDInputError
from ..utils.physics import calc_polarisation_spectrum


def argparser():
    """Provides an argparser for the pyQD-spec entrypoint."""

    parser = argparse.ArgumentParser(
        description="Spectrum plotter for pyQD output data."
    )
    parser.add_argument("h5file", help="hdf5-File from a pyQD RTP propagation.")
    parser.add_argument(
        "--mplstyle",
        "-s",
        help="Matplotlib style to use for plotting",
        default="default",
    )
    parser.add_argument(
        "--show", "-w", help="Show the interactive spectrum", action="store_true"
    )
    parser.add_argument(
        "--format", "-f", help="File format of the resulting spectrum", default="png"
    )
    parser.add_argument(
        "--aspect",
        "-r",
        help="Aspect ratio of the resulting spectrum",
        choices=["169", "43", "11", "flat"],
        default="169",
    )
    parser.add_argument(
        "--tstart",
        "-t",
        help=(
            "For polarisation based spectra, do you want a custom initial time? "
            "Give time in fs. Otherwise, the central time is extracted from the "
            "last(-indexed) field."
        ),
    )
    parser.add_argument(
        "--trim-index-end",
        "-u",
        help="Specify the index at which the polarisation dataset is trimmed at the end",
        type=int,
    )
    parser.add_argument(
        "--correlation",
        "-c",
        action="store_true",
        help="Plot the autocorrelation-based spectra?",
        default=False,
    )
    parser.add_argument(
        "--polarisation",
        "-p",
        action="store_true",
        help="Plot the polarisation function-based spectra?",
        default=False,
    )
    parser.add_argument(
        "--complex",
        "-e",
        action="store_true",
        help="Plot real and imaginary parts for the polarisation-based spectra?",
        default=False,
    )
    parser.add_argument(
        "--output",
        "-o",
        type=str,
        help="Output folder for the spectrem. Defaults the the folder of the h5file?",
        default="h5",
    )
    parser.add_argument(
        "--ebelschinke",
        "-b",
        help=(
            "Calculate the dE/dw based on the ansatz derived in Ebel & Schinke, 1994. "
            "DOI: https://doi.org/10.1063/1.467697"
        ),
        default=False,
        action="store_true",
    )

    args = parser.parse_args()

    return args


def main():
    """Main function for the pyQD-spec entrypoint."""

    ureg = pint.UnitRegistry()

    freq2evolt = ureg.Quantity("1/a_u_time").to("eV", "sp").m
    au2evolt = ureg.Quantity("Eh").to("eV", "sp").m

    args = argparser()

    h5path = Path(args.h5file)
    h5fname = h5path.name

    if args.output == "h5":
        outpath = h5path.parent
    else:
        outpath = Path(args.output).resolve()

    mpl.use("TkAgg" if args.show else "agg")

    plt.style.use(args.mplstyle)

    h5f = h5py.File(h5path, "r")

    polarisation = args.polarisation
    correlation = args.correlation

    if not polarisation and not correlation:
        logging.info(
            "You requested neither correlation nor polarisation-based spectra, "
            "assuming both."
        )
        correlation = polarisation = True

    if correlation:
        try:
            spec_corr = np.asarray(h5f["correlation"]["spectrum"])
            wgrid_corr = np.asarray(h5f["correlation"]["freqgrid"])
            fwdidx_corr = wgrid_corr.size // 2

        except KeyError as e:
            raise PyQDInputError(
                "You requested polarisation-based spectra,"
                " but the .h5-file does not provide them."
            ) from e

    if polarisation:
        try:
            tgrid_pol = np.asarray(h5f["tgrid"])

            if args.tstart is not None or args.trim_index_end is not None:

                spec_pol_timedata = np.asarray(h5f["polarisation"]["timedata"])
                logging.info("Custom spectrum calculation for polarisation!")

                try:
                    t_init_or_idx = int(0 if args.tstart is None else args.tstart)
                    t_init_idx = t_init_or_idx
                except ValueError:
                    t_init = ureg.Quantity(args.tstart).to("a_u_time").m
                    t_init_idx = np.argmin(np.abs(tgrid_pol - t_init))

                tstart = tgrid_pol[t_init_idx]
                trim_idx = getattr(args, "trim_index_end", None)

                wgrid_pol, spec_pol, spec_pol_abs = calc_polarisation_spectrum(
                    spec_pol_timedata, tgrid_pol, tstart, trim_idx
                )
            else:
                spec_pol = np.asarray(h5f["polarisation"]["spectrum"])
                spec_pol_abs = np.asarray(h5f["polarisation"]["spectrum_abs"])
                wgrid_pol = np.asarray(h5f["polarisation"]["freqgrid"])

            fwdidx_pol = wgrid_pol.size // 2

        except KeyError as e:
            raise PyQDInputError(
                "You requested polarisation-based spectra,"
                " but the .h5-file does not provide them."
            ) from e

    # Additionally, compute the Ebel Schinke approach (dE / dw)
    if args.ebelschinke:
        try:
            # First, get the polarisation data and the field
            polar_es = np.asarray(h5f["polarisation"]["timedata"])
            tgrid_es = np.asarray(h5f["tgrid"])

            wgrid_es = (
                np.fft.fftfreq(len(tgrid_es), d=(tgrid_es[1] - tgrid_es[0]))
                * freq2evolt
            )

            fwdidx_es = len(wgrid_es) // 2 - 1

            field_es = np.asarray(h5f["field_0"])[0]

            field_es_dot = np.gradient(field_es)

            polar_es_w = np.fft.ifft(polar_es)
            field_es_dot_w = np.fft.ifft(field_es_dot)

            # Calculate the FFTs of the polarisation and the field gradient
            r_omega_es_w = polar_es_w * field_es_dot_w

        except KeyError as e:
            raise PyQDInputError(
                "You requested polarisation-based spectra,"
                " but the .h5-file does not provide them."
            ) from e

    try:
        e_pot = np.asarray(h5f["e_pot"])
        pot_energies = np.asarray(h5f["ITP"]["energies"])
        plot_levels = True
    except KeyError as e:
        logging.info(
            "Energy expectancy values could not be found,"
            " so no energy levels can be plotted. {}".format(e)
        )
        plot_levels = False

    figsize = {"169": (6, 3.75), "43": (6, 4.5), "11": (6, 6), "flat": (6, 2.5)}[
        args.aspect
    ]

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111)

    if correlation:
        ax.plot(
            wgrid_corr[:fwdidx_corr],
            spec_corr[:fwdidx_corr] / np.nanmax(spec_corr[:fwdidx_corr]),
            label=r"$\sigma_{C}(\omega)$",
            color="darkorange",
        )

    if polarisation:
        ax.plot(
            wgrid_pol[:fwdidx_pol],
            spec_pol_abs[:fwdidx_pol] / np.nanmax(spec_pol_abs[:fwdidx_pol]),
            label=r"$\sigma_{P}(\omega)\ \to\ \Re^2+\Im^2$",
            color="steelblue",
        )

        if args.complex:
            ax.plot(
                wgrid_pol[:fwdidx_pol],
                np.real(spec_pol[:fwdidx_pol])
                / np.nanmax(np.abs(np.real(spec_pol[:fwdidx_pol]))),
                label=r"$\sigma_{{P}}(\omega)\ \to\ \Re$",
                alpha=0.4,
                lw=1.0,
                color="darkblue",
            )
            ax.plot(
                wgrid_pol[:fwdidx_pol],
                np.imag(spec_pol[:fwdidx_pol])
                / np.nanmax(np.abs(np.real(spec_pol[:fwdidx_pol]))),
                label=r"$\sigma_{{P}}(\omega)\ \to\ \Im$",
                alpha=0.4,
                lw=1.0,
                color="darkred",
            )

    # Egel Schinke Algorithm
    if args.ebelschinke:
        ax.plot(
            wgrid_es[:fwdidx_es],
            -2
            * (
                np.real(r_omega_es_w[:fwdidx_es])
                / np.nanmax(np.abs(np.real(r_omega_es_w[:fwdidx_es])))
            ),
            label=r"$R(\omega)\ \to\ \Re$",
            color="limegreen",
        )

    # The potential energy offset is (hbar * omega) / 2
    if plot_levels:
        pot_offset = pot_energies[0] * au2evolt
        # The nu0-nu0-transition between the ground and first excited state
        trans_00 = (np.nanmin(e_pot[..., 1]) - np.nanmin(e_pot[..., 0])) * au2evolt
        for ii in range(5):
            color = ("#404040" + "{:x}".format(20 + 40 * ii)) if ii > 0 else "#00bb0088"
            ax.axvline(
                trans_00 + (2 * ii) * pot_offset,
                color=color,
            )

    ax.set_xlim(1.9, 2.8)
    ax.set_ylim((-0.05, 1.05) if not args.complex else (-0.15, 1.05))
    # ax.set_yticks([])
    ax.set_xlabel("Energy / eV")
    ax.set_ylabel("Norm. intensity / arb. u.")
    ax.legend(fontsize="small", loc=1)

    fig.tight_layout()

    outname = ".".join(["pyQD", "SPEC"] + h5fname.split(".")[2:-1] + ["png"])

    if args.show:
        plt.show()
    else:
        fig.savefig(outpath / outname, extension=args.format)


if __name__ == "__main__":
    main()
