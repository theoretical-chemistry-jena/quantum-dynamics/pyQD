# -*- coding: utf-8 -*-

"""
Experimental plotting facility to generate animations based on the real time propagation data.
"""

# pylint: disable=E1136

import argparse
import logging
import shutil
import subprocess
import sys
import tempfile
import warnings
from dataclasses import dataclass
from multiprocessing import Pool, cpu_count
from pathlib import Path
from typing import List, Optional, Tuple, Union

import h5py
import matplotlib
import matplotlib.cm as mcm
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec, SubplotSpec

from ..utils.convert import EH2EV

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

COLORS = plt.rcParams["axes.prop_cycle"].by_key()["color"]


def parse_args(args):
    """
    This function provides an argparser for the CLI.
    """

    parser = argparse.ArgumentParser(
        description="Simple plotting facility for pyQD-hdf5 files."
    )

    parser.add_argument(
        "h5file", help="Main hdf5 file generated in the RTP. ITP not functional yet."
    )

    parser.add_argument(
        "-d",
        "--disable-field",
        dest="do_plot_field",
        action="store_false",
        default=True,
        help="Disable parallel rendering of frames",
    )

    parser.add_argument(
        "-b",
        "--disable-ptb",
        dest="do_plot_ptb",
        action="store_false",
        default=True,
        help="Disable parallel rendering of frames",
    )

    parser.add_argument(
        "-f",
        "--format",
        help="Which format should we plot to?",
        default="gif",
    )

    parser.add_argument(
        "-c",
        "--colormap",
        help="Which matplotlib colormap should we use?",
        default="viridis",
    )

    parser.add_argument(
        "-a",
        "--arrows",
        help="Draw arrows from the momentum and position expectancy values (if available)",
        default=True,
    )

    parser.add_argument(
        "-z",
        "--zoom",
        help="Zoom factor to use for plotting the wavefunction",
        default=2,
        type=int,
    )

    parser.add_argument(
        "-s",
        "--style",
        help=(
            "Matplotlib style to be used for the plotting. "
            "Information to defining matplotlib styles can be found here: "
            "https://matplotlib.org/3.3.0/tutorials/introductory/customizing.html."
        ),
        default="seaborn-ticks",
    )

    parser.add_argument(
        "-q",
        "--disable-parallel",
        dest="parallel",
        action="store_false",
        default=True,
        help="Disable parallel rendering of frames",
    )

    parser.add_argument(
        "-g",
        "--plot-groundstate",
        dest="plot_gs",
        default=False,
        action="store_true",
        help="Also plot the groundstate in the norm plot",
    )

    return parser.parse_args(args)


@dataclass
class CommonParameters:
    """
    Parameters dataclass as a container for animation plotting
    """

    # Basics
    fname: str

    # Arrays
    tgrid: np.ndarray
    norm: np.ndarray
    fields: List[np.ndarray]
    xy_data: List[np.ndarray]
    pes: np.ndarray
    xgrid: np.ndarray

    # Config
    tmpdir: Path
    cmap: str
    plot_groundstate: bool = False
    zoom: Optional[int] = 0.5
    vmax: Optional[float] = 1.0

    # Optional arrays
    ptbs: Optional[dict] = None
    posmom: Optional[Tuple[np.ndarray, np.ndarray]] = None

    _colors: List[str] = None

    def __post_init__(self):
        self._colors = list(plt.rcParams["axes.prop_cycle"].by_key()["color"])

    @property
    def tmin(self):
        """Minimum of the time grid array"""
        return np.min(self.tgrid)

    @property
    def tmax(self):
        """Maximum of the time grid array"""
        return np.max(self.tgrid)

    @property
    def n_states(self):
        """Number of states of the system"""
        return self.pes.shape[-1]

    @property
    def n_monomers(self):
        """
        Number of dimensions of the wavefunction.
        Associated to the number of monomers in the individual axis model.
        """
        return len(self.pes.shape) - 1

    @property
    def xlim(self):
        """Plotting limits based on the specified zoom factor"""
        return (self.zoom * np.min(self.xgrid), self.zoom * np.max(self.xgrid))

    @property
    def colors(self):
        """Return the default color cycle from matplotlib as a list"""
        return self._colors


def plot_wfs_contour(
    params: CommonParameters,
    idx: int,
    fig: Figure,
    grid: Union[GridSpec, SubplotSpec],
    dataset: h5py.Dataset,
):
    """Plot wavefunction plots into the specified grid

    Args:
        params (CommonParameters): Common parameters instance that contains the data
        idx (int): Current propagation index
        fig (Figure): Figure instance in which to plot
        grid (Union[GridSpec, SubplotSpec]): GridSpec container instance to use for subgridding
            and plotting the contours.
        dataset (h5py.Dataset): Open h5py dataset to read the wavefunction from.
    """

    n_states = params.n_states

    # Duck typing: We need a SubplotSpec
    try:
        wfgrid = grid.subgridspec(1, n_states, wspace=0.02)
    except AttributeError:
        wfgrid = grid[0].subgridspec(1, n_states, wspace=0.02)

    # Get colormap for plotting
    colormap = mcm.ScalarMappable(cmap=params.cmap)
    colormap.set_array(np.arange(0.0, 1.6, 0.2))

    for state, sgrid in enumerate(wfgrid):
        ax = fig.add_subplot(sgrid, aspect="equal")
        ax.annotate(
            f"{state}",
            xy=(0.05, 0.9),
            xycoords="axes fraction",
            color="#777777",
            size="small",
            weight="bold",
        )
        ax.set_axis_off()

        ax.set_xlim(params.xlim)
        ax.set_ylim(params.xlim)

        ax.contourf(
            *params.xy_data,
            np.abs(np.asarray(dataset)[..., state]) ** 2,
            vmin=0.0,
            vmax=params.vmax[state],
            levels=30,
            cmap=params.cmap,
        )

        if params.posmom is not None:
            pos, mom = params.posmom
            curr_pos = pos[state, :, idx]
            curr_mom = mom[state, :, idx]
            ax.scatter(*curr_pos, color="#40404088", s=2)
            ax.arrow(
                *curr_pos,
                *curr_mom,
                color="#40404088",
                width=0.005,
                length_includes_head=True,
            )

    return wfgrid


def plot_wfs_potential(
    params: CommonParameters,
    fig: Figure,
    grid: Union[GridSpec, SubplotSpec],
    dataset: h5py.Dataset,
):
    """Plot wavefunction plots into the specified grid

    Args:
        params (CommonParameters): Common parameters instance that contains the data
        idx (int): Current propagation index
        fig (Figure): Figure instance in which to plot
        grid (Union[GridSpec, SubplotSpec]): GridSpec container instance to use for subgridding
            and plotting the wavefunction.
        dataset (h5py.Dataset): Open h5py dataset to read the wavefunction from.
    """

    pes = params.pes
    xgrid = params.xgrid
    n_states = params.n_states
    colors = params.colors

    # Duck typing: We need a SubplotSpec
    try:
        wfgrid = grid.subgridspec(1, n_states, wspace=0.02)
    except AttributeError:
        wfgrid = grid[0].subgridspec(1, n_states, wspace=0.02)

    for wstate, sgrid in enumerate(wfgrid):
        pax = fig.add_subplot(sgrid)
        wax = pax.twinx()

        # Plot the potentials
        for pstate in range(pes.shape[-1]):
            pax.plot(xgrid, pes[:, pstate] * EH2EV)

        # plot the wavefunction
        wax.plot(
            xgrid,
            np.abs(np.asarray(dataset)[..., wstate]) ** 2,
            lw=2.5,
            color=colors[wstate],
        )

        pax.set_xlim(params.xlim)
        pax.set_ylim(-0.1, 4.05)
        wax.set_ylim(-0.05 * params.vmax[wstate], 1.05 * params.vmax[wstate])

        if wstate > 0:
            pax.set_yticks([])


def plot_norm(
    params: CommonParameters, idx: int, fig: Figure, grid: Union[GridSpec, SubplotSpec]
):
    """Plot the norm data

    Args:
        params (CommonParameters): Parameters data class instance
        idx (int): Current propagation index
        fig (Figure): Figure to plot into
        grid (Union[GridSpec, SubplotSpec]): GridSpec grid to subplot
    """

    tgrid = params.tgrid
    norm = params.norm

    # Duck typing: We need a SubplotSpec
    try:
        nax = fig.add_subplot(grid[0])
    except TypeError:
        nax = fig.add_subplot(grid)

    # Whether to start at the first or second index
    fstate = int(not params.plot_groundstate)
    norm_max = np.max(norm[fstate, :])

    logger.debug("Norm: {n.shape}\t{n.dtype}".format(n=norm))

    # Exclude the ground state if requested
    for i in range(fstate, params.n_states):
        nax.plot(tgrid[:idx], norm[i, :idx], label=f"{i}")

    nax.set_xlim([params.tmin, params.tmax])
    nax.set_ylim([-0.05 * norm_max, 1.05 * norm_max])
    nax.set_ylabel("Norm per state")
    nax.legend(loc=1, fontsize="x-small")

    return nax


def plot_field(
    params: CommonParameters, idx: int, fig: Figure, grid: Union[GridSpec, SubplotSpec]
):
    """Plot the field data

    Args:
        params (CommonParameters): Parameters data class instance
        idx (int): Current propagation index
        fig (Figure): Figure to plot into
        grid (Union[GridSpec, SubplotSpec]): GridSpec grid to subplot
    """

    fields = np.asarray(params.fields)

    # Duck typing: We need a SubplotSpec
    try:
        fax = fig.add_subplot(grid[0])
    except TypeError:
        fax = fig.add_subplot(grid)

    tgrid = params.tgrid

    # Get a good scaling factor and scale fields accordingly
    expon_by_three = np.log10(np.nanmax(fields)) // 3
    fields /= 10 ** (3 * expon_by_three)

    # Plot the fields
    for ii, field in enumerate(fields):
        label = f"Field {ii + 1}"
        fax.plot(tgrid[:idx], field[0, :idx], label=label)

    # Formatting of the axis
    fax.set_xlim([params.tmin, params.tmax])
    minmax = 1.05 * np.nanmin(fields), 1.05 * np.nanmax(fields)
    fax.set_ylim(minmax)
    fax.set_ylabel(f"$E(t)$ / $10^{{{int(expon_by_three * 3)}}}$ a.u.")

    fax.legend(loc=1, fontsize="x-small")

    return fax


def plot_perturbation(
    params: CommonParameters, idx: int, fig: Figure, grid: Union[GridSpec, SubplotSpec]
):
    """Plot the field data

    Args:
        params (CommonParameters): Parameters data class instance
        idx (int): Current propagation index
        fig (Figure): Figure to plot into
        grid (Union[GridSpec, SubplotSpec]): GridSpec grid to subplot
    """

    # Duck typing: We need a SubplotSpec
    try:
        pax = fig.add_subplot(grid[0])
    except TypeError:
        pax = fig.add_subplot(grid)

    tgrid = params.tgrid

    minmaxs = sorted(
        [m for _, p in params.ptbs.items() for m in (np.nanmin(p), np.nanmax(p))]
    )
    expon_by_three = np.log10(np.nanmax(minmaxs)) // 3

    for key in params.ptbs:
        ptb = np.asarray(params.ptbs[key]) / 10 ** (3 * expon_by_three)
        pax.plot(tgrid[:idx], ptb[0, :idx], label=key.capitalize())

    lims = [1.05 * i / 10 ** (3 * expon_by_three) for i in [minmaxs[0], minmaxs[-1]]]

    pax.set_xlim([params.tmin, params.tmax])
    pax.set_ylim(lims)
    pax.set_ylabel(f"$P(t)$ / $10^{{{int(expon_by_three * 3)}}}$ $E_{{H}}$")

    pax.legend(loc=1, fontsize="x-small")

    return pax


def plot_frame_2d(
    params: CommonParameters,
    idx: int,
    idx_string: Union[str, int],
    dataset: h5py.Dataset,
) -> None:

    """
    Plotting function. Generates a single frame based on the propagation index.
    To be used with the multiprocessing.Pool facilities.

    Args:
        params: (CommonParameters): CommonParameters instance that hold, the common data
        idx (int): Current *frame* index
        idx_string (Union[str, int]): Current propagation index (the name of the dataset, usually)
        dataset (h5py.Dataset): h5py dataset to plot the wavefunction from
    """

    matplotlib.use("agg")

    logger.debug(
        "Current index: {idx}; str idx {idx_string}".format(
            idx=idx, idx_string=idx_string
        )
    )

    # Get data from params
    tmpdir = Path(params.tmpdir)
    prop_idx = int(idx_string)
    do_plot_field = len(params.fields) != 0
    do_plot_ptb = len(params.ptbs) != 0
    nr_timeplots = 1 + int(do_plot_field) + int(do_plot_ptb)

    logger.debug("Current propagation idx: {}".format(prop_idx))

    # * Figure preparation using GridSpec
    fig = plt.figure(figsize=(6, 4 + 2 * (int(do_plot_field) + int(do_plot_ptb))))
    outergrid = fig.add_gridspec(2, 1, height_ratios=[1, nr_timeplots])

    # * Prepare other subgrids for TD plots
    subgrid_n = outergrid[1].subgridspec(nr_timeplots, 1, wspace=0.02, hspace=0.02)

    # * Plotting the wavefunctions
    if params.n_monomers == 1:
        plot_wfs_potential(params, fig, outergrid[0], dataset)

    else:
        plot_wfs_contour(params, prop_idx, fig, outergrid[0], dataset)

    # * Norm plot
    norm_ax = plot_norm(params, prop_idx, fig, grid=subgrid_n[0])

    # * Field plot
    if do_plot_field:
        field_ax = plot_field(params, prop_idx, fig, grid=subgrid_n[1])

    # * Perturbation plot
    if do_plot_ptb:
        plot_perturbation(params, prop_idx, fig, grid=subgrid_n[1 + int(do_plot_field)])

    if do_plot_field:
        norm_ax.set_xticklabels([])

    if do_plot_ptb:
        norm_ax.set_xticklabels([])

        if do_plot_field:
            field_ax.set_xticklabels([])

    fig.tight_layout()

    fname = tmpdir / params.fname.format(idx=f"{idx:04.0f}")
    fig.savefig(fname, format="png", transparent=False)
    plt.close(fig=fig)


# ! =============================================================


def main(argv=None, return_tmppath=False):
    """
    Main function for the pyQD-anim CL program.
    """

    if argv is None:
        argv = sys.argv[1:]

    # Prevent matplotlib from complaining about GridSpec plots and tight_layout
    warnings.filterwarnings(
        "ignore",
        message="This figure includes Axes that are not compatible with tight_layout",
    )

    args = parse_args(argv)

    # Get data from the hdf5 file
    f = h5py.File(args.h5file, "r")

    n_dim = f.attrs["n_dim"]
    xgrid = [np.asarray(f["xgrid"])] * n_dim
    stringed_idxs = list(f["wf"].keys())
    frames = list(f["wf"].values())

    full_vmax = np.nanmax(np.abs(np.asarray(frames)) ** 2, axis=tuple(range(n_dim + 1)))
    logger.info("Full vmax: {}".format(full_vmax))

    xy_data = np.meshgrid(*xgrid)

    fields = []
    ctr = 0

    if args.do_plot_field:
        while True:
            try:
                fields.append(np.asarray(f[f"field_{ctr}"]["field"]))
                ctr += 1
            except KeyError:
                break

    # Get the perturbations
    ptbs = {}
    if args.do_plot_ptb:
        for key in ("legacy", "nanoparticle"):
            try:
                ptbs[key] = np.asarray(f[key])
            except KeyError:
                pass

    # If arrows are requested, we need position and momentum
    # expectation values
    if args.arrows:
        try:
            pos = np.asarray(f["position"])
            mom = np.asarray(f["momentum"])

            posmom = (pos, mom)
        except KeyError:
            logger.info("Position and/or momentum EVs not found.")
            posmom = None

    else:
        posmom = None

    # Get a temp dir to store the intermediate pngs
    tmpdir = Path(tempfile.mkdtemp())

    # Define a template filename
    fname_tmpl = "wf_{idx}.png"

    params = CommonParameters(
        norm=np.asarray(f["norm"], dtype=np.float64),
        fields=fields,
        xy_data=xy_data,
        tmpdir=str(tmpdir),
        fname=fname_tmpl,
        cmap=args.colormap,
        tgrid=np.asarray(f["tgrid"], dtype=np.float64),
        pes=np.asarray(f["e_pot"], dtype=np.float64),
        xgrid=np.asarray(f["xgrid"], dtype=np.float64),
        ptbs=ptbs,
        posmom=posmom,
        zoom=1 / float(args.zoom),
        vmax=full_vmax,
        plot_groundstate=args.plot_gs,
    )

    logger.info("Saving temporary data to {}".format(tmpdir))

    # ! Main loop
    plt.style.use(args.style)

    if args.parallel:
        logger.info(
            "Plotting the frames in parallel using {} CPU cores ...".format(cpu_count())
        )

        with Pool(processes=cpu_count()) as pool:
            pool.starmap(
                plot_frame_2d,
                (
                    (params, idx, sidx, np.asarray(frame))
                    for idx, (sidx, frame) in enumerate(zip(stringed_idxs, frames))
                ),
            )

    else:
        logger.info("Plotting frames sequentially.")

        for idx, (sidx, frame) in enumerate(zip(stringed_idxs, frames)):
            plot_frame_2d(params, idx, sidx, np.asarray(frame))

    logger.info("Plotting the frames done.")

    # ! Do conversion to gif/mp4 etc
    outpath = Path(args.h5file)
    out_fname = outpath.parent / "pyQD.PROP_{}.{}".format(
        ".".join(outpath.name.split(".")[2:-1]), args.format.strip(".")
    )

    conv_call = [
        "convert",
        "-dispose",
        "Background",
        "+dither",
        "-remap",
        "netscape:",
        "{}/{}".format(tmpdir, fname_tmpl.format(idx="*")),
        str(out_fname),
    ]

    logger.info("Running command: {}".format(" ".join(conv_call)))

    try:
        subprocess.check_call(conv_call)
        logger.info("Wrote the final .gif file to {!r}".format(out_fname))

    except subprocess.CalledProcessError as e:
        logger.error(
            "'convert' command did not succeed with error message: {}".format(e),
            exc_info=e,
        )

    if not return_tmppath:
        shutil.rmtree(tmpdir)
        logger.info("Removed the temporary directory")

    else:
        logger.info(
            "Did not remove the temporary directory. You are now responsible for it!"
        )
        return tmpdir
