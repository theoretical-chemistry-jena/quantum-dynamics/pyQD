# -*- coding: utf-8 -*-
""" Unit tests for pyqd.plotting.rtp """

import logging
import shutil
import uuid
from pathlib import Path

import h5py
import numpy as np
import pytest
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec
from matplotlib.patches import FancyArrow
from PIL import Image

from ..rtp import (
    CommonParameters,
    main,
    plot_field,
    plot_norm,
    plot_perturbation,
    plot_wfs_contour,
    plot_wfs_potential,
)

HERE = Path(__file__).parent

logging.basicConfig(level=logging.INFO)


@pytest.fixture(name="h5file")
def fixture_h5file(tmpdir):
    """Temporary h5file for CLI tests"""

    file = HERE / "data/pyQD.RTP.h5"
    filepath = shutil.copy(file, Path(tmpdir))

    return filepath


@pytest.fixture(name="fig", scope="function")
def fixture_fig():
    """Simple Figure as fixture"""
    return Figure()


@pytest.fixture(name="grid")
def fixture_gridspec():
    """Simple GridSpec as fixture"""
    return GridSpec(nrows=1, ncols=1)


@pytest.fixture(name="params")
def commonparam(tmpdir):
    """Return a CommonParameters instance for testing"""
    return CommonParameters(
        fname="Test",
        tgrid=np.linspace(0, 1.0, 100),
        norm=np.random.rand(3, 100),
        fields=[np.random.rand(3, 100)] * 2,
        xy_data=[np.random.rand(10, 10)] * 2,
        pes=np.random.rand(10, 10, 3),
        xgrid=np.linspace(-100, 100, num=10),
        tmpdir=Path(tmpdir),
        cmap="viridis",
        plot_groundstate=True,
        zoom=0.5,
        vmax=[1.0, 1.0, 1.0],
    )


@pytest.fixture(name="params_1mns")
def commonparam_1mns(params: CommonParameters):
    """
    Return a CommonParameters instance for testing,
    specifically mimicking 1M-nS calculations
    """

    params.pes = np.random.rand(10, 3)
    params.xgrid = np.linspace(-100, 100, num=10)

    return params


@pytest.fixture(name="dataset_1mns")
def fixture_dataset_1mns(tmpdir):
    """Fixture to yield a temporary dataset for contours"""

    tmpdir = Path(tmpdir)
    tmpfile = tmpdir / (str(uuid.uuid4()) + ".h5")
    tmpdir.mkdir(parents=True, exist_ok=True)

    with h5py.File(tmpfile, "w") as f:
        dset = f.create_dataset(name=None, data=np.random.rand(10, 3))
        yield dset

    tmpfile.unlink()


@pytest.fixture(name="dataset")
def fixture_dataset(tmpdir):
    """Fixture to yield a temporary dataset for contours"""

    tmpdir = Path(tmpdir)
    tmpfile = tmpdir / (str(uuid.uuid4()) + ".h5")
    tmpdir.mkdir(parents=True, exist_ok=True)

    with h5py.File(tmpfile, "w") as f:
        dset = f.create_dataset(name=None, data=np.random.rand(10, 10, 3))
        yield dset

    tmpfile.unlink()


@pytest.fixture(name="idx", params=[0, 10, 99])
def fixture_index(request) -> int:
    """Index fixture giving the current propagation index"""
    return request.param


def test_properties(params):
    """Basic property unit test for the CommonParameters class"""
    assert params.ptbs is None
    assert params.posmom is None
    assert params.tmin == 0.0
    assert params.tmax == 1.0


@pytest.mark.parametrize("posmom", [False, True])
def test_plot_wfs_contour(params, idx, fig, grid, dataset, posmom):
    """Unit test for the plot_wfs function"""

    if posmom:
        posmom = [np.random.rand(3, 2, 100)] * 2
        params.posmom = posmom

    plot_wfs_contour(params, idx, fig, grid, dataset=dataset)

    assert len(fig.axes) == 3

    if posmom:
        print(fig.axes[0].get_children())
        assert len(fig.axes[0].findobj(match=FancyArrow)) == 1


def test_plot_wfs_potential(params_1mns, fig, grid, dataset_1mns):
    """Unit test for the plot_norm function"""

    params = params_1mns
    params.plot_groundstate = True

    plot_wfs_potential(params, fig, grid, dataset=dataset_1mns)
    pot_lines = fig.axes[0].get_lines()
    wf_lines = fig.axes[1].get_lines()

    assert len(fig.axes) == 6
    assert len(pot_lines) == 3
    assert len(wf_lines) == 1
    assert fig.axes[0].get_xlim() == params.xlim


@pytest.mark.parametrize("plot_gs", [(True,), (False,)])
def test_plot_norm(params, idx, fig, grid, plot_gs):
    """Unit test for the plot_norm function"""

    params.plot_groundstate = plot_gs

    plot_norm(params, idx, fig, grid)
    lines = fig.axes[0].get_lines()

    assert len(fig.axes) == 1
    assert len(lines) == 2 if not plot_gs else 3
    assert len(lines[0].get_ydata()) == idx
    assert fig.axes[0].get_xlim() == (params.tmin, params.tmax)


def test_plot_fields(params, idx, fig, grid):
    """Unit test for the plot_fields function"""

    plot_field(params, idx, fig, grid)

    lines = fig.axes[0].get_lines()

    assert len(fig.axes) == 1
    assert len(lines) == 2
    assert len(lines[0].get_ydata()) == idx
    assert np.allclose(lines[0].get_ydata(), params.fields[0][0, :idx] * 1e3)
    assert fig.axes[0].get_xlim() == (params.tmin, params.tmax)


@pytest.mark.parametrize(
    "data",
    [{"nanoparticle": None}, {"legacy": None}, {"nanoparticle": None, "legacy": None}],
)
def test_plot_ptb(params, idx, fig, grid, data):
    """Unit test for the plot_perturbation function"""

    for key in data:
        data[key] = np.random.rand(2, 100)

    params.ptbs = data

    plot_perturbation(params, idx, fig, grid)

    assert len(fig.axes) == 1
    assert len(fig.axes[0].get_lines()) == len(data)
    assert len(fig.axes[0].get_lines()[0].get_ydata()) == idx


@pytest.mark.parametrize(
    "add_args",
    [
        ["-q", "-g", "-a", "False", "-z", "3"],
        ["-q", "-g", "-d"],
        ["-q", "-g", "-b", "-c", "viridis_r"],
        ["-q", "-d", "-b"],
    ],
)
def test_rtp_main(h5file, add_args):
    """
    Test for the whole plotter CLI

    HINT:
    We need to run all tests sequentially here, otherwise pytest will deadlock.
    See https://github.com/pytest-dev/pytest-cov/issues/295
    """

    args = [h5file] + add_args

    # Get the expected height of the image in pixels
    if "-b" in add_args and "-d" in add_args:
        height = 400
    elif "-b" in add_args or "-d" in add_args:
        height = 600
    else:
        height = 800

    tmppath = main(args, return_tmppath=True)
    files = list(tmppath.glob("*.png"))

    assert len(files) == 100

    img = Image.open(files[0])
    assert img.height == height
    assert img.width == 600

    # Remove the temporary directory
    shutil.rmtree(tmppath)
