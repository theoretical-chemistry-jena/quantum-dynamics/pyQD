# -*- coding: utf-8 -*-
"""pyQD exception module"""


class PyQDError(Exception):
    """Basic exception class"""


class PyQDIOError(PyQDError):
    """Exception class for all things I/O"""


class PyQDInputError(PyQDError):
    """Exception class for all things regarding input parsing."""


class PyQDPhysicsError(PyQDError):
    """Exception class for all things regarding 'weird' physics."""
