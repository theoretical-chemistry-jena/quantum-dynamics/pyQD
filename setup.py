# -*- coding: utf-8 -*-
"""Setup file for pyQD, the split-operator TDSE package."""

# from distutils.extension import Extension

from setuptools import find_packages, setup

import versioneer

NAME = "pyqd"
DESCRIPTION = (
    "Python implementation of a ND quantum molecular dynamics solver, "
    "based on the wavefunction split-operator method."
)
URL = "https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/pyQD"
EMAIL = "fabian.droege@uni-jena.de"
AUTHOR = "Fabian G. Dröge"
REQUIRES_PYTHON = ">=3.7.0"
REQUIRED = [
    "numpy",
    "matplotlib",
    "h5py",
    "numba",
    "pyfftw",
    "opt_einsum",
    "pint",
    "pyyaml",
    "tqdm",
    "attrs",
]
ENTRYPOINTS = {
    "console_scripts": [
        "pyQD = pyqd.__main__:main",
        "pyQD-spec = pyqd.plotting.spec:main",
        "pyQD-anim = pyqd.plotting.rtp:main",
        "pyQD-wave = pyqd.plotting.itp:main",
    ]
}
CLASSIFIERS = [
    # Trove classifiers
    # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
    "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: Implementation :: CPython",
]

setup(
    name=NAME,
    description=DESCRIPTION,
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    install_requires=REQUIRED,
    python_requires=REQUIRES_PYTHON,
    packages=find_packages(exclude=("pyqd.test", "pyqd.plotting.test")),
    entry_points=ENTRYPOINTS,
    classifiers=CLASSIFIERS,
    zip_safe=False,
    license="GPLv3",
    include_package_data=True,
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
)
