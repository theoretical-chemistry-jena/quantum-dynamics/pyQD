let
  pkgs = import ./nix/pkgs.nix { };

  pyQDPython = pkgs.python3.withPackages( ps: with ps; [
    setuptools
    matplotlib

    (ps.callPackage ./. { inherit pkgs; })
  ] );

in with pkgs; with pkgs.python3Packages; {

  production = mkShell {
    name = "pyQD";
    buildInputs = [
      pyQDPython
    ];
  };

  dev = callPackage ./. {
    inherit pkgs;
    additionalDevDeps = [
      which
      git

      ipython
      pylint
      black
      pre-commit
    ];
  };

}
