{ lib, requireFile, buildPythonPackage, nix-gitignore
# Python stuff
, numpy, scipy, opt-einsum, pyfftw, h5py, numba, pint, pyyaml, attrs, tqdm
, matplotlib, pytestCheckHook, pytest-cov, pillow, imagemagick
# Additional inputs for dev purposes
, additionalDevDeps ? [ ], doCheck ? true }:

buildPythonPackage rec {
  pname = "pyqd";
  version = "0.1.1";

  src = nix-gitignore.gitignoreSource [ ] ./..;

  nativeBuildInputs = additionalDevDeps;

  propagatedBuildInputs = [
    # Python runtime dependencies
    numpy
    attrs
    scipy
    matplotlib
    h5py
    numba
    pyfftw
    opt-einsum
    pint
    pyyaml
    tqdm

    # ImageMagick for GIF generation (pyQD-anim)
    imagemagick
  ];

  inherit doCheck;

  checkInputs = [ imagemagick pillow pytest-cov pytestCheckHook ];

  meta = with lib; {
    description =
      "Python implementation of a ND quantum molecular dynamics solver, based on the wavefunction split-operator method";
    license = licenses.gpl3;
    homepage =
      "https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/pyQD";
    platforms = platforms.linux;
  };
}
