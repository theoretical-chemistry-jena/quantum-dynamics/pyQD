{ useMKL ? false, useAVX2 ? true }:

let
  sources = import ./sources.nix;

  # Fix FFTW
  mathOverlay = self: super: {
    blas = if useMKL then super.blas.override { blasProvider = super.mkl; } else super.blas;
    fftw = if !useAVX2 then super.fftw else super.fftw.overrideAttrs (oldAttrs: {
      configureFlags = with super.lib.lists; oldAttrs.configureFlags
      ++ optional (oldAttrs.name == "fftw-double-3.3.8" || oldAttrs.name == "fftw-single-3.3.8") "--enable-avx2";
    });
  };

  nixpkgs = import sources.nixpkgs {
    config = {
      overlays = [ mathOverlay ];
      allowUnfree = true;
    };
  };
in
  nixpkgs
