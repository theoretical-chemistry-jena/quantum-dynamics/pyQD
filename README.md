# pyQD

[![pipeline status](https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/pyQD/badges/develop/pipeline.svg)](https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/pyQD/-/commits/develop)
[![coverage report](https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/pyQD/badges/develop/coverage.svg)](https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/pyQD/-/commits/develop)

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

Python implementation of a ND quantum molecular dynamics solver, based on the wavefunction split-operator method.

## Installation

### Requirements

In order to run pyQD, you will need:

- Python => 3.8 (extensively tested) with

- pip, (best practise: venv)

- FFTW3 installed

### How to install

To install safely, create a new virtual env using the [venv utility]([pyQDenv](https://docs.python.org/3.7/library/venv.html))

```bash
python3 -m venv </path/to/new/virtual/environment>
```

Then, activate it using:

```bash
. </path/to/new/virtual/environment>/bin/activate
```

Now, install all the pyQD dependencies using the pip provided by the virtual environment and then, install pyQD. The `-e` is required if you want to change something in the code, then you can have an "editable" installation.

```bash
cd <pyQD folder>
pip install --upgrade pip
pip install --upgrade setuptools
pip install numpy cython
python3 setup.py build_ext --inplace
pip install .
```

You should now have four new executables:

- `pyQD`: Main executable to run propagations, requires a .yaml input file
- `pyQD-anim`: Based on propagation data, generate an animation (currently only for 2D simulation)
- `pyQD-spec`: Generate spectra for a given propagation (uses the HDF5 RTP propagation output)
- `pyQD-wave`: Generates images from the output ITP files

A sample input file is found in [./data/inputSample.yaml](./data/inputSample.yaml)

Have fun!
