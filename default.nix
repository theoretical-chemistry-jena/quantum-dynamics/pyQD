{ pkgs ? import ./nix/pkgs.nix { }, additionalDevDeps ? [ ], doCheck ? true }:

with pkgs; python3Packages.callPackage ./nix/pyqd.nix {
  inherit additionalDevDeps; inherit doCheck;
}
